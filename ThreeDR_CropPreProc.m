%% Information
% This program (called by ThreeDR_Main) takes the first steps in converting
% 3D reconstruction footage to a 3D model, by performing stabilization,
% cropping, frame selection, and background subtraction.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 11th, 2018


%% Define and load some global variables
global ThreeDRCurrent       % Active analysis status
global temppath             % Temporary folder location
global cfig

ThreeDRCurrent = 1;     % Step 1 of the process

if or(isempty(temppath),isempty(cfig))  % Run Main if needed
    ThreeDR_Main;
else
    updateMainButtons(ThreeDRCurrent)
end


%% Get data locations & load image
% Select videos copied to the temporary folder, or browse to a new one
[matPath,vidPath] = loadVideoFile;

% % If matPath is not a valid file, abort
% if exist(matPath,'file')==0
%     error('File not found');
% end
% load(matPath, 'IMAGE');

% Create crop image file name
[fpath, fname, ext] = fileparts(matPath);
cropPath = [fpath, filesep, fname, 'crop', ext];
cropnonBGPath = [fpath, filesep, fname, 'crop_nonbg', ext];
% Crop settings file name
par_file = [fpath, filesep, strrep(fname,'_IMAGE',''), '_params.mat'];


%% Create cropped file if needed

if exist(cropPath,'file')==2
    disp('  Cropped image stack already exist.');
    
    cont = questdlg(['Cropped image stack already exist. You may '...
        'edit existing settings but the cropped file will be '...
        'overwritten. Continue anyway?'], ...
        'Stack and crop already exist', ...
        'Continue','Cancel','Continue');
    switch cont
        case 'Continue'
        case 'Cancel'
            ThreeDRCurrent = 0;
            return;
    end
end

%% Load parameter sets if available and desired
motiontxt = '';
clear doMotionTracking
if exist(par_file,'file')==2
    load(par_file);     % if exists, load post-proc parameters
    
    % Look for crop settings
    if isfield(output,'TCrop')
        choice = questdlg(['Crop settings detected. Clear or load ',...
            'settings? Any existing motion correction settings ',...
            'will not be removed.'], ...
            'Crop detected', ...
            'Load','Clear','Load');
        
        if strcmp(choice,'Clear')
            % Remove crop settings
            output = rmfield(output,{'LCrop','RCrop','TCrop',...
                'BCrop','bgmedfilt'});
        end
    end
    
    if isfield(output,'motioncorr')     % Correction applied
        motiontxt = ['Previously saved motion correction data ',...
            'was found. Selecting ',...
            '''Yes'' will load those settings and allow ',...
            'editing of the correction. Choosing ',...
            '''No'' will erase the motion parameters and ',...
            'no correction will be applied. Existing ',...
            'crop settings may be discarded.'];
        doMotionTracking = questdlg(['Do you want to correct ',...
            'for translational motion? ',motiontxt], ...
            'Motion Correction?', ...
            'Yes, load and edit','Yes, load existing','Discard correction','Yes, load existing');
    else    % No correction applied
        motiontxt = '';
    end
else
    output = struct();      % Initialize output structure
end
output.vidPath = vidPath;


%% Motion correction
if ~exist('doMotionTracking','var')
    doMotionTracking = questdlg(['Do you want to correct ',...
        'for translational motion? ',motiontxt], ...
        'Motion Correction?', ...
        'Yes','No','Yes');
end

switch doMotionTracking
    case {'Yes','Yes, load and edit'}
        [output.motioncorr,output.motiontrackpoints,...
                output.autostab] = MotionTracker(vidPath);
%         if ~isfield(output,'motioncorr')
%             %             output.motiontrackpoints = {[0,0,0]};
%             %             output.motioncorr = zeros(size(IMAGE,3),3);
%             %             output.autostab = struct();
%             [output.motioncorr,output.motiontrackpoints,...
%                 output.autostab] = MotionTracker(vidPath);
%         else
%             % Load motiontracking function. Retrieves an
%             % integer pixel correction for each frame
%             [output.motioncorr,output.motiontrackpoints,...
%                 output.autostab] = MotionTracker(vidPath,...
%                 output.motiontrackpoints,output.autostab);
%             %         [output.motioncorr,output.motiontrackpoints,...
%             %             output.autostab] = MotionTracker(IMAGE,...
%             %             output.motiontrackpoints,output.autostab);
%         end
    case 'Yes, load existing'
    case {'No','Discard correction'}
        % Reset motion tracking parameters
        output.motiontrackpoints = [0,0,0];
%         output.motioncorr = zeros(size(IMAGE,3),3);
        output.motioncorr = [];
        output.autostab = struct();
        % Remove crop settings
        if isfield(output,'TCrop')
            output = rmfield(output,{'LCrop','RCrop','TCrop',...
                'BCrop','bgmedfilt'});
        end
end

save(par_file,'output');    % Save motion settings


%% Aux camera motion

if isfield(output,'auxmotioncorr')
    foundAuxMotionTracking = questdlg(['Existing aux motion correction ',...
        'found. Do you want to edit or discard this motion?'], ...
        'Aux camera found', ...
        'Load and edit','Load','Discard','Load');
    
    switch foundAuxMotionTracking
        case 'Load and edit'
            doAuxMotionTracking = 'Yes';
        case 'Load'
            doAuxMotionTracking = 'Load';
        case 'Discard'
            output = rmfield(output,'auxmotioncorr');
            output = rmfield(output,'auxpath');
            output = rmfield(output,'auxoffset');
    end
else
    foundAuxMotionTracking = 'None';
end

if ~exist('doAuxMotionTracking','var')
% Ask for aux camera motion tracking
doAuxMotionTracking = questdlg(['Do you want to use an auxiliary ',...
    'camera for z-motion correction?'], ...
    'Aux camera tracking?', ...
    'Yes','No','Yes');
end

switch doAuxMotionTracking
    case 'Yes'
        % Get path to aux
        if isfield(output,'auxpath')
            auxpath = output.auxpath;
            % Check if it exists, otherwise look for it
            if exist(auxpath,'file') == 0
                warning(['Aux file not found at ' auxpath ...
                    '. Please select new location.']);
                % Locate file
                [FileName,PathName] = uigetfile({'*.*';'*.avi';'*.mov';...
                    '*.MOV';'*.mp4';'*.mpg';'*.mat'},'Locate video file',auxpath);
                auxpath = [PathName,FileName];
                output.auxpath = auxpath;
            end
            vv = VideoReader(auxpath);
        else
            auxpath = strrep(vidPath,'main','aux');
            [FileName,PathName] = uigetfile({'*.*';'*.avi';'*.mov';...
                '*.MOV';'*.mp4';'*.mpg';'*.mat'},'Select video file',auxpath);
            auxpath = [PathName,FileName];
            vv = VideoReader(auxpath);
            output.auxframerate = vv.FrameRate;
            output.auxpath = auxpath;
        end
        
        % Stabilize aux
        opt.laseraux = 1;
        if ~isfield(output,'auxmotioncorr')
            output.auxmotiontrackpoints = [0 0 0];
            output.auxautostab = struct();
%             [output.auxmotioncorr,output.auxmotiontrackpoints,...
%             output.auxautostab] = MotionTracker(auxpath,output.auxmotiontrackpoints,output.auxautostab,opt);
        end
        
        vid = VideoReader(vidPath);
        output.timex = vid.FrameRate/vv.FrameRate;
        [output.auxmotioncorr,output.auxmotiontrackpoints,...
            output.auxautostab] = MotionTracker(auxpath,output.auxmotiontrackpoints,output.auxautostab,opt);
        % Get offset between aux and main
        % Guess first
        
        if ~isfield(output,'auxoffset')
            [c,lags] = xcorr(detrend(medfilt1(output.motioncorr(:,3))),detrend(interp1(1:size(output.auxmotioncorr,1),medfilt1(output.auxmotioncorr(:,3)),1:1/output.timex:size(output.auxmotioncorr,1))));
            [~,ind] = max(c);
            output.auxoffset = lags(ind);   % Offset is delay of aux rel. to main
            output.auxcal = 0.2;    % Default in mm/pix
            % Plot velocity
            %         figure(3);plot(diff(movmean(medfilt1(output.auxmotioncorr(:,2),5),13)))
%             zSteps = -movmean(medfilt1(output.auxmotioncorr(:,2),5),13)*output.auxcal;   % Forward camera move in mm
            %         avV = mean(diff(zSteps))*output.auxframerate;   % Speed in mm/s
            %         intv = 0.5; % Sample every intv seconds
            %         inds = round(intv*output.auxframerate:intv*output.auxframerate:size(output.auxmotioncorr,1)-intv*output.auxframerate);
            %         inds = round(1:intv*output.auxframerate:size(output.auxmotioncorr,1));
            %         temp = movmean(medfilt1(output.auxmotioncorr(:,2),3),intv*output.auxframerate);
            %         temp = medfilt1(output.auxmotioncorr(:,2),3)
            %         temp2 = temp(inds);
            %         zSteps2 = -interp1(inds,temp2,1:numel(output.auxmotioncorr(:,2)),'spline')*output.auxcal; % Forward camera move in mm
            %         figure(3)
            %         plot([zSteps,zSteps2'])
%             plot(movmean(diff(zSteps),30))
%             output.zStepsAux = zSteps;
        end
        zSteps = -movmean(medfilt1(output.auxmotioncorr(:,2),5),13)*output.auxcal;
        output.zStepsAux = zSteps;
        [~,output] = setAuxOffset(vidPath,output);
    case 'Load'
        % Do nothing, unless "timex" is missing
        if ~isfield(output,'timex')
            vv = VideoReader(output.auxpath);
            vid = VideoReader(vidPath);
            output.timex = vid.FrameRate/vv.FrameRate;
        end
    case 'No'
        % Do nothing
end

save(par_file,'output');    % Save motion settings

%% Crop
fprintf('  Set crop settings in pop-up window...');
% [hfig,output] = setCrop(IMAGE,output);    %define cropped boundaries on image
[hfig,output] = setCrop(vidPath,output);    %define cropped boundaries on image
save(par_file,'output');    % Save motion settings

% Save cropped and extrapolated motion track
if isfield(output,'zStepsAux')
    zsAux = output.auxmotioncorr(:,2);  % Alternatively, use filtered output.zStepsAux
    zStepsRef = interp1(output.auxoffset+(1:numel(zsAux))*output.timex,...
        zsAux,output.startFrame:output.endFrame,'linear','extrap');
    if sum(isnan(zsAux))>0
        warning('NaNs in Aux correction array')
        temp = isnan(zsAux);
        arr = 1:numel(zsAux);
        zsAux(temp) = interp1(arr(~temp),zsAux(~temp),arr(temp));
    end
    if sum(isnan(zStepsRef))>0
        warning('Aux does not span entire range')
        xax = [1 output.auxoffset+(1:numel(zsAux))];
        [xax,ind,~] = unique(xax);
        yax = [zsAux(1)-output.auxoffset*mean(diff(zsAux(1:60))); zsAux];
        yax = yax(ind);
        zStepsRef = interp1(xax,...
        yax,output.startFrame:output.endFrame,'linear');
    end
    output.zStepsRef = zStepsRef;
end

save(par_file,'output');        %save post-processing parameters
fprintf(' Done, parameters saved.\n');

% Ask to export external file
doExportVideo = questdlg(['Do you want to export the cropped and '...
    'trimmed images to a video or image stack?'], ...
    'Export cropped images?', ...
    'Yes','No','Yes');
switch doExportVideo
    case 'Yes'
        
        % If external file, auto-generate a file name and location
%         if ~imgstack
            % Request file name
            [filepath,name,~] = fileparts(vidPath);
            newFileName = [filepath,filesep,name,'_crop.mp4'];
            vid = VideoReader(vidPath);
            framerate = vid.FrameRate;
%         else
%             newFileName = 'StabilizedVideo.mp4';
%             frrate = 30;
%         end
        
        % File format options
        fformats = {'*.mp4','MPEG-4 (*.mp4)';...
            '*.avi','High-quality AVI (*.avi)';...
            '*.avi','Uncompressed AVI (*.avi)';...
            '*.mj2','Archival JPEG 2000 lossless (*.mj2)';...
            '*.tif','Image stack (*.tif)';...
            '*.png','Image stack (*.png)'};
        
        [FileName,PathName,FilterIndex] = uiputfile(fformats,...
            'Export video as...',newFileName);
        
        if isfield(output,'zStepsAux')
        videoExportSpacing = questdlg(['Do you want the exported '...
            'images from the source frames or interpolated on an '...
            'equidistant grid?'], ...
            'z-direction spacing', ...
            'From source','Linear grid','From source');
        else
            videoExportSpacing = 'From source';
        end
        
        if or(strcmp(videoExportSpacing,'Linear grid'),~isfield(output,'zStepsAux'))
            videoExportSpacingInterp = inputdlg(['Z interpolation ', ...
                'multiplication factor (1 = no additional interpolation):'], ...
                'Z interpolation',[1 50],{'1'});
            Zsuperres = str2double(videoExportSpacingInterp{1});
        end
        
    case 'No'
end

%% Apply and export

% % Verify which channel to export
% expChannel = 0;
% while expChannel == 0
%     expChannel = inputdlg('Channel to export:','Confirm export channel',...
%         [1 20],{num2str(output.channelmix)});
%     expChannel = str2double(expChannel{1});
%     if expChannel < 5
%         % do something
%     else
%         expChannel = 0;
%         warndlg(['A numeric channel selection is required (1 = R, '...
%             '2 = G, 3 = B, 4 = L)'],'Warning');
%     end
% end
% output.channelmix = expChannel;

fprintf('  Applying stabilization, crop & BG subtraction...');
% IMAGEcrop = postprocIM(IMAGE,output);  %crop, threshold, and filter images
IMAGEcrop = postprocIM(vidPath,output);  %crop, threshold, and filter images
fprintf(' Done.\n');

switch doExportVideo
    case 'Yes'
        % Adjust spacing if requested
        % The linear grid
        if strcmp(videoExportSpacing,'Linear grid')
            newsz = size(IMAGEcrop);
            stepsz = (max(zStepsRef)-min(zStepsRef))/...
                (newsz(3)-1);
            stepsz = stepsz / Zsuperres;
            newZ = (min(zStepsRef):stepsz:max(zStepsRef));
            if mean(diff(zStepsRef))<0
                newZ = fliplr(newZ);
            end
            [xq,yq,zq] = meshgrid(1:newsz(2),1:newsz(1),newZ);
            [~,varr,~] = unique(zStepsRef);
            IMAGEcrop2 = interp3(1:newsz(2),1:newsz(1),zStepsRef(varr),...
                single(IMAGEcrop(:,:,varr)),xq,yq,zq);
            exportVideo(uint8(IMAGEcrop2),PathName,FileName,FilterIndex,framerate,output);
            if FilterIndex>4
                % If image stack requested, secretly save an mp4 as well
                exportVideo(uint8(IMAGEcrop2),PathName,...
                    [FileName(1:end-4) '.mp4'],1,framerate,output);
            end
            clear IMAGEcrop2 xq yq zq
            
            % Provide a figure with a calibration
            figure(2)
            plot(1:numel(zStepsRef),zStepsRef)
            hold on
            plot(1:numel(newZ),newZ)
            hold off
            legend('Tracked location','Export array locations')
            title({'Z position of main camera frames',...
                'from aux camera motiontrack',...
                ['mean: ' num2str((newZ(end)-newZ(1))/numel(newZ)) ' px/fr']})
            xlabel('frame #')
            ylabel('z (pix)')
            
        else
            if Zsuperres ~= 1
                newsz = size(IMAGEcrop);
                stepsz = 1;
                stepsz = stepsz / Zsuperres;
                newZ = 1:stepsz:newsz(3);
                [xq,yq,zq] = meshgrid(1:newsz(2),1:newsz(1),newZ);
                IMAGEcrop2 = interp3(1:newsz(2),1:newsz(1),1:newsz(3),...
                    single(IMAGEcrop),xq,yq,zq);
                exportVideo(uint8(IMAGEcrop2),PathName,FileName,FilterIndex,framerate,output);
            else

                exportVideo(IMAGEcrop,PathName,FileName,FilterIndex,framerate,output);
            end
            %             % If image stack requested, secretly save an mp4 as well
            %             exportVideo(uint8(IMAGEcrop),PathName,...
            %                 [FileName(1:end-4) '.mp4'],1,framerate,output);
        end
    case 'No'
end
[~,name,ext] = fileparts(cropPath);
fprintf(['  Saving processed file ' name ext '...']);
save(cropPath,'IMAGEcrop','-v7.3');        % save processed images

% Do it without bg subtraction, too
if strcmp(doExportVideo,'Yes') && (output.bgmedfilt>1 || output.bgminfilt>1)
    bgmedfiltold = output.bgmedfilt;
    output.bgmedfilt = 0;
    bgminfiltold = output.bgminfilt;
    output.bgminfilt = 0;
    % IMAGEcrop = postprocIM(IMAGE,output);  %crop, threshold, and filter images
    IMAGEcrop = postprocIM(vidPath,output);  %crop, threshold, and filter images
    FileName = [FileName(1:end-4) '_nobgsub' FileName(end-3:end)];
    % Adjust spacing if requested
    if strcmp(videoExportSpacing,'Linear grid')
        [xq,yq,zq] = meshgrid(1:newsz(2),1:newsz(1),newZ);
        [~,varr,~] = unique(zStepsRef);
        IMAGEcrop2 = interp3(1:newsz(2),1:newsz(1),zStepsRef(varr),...
            single(IMAGEcrop(:,:,varr)),xq,yq,zq);
        exportVideo(uint8(IMAGEcrop2),PathName,FileName,FilterIndex,framerate,output);
        clear IMAGEcrop2 xq yq zq
    else
        if Zsuperres ~= 1
            IMAGEcrop2 = interp3(1:newsz(2),1:newsz(1),1:newsz(3),...
                single(IMAGEcrop),xq,yq,zq);
            exportVideo(uint8(IMAGEcrop2),PathName,FileName,FilterIndex,framerate,output);
        else
            exportVideo(IMAGEcrop,PathName,FileName,FilterIndex,framerate,output);
        end
    end
save(cropnonBGPath,'IMAGEcrop','-v7.3');        % save processed images
end

fprintf(' Done.\n');
clear IMAGE; clear output % (because we've corrupted it to export nobgsub)


ThreeDRCurrent = 0;
disp('ThreeDR_CropPreProc.m finished.');


%% Functions
% Export stabilized video
    function exportVideo(IMAGEcrop,PathName,FileName,FilterIndex,framerate,output)
        
        if FilterIndex > 4         % If exporting image stack
            [~,FileName,ext] = fileparts(FileName);
            mkdir([PathName FileName]);
            baseFileName = [PathName, FileName, filesep, FileName, ...
                '_'];
            newsidecarfile = [PathName, FileName, filesep, FileName, ...
                '_exportinfo.txt'];
        else                        % If exporting video
            newFileName = [PathName, FileName];
            [~,temp,~] = fileparts(FileName);
            newsidecarfile = [PathName, temp, '_exportinfo.txt'];
            % Build video file
            switch FilterIndex
                case 1
                    v = VideoWriter(newFileName,'MPEG-4' );
                    v.Quality = 90;
                case 2
                    v = VideoWriter(newFileName,'Motion JPEG AVI' );
                    v.Quality = 100;
                case 3
                    v = VideoWriter(newFileName,'Uncompressed AVI' );
                case 4
                    v = VideoWriter(newFileName,'Archival' );
            end
            v.FrameRate = framerate;
            open(v);
        end
        
        % Write side car file
        fileID = fopen(newsidecarfile,'w');
        fns = fieldnames(output);
        for i=1:numel(fns)
            if size(output.(fns{i}),2) == 1 && ~isstruct(output.(fns{i}))
                if ischar(output.(fns{i}))
                    fprintf(fileID,[fns{i} ':\t' output.(fns{i}) '\n']);
                else
                    if numel(output.(fns{i})) == 1
                        fprintf(fileID,[fns{i} ':\t' num2str(output.(fns{i})) '\n']);
                    end
                end
            end
        end
        if exist('VideoExportSpacing','var')
            fprintf(fileID,['VideoExportSpacing:\t' VideoExportSpacing '\n']);
        else
            fprintf(fileID,['VideoExportSpacing:\t' 'From source' '\n']);
        end
        if exist('videoExportSpacingInterp','var')
            fprintf(fileID,['VideoExportSpacingInterp:\t' videoExportSpacingInterp '\n']);
        else
            fprintf(fileID,['VideoExportSpacingInterp:\t' 'None' '\n']);
        end
        fclose(fileID);
        
        
        fprintf('  Exporting...  ');
        newtxt = [num2str(0) '%%'];
        fprintf(newtxt);
        for k = 1:size(IMAGEcrop,3)
            oldtxt = newtxt;
            newfr = IMAGEcrop(:,:,k);
            
            % Save frame
            if FilterIndex > 4
                % Make new file name
                newFileName = [baseFileName, num2str(k-1,...
                    ['%0' num2str(ceil(log10(size(IMAGEcrop,3)))) 'u']), ext];
                % Save as image
                imwrite(newfr,newFileName);
            else
                writeVideo(v,newfr);
            end
            newtxt = [num2str(100*k/(size(IMAGEcrop,3)),'%.0f') '%%'];
            fprintf(repmat('\b',1,numel(oldtxt)-1));
            fprintf(newtxt);
        end
        
        if FilterIndex < 5
            close(v);
        end
        
        fprintf('\nExport complete.\n');
    end
