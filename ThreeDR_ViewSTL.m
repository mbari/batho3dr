
%% Define and load some global variables
global ThreeDRCurrent       % Active analysis status
global temppath             % Temporary folder location
global cfig

ThreeDRCurrent = 5;     % Step 4 of the process

if or(isempty(temppath),isempty(cfig))  % Run Main if needed
    ThreeDR_Main;
else
    updateMainButtons(ThreeDRCurrent);
end


%% Open viewer

view3D;

%% Finish
ThreeDRCurrent = 0;


% % disp('Plotting 3D reconstruction');
% h=figure(3);
% set(h,'Position',[100,100,1000,1000]);
% % disp(['  Plotting isosurface data with val = ',num2str(val),'...']);
% hiso=patch(fv,'FaceColor',[0.5,0.5,0.65],'EdgeColor','none');
% isonormals(IMAGEsm,hiso)
% alpha(0.2)  %sets level of transparency
% view(3)
% camlight
% lighting gouraud
% grid on
% daspect([1 1 1]);
% pause(1)
% % savefig([outdata(1:end-4),'.fig']);