%% Define and load some global variables
global ThreeDRCurrent       % Active analysis status
global temppath             % Temporary folder location
global cfig

ThreeDRCurrent = 3;     % Step 3 of the process

if or(isempty(temppath),isempty(cfig))  % Run Main if needed
    ThreeDR_Main;
else
    updateMainButtons(ThreeDRCurrent)
end

%% 

[filename, pathname] = ...
     uigetfile([temppath filesep '*.mat'],'Select mask to edit');
load([pathname filesep filename],'MASKfilter');     % Load mask

% Guess locations of other files
ind = strfind(filename,'feature');
cropfile = [filename(1:ind-1) 'IMAGEcrop.mat'];
% cropfile = [filename(1:ind-1) 'IMAGEcropnonbg.mat'];
ind = strfind(filename,'MASK');
paramfile = [filename(1:ind-1) 'params.mat'];

try
    load([pathname filesep cropfile],'IMAGEcrop');  % Load cropped image
catch
    [filename2, pathname2] = uigetfile([temppath filesep '*.mat'],...
        'Select background image file');
    load([pathname2 filesep filename2],'IMAGEcrop');     % Load cropped img
end

while sum(abs(size(MASKfilter)-size(IMAGEcrop)))~=0
    [filename2, pathname2] = uigetfile([temppath filesep '*.mat'],...
        'Select background image file');
    fl = load([pathname2 filesep filename2]);
    names = fieldnames(fl);
    eval(['IMAGEcrop = double(fl.' names{1} ');']);
end

try
    load([pathname filesep paramfile],'output');  % Load output settings
    manualMaskEditor(MASKfilter,IMAGEcrop,output);
catch
    manualMaskEditor(MASKfilter,IMAGEcrop);
end

ThreeDRCurrent = 0;
% disp([mfilename '.m finished.']);