## Info
This repository contains programs to develop 3D models from image stacks. Specifically, it is focused on converting video files recorded with the *DeepPIV* imaging system, using a transverse laser sheet to illuminate just a slice of a semi-transparent animal, and producing an STL file for further analysis.
Initially the main purpose of this code is to study the complex geometries of the house-like mucus structures secreted by deep-sea larvaceans of the genus *Bathochordaeus.*

Many aspects of this code require the Image Processing Toolbox. The code was developed and tested on MATLAB version 9.3 (R2017b) with Image Processing Toolbox version 10.1, on macOS Mojave. This package is published on https://bitbucket.org/mbari/batho3dr but will likely not be updated as it serves as a snapshot of the code used for the material presented in mutliple publications. A newer version may be published in the future on https://bitbucket.org/mbari/

## How to use this code
1. Run ThreeDR_Main.m. It will prompt to select a folder for temporary files the first time it runs. It is highly recommended that you select a folder on a fast drive with at least 20 GB free space, depending on the size of the files you intend to process. This program also creates a settings *.mat file for future reference, storing the location of the temporary folder. This can be changed via the 'Settings' button in 3DR Main.
2. The 3DR Main menu takes you stepwise from video file to STL file by clicking the buttons in sequence and following the steps. Only "Step 3: Edit mask" and "Step 5: View STL file" are optional, but you can start at a later step if you already have performed the first steps at an earlier time. The section below details what happens in the individual steps.

### Step 1: Pre-process video
This calls ThreeDR_CropPreProc.m - loads video file, creates a local copy and allows preprocessing (cropping, basic background subtraction) of the image stack.

+ calls loadVideoFile.m - this locates the video and stores a copy in the temporary folder.
+ optionally calls MotionTracker.m for simple image stabilization.
+ calls setCrop.m for cropping, trimming and basic background subtraction.
+ calls postprocIM.m to apply the crop, trim and filter.
+ saves file [dataset]_IMAGE.mat - this is the unmodified image stack.
+ saves file [dataset]_IMAGEcrop.mat - this is the image stack with the crop settings applied.
+ saves file [dataset]_IMAGEcrop_nonbg.mat - this is the image stack with the crop settings applied, except for background subtraction.
+ saves file [dataset]_params.mat - this contains crop, stabilization and other settings

### Step 2: Feature selection
This calls ThreeDR_FeatureSelect.m - allows curves adjustment, selection of min/max threshold and median filter options to create the initial luminosity mask for the desired features.

+ calls loadVideoFile.m to retrieve the desired image stack.
+ calls manipulate_imops3D_multiple.m - this program loads a GUI that allows curves adjustments (uses curves_adj_mono.m), a 3D median filter to reduce noise and luminosity thresholds for one or more features.
+ calls apply_imops3D.m for each feature; this applies image operations for selected thresholds.
+ saves settings in the 'params.mat file.
+ saves file [dataset]_[feature]_MASK.mat for each feature - this is the binary mask.
+ saves file [dataset]_[feature]_IMAGE.mat for each feature - this is the image with the mask applied.
+ saves file [dataset]_[feature]_params.mat for each feature - contains pre-processing plus filter and threshold settings.
+ saves file [dataset]_[feature]_IMAGEcurv.mat - this is the image with the curves applied.
+ saves file [dataset]_[feature]_IMAGEcurvmedfilt.mat - this is the image with the curves applied, as well as the median filter.

### Step 3: Manual mask edits
Step 3 uses ThreeDR_EditMask.m, which calls manualMaskEditor.m for an interface allowing manual modifications of the binary mask, applied either to multiple or single frames, and both inclusion and removal from the mask. The module shows a base image with the mask overlaid in green. The opacity of both the background as well as the overlay can be adjusted to help in visualization. Other masks can be added and subtracted, and when pixels are manually excluded from the primary mask, they are kept in memory in a secondary mask (and shown in red), to allow inclusion. The user can apply a median filter and morphological operations to the mask; these apply to all frames! After clicking "apply filter to stack", the user can do manual freeform exclusion, inclusion (from red mask) and addition operations to a single frame (slice) or multiple at once by increasing the "z-extent". Note that the stack-wide filter can be edited but then manual modifications are lost. When using the manual mask editing options, the program waits for user input and operates as soon as areas are selected. This can result in warnings and errors in MATLAB's command window, but those can typically be ignored.

+ calls editblobs3D.m, where blobs (connected areas in the mask) are selected and included or removed. Note that z-extent applied to these operations, and a large extent makes this operation slow.
+ calls editpix3D.m, where pixels are included, removed, or added. Note that z-extent applied to these operations, and a large extent makes this operation slow.
        
### Step 4: Create STL model
An STL file is created from the masks and images by running ThreeDR_CreateSTL.m, which in turn calls the subfunction maskToIsoSurf.m. This program takes a user-defined mask and image stack, requests calibration values (which are then stored in the 'params.mat' file for later use), and asks for an isovalue to build the mesh. Multiple comma-separated values can be entered to create multiple meshed with different isovalues. The software then references the points and uses the isosurface command to create a model.

+ calls the function stlwrite2018.m by Sven Holcombe (https://www.mathworks.com/matlabcentral/fileexchange/20922-stlwrite-write-ascii-or-binary-stl-files)

### Step 5: View STL model
ThreeDR_ViewSTL.m is a simple program to view STL files in MATLAB, and allows visualization and view changes.

+ calls view3D.m which contains the actual viewer interface.
+ calls STLread.m by Eric Johnson.

## Included files
##### Main functions
+ ThreeDR_CreateSTL.m - Create STL file from binary mask file and image stack.
+ ThreeDR_CropPreProc.m - Allows selection and pre-processing of video files.
+ ThreeDR_EditMask.m - Loads interface for manual mask editing.
+ ThreeDR_FeatureSelect.m - Initial mask generation from luminosity thresholds.
+ ThreeDR_Main.m - Loads main window to aid in navigation.
+ ThreeDR_ViewSTL.m - Simple viewing and visualizaing of entire models and multiple STLs.
##### Subfunctions
+ apply_imops3D.m - Applies image operations for selected thresholds.
+ create_curvmap.m - Tool to quickly perform curves remapping.
+ curves_adj_mono.m - Loads an interface to allow curves adjustments.
+ editblobs3D.m - Selection, inclusion, removal of connected groups of voxels in the 3D mask.
+ editpix3D.m - Inclusion, removal and manual addition of (groups of) pixels in the binary mask.
+ generalEditorWindow.m - Loads empty interface window; this is used for various GUIs.
+ loadVideoFile.m - Loads interface for selecting video file from temporary folder, or importing a new one.
+ manipulate_imops3D_multiple.m - Calculate initial masks from filter and threshold settings.
+ manualMaskEditor.m - GUI to perform global and local adjustments on a binary mask.
+ maskToIsoSurf.m - Calculates an isosurface model from calibrated binary masks.
+ MotionTracker.m - Basic tool for motion tracking and correction.
+ postprocIM.m - Applies preprocessing steps including trim and crop.
+ setCrop.m - Loads interface for cropping, trimming and background subtraction of video.
+ setthresh.m - Applies luminosity thresholds to image.
+ stlread.m - Reads STL files.
+ stlwrite2018.m - Converts a MATLAB FV struct to STL file.
+ view3D.m - Simple 3D STL/PLY file viewer interface.

## Authors
This code was developed by Joost Daniels and Kakani Katija at the Monterey Bay Aquarium Research Institute: http://www.mbari.org.
STLread.m was written by Eric Johnson: https://www.mathworks.com/matlabcentral/fileexchange/22409-stl-file-reader.
STLwrite.m was written by Sven Holcombe: https://www.mathworks.com/matlabcentral/fileexchange/20922-stlwrite-write-ascii-or-binary-stl-files
