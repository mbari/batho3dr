%% Information
% This program (called by ThreeDR_Main) allows the user to generate
% starting masks by doing curves adjustments to the footage and selecting
% luminosity ranges to be identified as certain features.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 11th, 2018


%% Define and load some global variables
global ThreeDRCurrent       % Active analysis status
global temppath             % Temporary folder location
global cfig

ThreeDRCurrent = 2;     % Step 2 of the process

if or(isempty(temppath),isempty(cfig))  % Run Main if needed
    ThreeDR_Main;
else
    updateMainButtons(ThreeDRCurrent)
end


%% Get data locations

% Select videos copied to the temporary folder, or browse to a new one
[matPath] = loadVideoFile;

% If matPath is not a valid file, abort
if exist(matPath,'file')==0
    return
end
% load(matPath, 'IMAGE');

% Get crop image file name
[fpath, fname, ext] = fileparts(matPath);
cropPath = [fpath, filesep, fname, 'crop', ext];
cropnonBGPath = [fpath, filesep, fname, 'crop_nonbg', ext];
% Crop settings file name
par_file = [fpath, filesep, strrep(fname,'_IMAGE',''), '_params.mat'];


%% Load files

if exist(cropPath,'file')==2
    fprintf('  Loading image stacks...');
    load(cropPath);
    load(par_file);
    fprintf(' Done.\n');
else
    error(['  Cropped image stack does not exist. Run ',...
        'ThreeDR_CropPreProc.m.']);
end


%% Load GUIs to select settings and initialize masks

temp = struct2cell(dir(fpath));     % Get folder content
nmlst = temp(1,:)';     % List of filenames in folder
% Get indices of files with 'feature' AND 'params'
featfiles = contains(nmlst,'feature') .* contains(nmlst,'params');

if sum(featfiles) > 0     % See if files with text 'feature' exist
    
    % Construct a questdlg with two options
    choice = questdlg(['One or more mask setting files were already ',...
        'detected. Would '...
        'you like to create a new one or edit an existing feature?'], ...
        'Feature selection', ...
        'Edit','New file','New file');
    
    % Handle response
    switch choice
        case 'Edit'
            featPath = loadFeature(fpath);
            load(featPath);      % 'output' should be loaded from file
        case 'New file'
            % Nothing needs to be loaded
    end
end

% Load GUI to generate feature masks
[hfig,output] = selectFeatures(IMAGEcrop,output);

basename = [fpath, filesep, strrep(fname,'_IMAGE','')];
for featnum=1:size(output.names,1)
    % save post-processing parameters
    save([basename,'_feature_',...
        output.names{featnum},'_params.mat'],'output');
end


%% Apply settings to entire clip automatically
fprintf('  Applying settings to entire clip... ')

numfeats = numel(output.minthresh);
IMAGEfilter = IMAGEcrop.*0;
MASKfilter = IMAGEcrop.*0;

numframes = size(IMAGEcrop,3);
start = 1;
curvmap = create_curvmap(output.curves);
sizeImtemp = size(IMAGEcrop(:,:,1));

% Do all the general operations first, to save time

% Apply curves
fprintf(' curves...');
curvimg = double(IMAGEcrop);            % Allows double and not just integer values for better bit depth
[uniqvals,~,ic] = unique(curvimg);
newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
for i=1:numel(uniqvals)
    newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
end
im = reshape(newuniqvals(ic),size(IMAGEcrop));
IMAGEcurv = im; %#ok<NASGU>
    save([basename,'_feature_',output.names{1},'_IMAGEcurv.mat'],'IMAGEcurv','-v7.3');        % save curve-applied image
clear IMAGEcurv

% 3D median filter
fprintf(' median filter... ');
medfilt=output.filt;
medfilt3D=output.filt3D;
if or(medfilt>1,medfilt3D>1)
    im = medfilt3(im,[medfilt medfilt medfilt3D],'symmetric');
end
IMAGEcurvmedfilt=im; %#ok<NASGU>
save([basename,'_feature_',output.names{1},'_IMAGEcurvmedfilt.mat'],'IMAGEcurvmedfilt','-v7.3');        % save curve-applied & medfilt image
clear IMAGEcurvmedfilt

%%
outputtemp = output;    % Temporary output settings to ignore medfilt in later apply_imops3D function call
outputtemp.filt = 1; outputtemp.filt3D = 1;
outputtemp.curves = [0 0; 127 127; 255 255];

for featnum = 1:numfeats
    featprogtxt = sprintf('feature %d/%d... ',featnum,numfeats);
    fprintf(featprogtxt);
    for i=start:1:size(IMAGEcrop,3)     % Loop through all frames
        tic
        % Update status text
        progtxt = sprintf('frame %d/%d',i,numframes);
        progtxtsz = size(progtxt,2);
        fprintf(progtxt);
        pause(0.001);
        
        [imnew,mask]=apply_imops3D(im(:,:,i),outputtemp,featnum);
        imorig = IMAGEcrop(:,:,i);
        
%         % Apply curves to original
%         curvimg = double(imorig);            % Allows double and not just integer values for better bit depth
%         [uniqvals,~,ic] = unique(curvimg);
%         newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
%         for tt=1:numel(uniqvals)
%             newuniqvals(tt) = (curvmap(uniqvals(tt)==curvmap(:,1),2));
%         end
%         imorig = reshape(newuniqvals(ic),sizeImtemp);
        
        figure(2)
%         imshow([imorig,imnew]);
        imshowpair(imorig,imnew,'method','montage');
        
        pause(0.05)
        IMAGEfilter(:,:,i) = imnew;
        MASKfilter(:,:,i) = mask;
        
        fprintf(repmat('\b',1,progtxtsz));  % Remove current frame number from command window
    end
%     [basename,'_feature_',output.names(i),'_params.mat']
    im_out=[basename,'_feature_',output.names{featnum},'_IMAGE.mat'];
    mask_out=[basename,'_feature_',output.names{featnum},'_MASK.mat'];
    save(im_out,'IMAGEfilter','-v7.3');
    save(mask_out,'MASKfilter','-v7.3');
    
    fprintf(repmat('\b',1,size(featprogtxt,2)));  % Remove current feature number
end
fprintf(' completed.\n');
for featnum = 1:numfeats
    disp(['  ' output.names{featnum} ' mask saved.']);
end
close(2)
ThreeDRCurrent = 0;
disp([mfilename '.m finished.']);