
%% Define and load some global variables
global ThreeDRCurrent       % Active analysis status
global temppath             % Temporary folder location
global cfig

ThreeDRCurrent = 4;     % Step 4 of the process

if or(isempty(temppath),isempty(cfig))  % Run Main if needed
    ThreeDR_Main;
else
    updateMainButtons(ThreeDRCurrent);
end

%% Make STL file

stlPath = maskToIsoSurf;

%% Finish
ThreeDRCurrent = 0;
disp([mfilename '.m finished.']);