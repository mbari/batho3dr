function f = compareChannelMixing(imgarr)
%compareChannelMixing shows different ways of mixing available color channels
f = figure('Name','Choose channel to use, close window to continue');

imsz = size(imgarr);

for i = [1:size(imgarr,3),4]
    subplot(2,2,i)
    if i < 4
        im = imgarr(:,:,i);
        titletxt = ['Channel ' num2str(i)];
    else
        im = rgb2gray(imgarr);
        titletxt = 'Luminance';
    end
    imagesc(im)
    colorbar
    
    % Select smaller section in center
    mag = 4;    % Magnification
    select = 0.1;   % Fraction of dimension to be selected
    
    xsel = round((0.5-select/2)*imsz(2)):round((0.5+select/2)*imsz(2));
    ysel = round((0.5-select/2)*imsz(1)):round((0.5+select/2)*imsz(1));
    pos = [xsel(1) ysel(1) xsel(end)-xsel(1) ysel(end)-ysel(1)];
    
    hold on
    rectangle('Position',pos,'EdgeColor','r','LineStyle',':',...
        'LineWidth',1.5);
    xsel2 = (imsz(2)-mag*pos(3)):imsz(2);
    ysel2 = 1:mag*pos(4);
    subimg = im(ysel,xsel);
    image(xsel2,ysel2,subimg);
    pos2 = [min(xsel2) min(ysel2) mag*pos(3) mag*pos(4)];
    rectangle('Position',pos2,'EdgeColor','r','LineStyle','-',...
        'LineWidth',1.5);
    hold off
    
    title(titletxt);
end
colormap jet

end

