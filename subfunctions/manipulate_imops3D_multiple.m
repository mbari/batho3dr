function hfig=manipulate_imops3D_multiple(IMAGE,output)

%% Initialize window settings

if isstruct(output) && isfield(output,'curves')
    % Load settings automatically
    curvpoints = output.curves;
    numfeatures = numel(output.names);
    disp('here')
else
    output.filt=1;
    output.filt3D=1;
    curvpoints = [0 0; 127 127; 255 255];
    
    numfeatures = 4;
    threshinit = round(255/(numfeatures+1):255/(numfeatures+1):255);
    output.minthresh = threshinit(1:end-1)+1;
    output.maxthresh = threshinit(2:end);
    output.holesize = zeros(numfeatures,1) + 1;
    output.names = cellstr([repmat('feat',numfeatures,1) num2str((1:numfeatures)')]);
end

% Create curves mapping
curvmap = create_curvmap(curvpoints);

sizeIm=size(IMAGE);
wa=get(0,'screensize');
screenwidth=wa(3);screenheight=wa(4);
minSlice=1;maxSlice=sizeIm(3);startSlice=round(mean([minSlice maxSlice]));
% minThresh=0;maxThresh=255;
% startThresh=output.thresh;
% minThresh1=0;maxThresh1=255; startThresh1=output.thresh1;
threshlim1=0;threshlim2=255;
minFilt=1;maxFilt=20;startFilt=output.filt;
minFilt3D=1;maxFilt3D=21;startFilt3D=output.filt3D;
minSize=1;maxSize=200;
% startSize=output.size;
smallStep=1/sizeIm(3);
largeStep=0.1;
sliderlength=300;
textlength=200;
spacer=150;

% Set initial OUTER window size
outerwidth = sizeIm(2)+sliderlength + 150;
outerheight = sizeIm(1)+150;
if or(outerwidth>screenwidth,outerheight>screenheight)
    outerwidth=screenwidth-70;
    outerheight=screenheight-120;
    xpos = 50;
    ypos = 100;
else
    xpos = (screenwidth-outerwidth)/2;
    ypos = (screenheight-outerheight)/2;
end


%% Build main window and controls

% Build window
hfig=figure('Menubar','none','Toolbar','none','Name','Slice Viewer',...
    'NumberTitle','on','IntegerHandle','off','Units','Pixels',...
    'OuterPosition',[xpos ypos outerwidth outerheight]);

% Determine inner size and set up other dimensions
temp = get(hfig,'InnerPosition');
width = temp(3); height = temp(4);
sliderleft = width - sliderlength - 20;
figsizex = sliderleft - 40;
figsizey = height - 80 - 60;
if or(sizeIm(2)<figsizex,sizeIm(1)<figsizey)
    figsizex = sizeIm(2);
    figsizey = sizeIm(1);
end

% Build axes
haxes = axes(hfig,'Units','Pixels','Position',[20 80 figsizex figsizey]);

% Curves button
curvbutt=uicontrol('Parent',hfig,'Style','pushbutton','Callback',@curvbutt_Callback,...
    'String','Curves adjustments','Units','pixels',...
    'Position',[sliderleft+(sliderlength-spacer*1.3)/2 height-80 spacer*1.3 30],...
    'Fontsize',16);

htxt3=uicontrol('Style','text','Position',[sliderleft height-120 textlength 20],...
    'String',['2D median filter size: ',num2str(startFilt)],'HorizontalAlignment','left');
slider3=uicontrol('Parent',hfig,'Style','slider','Callback',@slider3_Callback,...
    'String','Threshold','Units','pixels','Position',[sliderleft height-140 sliderlength 20],...
    'SliderStep',[0.1 0.3]);
set(slider3,'Min',minFilt,'Max',maxFilt,'Value',startFilt)

htxt4=uicontrol('Style','text','Position',[sliderleft height-180 textlength 20],...
    'String',['3D median filter size: ',num2str(startFilt)],'HorizontalAlignment','left');
slider4=uicontrol('Parent',hfig,'Style','slider','Callback',@slider4_Callback,...
    'String','Threshold','Units','pixels','Position',[sliderleft height-200 sliderlength 20],...
    'SliderStep',[0.1 0.3]);
set(slider4,'Min',minFilt3D,'Max',maxFilt3D,'Value',startFilt3D)

viewmodecontr = uicontrol('Parent',hfig,'Style','popupmenu',...
    'Callback',@updateImage,'String',{'Multi-feature view','Single feature view','Full image'},...
    'Units','pixels','Position',[sliderleft height-240 sliderlength 20],'Value',1);

% Set number of features
numfeattxt=uicontrol('Style','text','Position',[sliderleft height-280 110 20],...
    'String','Number of features: ','HorizontalAlignment','left');
numfeatcontr = uicontrol('Parent',hfig,'Style','edit',...
    'Callback',@changeNumFeatures,'String',num2str(numfeatures),...
    'Units','pixels','Position',[sliderleft+110 height-280 40 20]);


%% Feature tabs

tabheight = min(260,height-370);
numlines = 4;       % Number of parameters inside tab
tablineheight=(tabheight-80)/(numlines*3);
tabinset = 20;
tablinewidth = sliderlength-2*tabinset-20;
threshstepsmall = 1/256;
threshsteplarge = 15/256;

tabgp = uitabgroup(hfig,'Units','Pixels',...
    'Position',[sliderleft 120 sliderlength tabheight],...
    'SelectionChangedFcn',@updateImage);
for i=1:numfeatures
feattab(i) = uitab(tabgp,'Title',output.names{i},'Tag',['Feat' num2str(i)]);
% tab2 = uitab(tabgp,'Title','Feat2');
% tab3 = uitab(tabgp,'Title','Feat3');

uifeatnametxt(i)=uicontrol(feattab(i),'Style','text','Units','Pixels',...
    'Position',[tabinset 10+tablineheight*11 tablinewidth tablineheight],...
    'String','Feature name:','HorizontalAlignment','left');
uifeatname(i)=uicontrol(feattab(i),'Style','edit',...
    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
    'String',output.names(i),'Units','pixels',...
    'Position',[tabinset 10+tablineheight*10 tablinewidth tablineheight],...
    'HorizontalAlignment','left');

uithreshmintxt(i)=uicontrol(feattab(i),'Style','text','Units','Pixels',...
    'Position',[tabinset 10+tablineheight*8 tablinewidth tablineheight],...
    'String',['Min Threshold: ',num2str(output.minthresh(i))],'HorizontalAlignment','left');
uithreshmin(i)=uicontrol(feattab(i),'Style','slider',...
    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
    'String',['Min Threshold Feature ' num2str(i)],'Units','pixels',...
    'Position',[tabinset 10+tablineheight*7 tablinewidth tablineheight],...
    'SliderStep',[threshstepsmall threshsteplarge]);
set(uithreshmin(i),'Min',threshlim1,'Max',threshlim2,'Value',output.minthresh(i));

uithreshmaxtxt(i)=uicontrol(feattab(i),'Style','text','Units','Pixels',...
    'Position',[tabinset 10+tablineheight*5 tablinewidth tablineheight],...
    'String',['Max Threshold: ',num2str(output.maxthresh(i))],'HorizontalAlignment','left');
uithreshmax(i)=uicontrol(feattab(i),'Style','slider',...
    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
    'String',['Max Threshold Feature ' num2str(i)],'Units','pixels',...
    'Position',[tabinset 10+tablineheight*4 tablinewidth tablineheight],...
    'SliderStep',[threshstepsmall threshsteplarge]);
set(uithreshmax(i),'Min',threshlim1,'Max',threshlim2,'Value',output.maxthresh(i));

uisizetxt(i)=uicontrol(feattab(i),'Style','text','Units','Pixels',...
    'Position',[tabinset 10+tablineheight*2 tablinewidth tablineheight],...
    'String',['Hole size: ',num2str(output.holesize(i))],...
    'HorizontalAlignment','left','Enable','off');
uisize(i)=uicontrol(feattab(i),'Style','slider',...
    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
    'String',['Hole size Feature ' num2str(i)],'Units','pixels',...
    'Position',[tabinset 10+tablineheight*1 tablinewidth tablineheight],...
    'Enable','off');
set(uisize(i),'Min',minSize,'Max',maxSize,'Value',output.holesize(i));

end


%% Other GUI controls

% Slice selector
htxt1=uicontrol('Style','text','Units','pixels',...
    'Position',[20+(sizeIm(2)-sliderlength)/2 40 textlength 20],...
    'String',['Slice: ',num2str(startSlice)],'HorizontalAlignment','left');
slider1=uicontrol('Parent',hfig,'Style','slider','Callback',@slider1_Callback,...
    'String','Slice Number','Units','pixels',...
    'Position',[20+(sizeIm(2)-sliderlength)/2 20 sliderlength 20],'SliderStep',[smallStep largeStep]);
set(slider1,'Min',minSlice,'Max',maxSlice,'Value',startSlice)

% Finish button
button=uicontrol('Parent',hfig,'Style','pushbutton','Callback',@button_Callback,...
    'String','Finish','Units','pixels','Fontsize',15,...
    'Position',[sliderleft+(sliderlength-spacer)/2 40 spacer 40]);

% Updating preview indicator
updtxt = uicontrol('Style','text','Position',[20+(sizeIm(2)-sliderlength)/2 height-60 sliderlength 30],...
    'String','Updating preview...','BackgroundColor',[0.8 0.8 0.8],...
    'HorizontalAlignment','center','FontWeight','bold','visible','off','Fontsize',16);
uistack(updtxt,'top')

% Initialize a few global parameters
% curvimg = 0; 

updateImage


%%
    function []=updateImage(~,~,~)
        set(updtxt,'Visible','on');
        pause(0.005);
        
        slice=get(slider1,'Value');
        medfilt=get(slider3,'Value');
        medfilt3D=get(slider4,'Value');
        im=IMAGE(:,:,(slice-(medfilt3D-1)/2):(slice+(medfilt3D-1)/2));
        sizeImtemp = size(im);
        
        % Apply curves
        curvimg = double(im);            % Allows double and not just integer values for better bit depth
        [uniqvals,~,ic] = unique(curvimg);
        newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
        for i=1:numel(uniqvals)
            newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
        end
        im = reshape(newuniqvals(ic),sizeImtemp);
        
        % 3D median filter
        im = medfilt3(im,[medfilt medfilt medfilt3D],'symmetric');
        im = im(:,:,medfilt3D);     % Show only middle part
        
        switch viewmodecontr.Value
            case 1 %'Multi-feature view'
                % Mask out sections
                newim = zeros(size(im));
                colors = jet(numfeatures);
                colors = [0 0 0;colors];
                
%                 newim = zeros(size(im,1),size(im,2),3);
        
                for ii=1:numfeatures
                    mask=bwareaopen(bwmorph(bwmorph(setthresh(im,...
                        output.minthresh(ii),output.maxthresh(ii)),'spur'),'close'),output.holesize(ii));
%                     mask3D = false(size(newim));
%                     mask3D(:,:,1) = mask;
%                     mask3D(:,:,2) = mask;
%                     mask3D(:,:,3) = mask;
%                     type mask3D
%                     tempim = im(logical(mask));
                    % Set the right colors here!!! Choose between overlay
                    % (luminosity levels within feature visible) or
                    % high-contrast
                    
                  
%                     newim(mask) = im(mask)*(ii/numfeatures)+(ii-1)*255/numfeatures;
                    newim(mask)=ii;
%                     newim(mask3D) = colors(ii,:);
%                     newim(:,:,1) = logical(mask)*colors(ii,1);
%                     newim(:,:,2) = logical(mask)*colors(ii,2);
%                     newim(:,:,3) = logical(mask)*colors(ii,3);
                    feattab(ii).ForegroundColor = colors(ii+1,:);
                end
%                 unique(newim.*im)
                imshow(newim,[]);
                colormap(haxes,colors);
                
            case 2 %'Single feature view'
                % Mask out sections
                ii = str2double(tabgp.SelectedTab.Tag(end));
%                 mask=bwareaopen(bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
% output.maxthresh(ii)),'spur'),'close'),output.holesize(ii));
mask=bwareaopen(bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
    output.maxthresh(ii)),'spur'),'close',output.holesize(ii)),1);
%                 mask=bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
% output.maxthresh(ii)),'spur'),'close');
%                 se = strel('disk',output.holesize(ii));
%                 mask = ~(imerode((~mask),se));
%                 imt=setthresh(im,output.minthresh(ii),output.maxthresh(ii));
% save('imtemp.mat','imt');


% Perhaps a combination of the following:
% - 
                imshow(im.*mask,[]);
                colormap(haxes,gray);
                
                % Set colors of tabs
                for ii=1:numfeatures
                    feattab(ii).ForegroundColor = [0 0 0];
                end
                
            case 3 %'Full image'
                imshow(im,[]);
                colormap(haxes,gray);
                % Set colors of tabs
                for ii=1:numfeatures
                    feattab(ii).ForegroundColor = [0 0 0];
                end
        end
        set(updtxt,'Visible','off');
    end

%%
    function []=changeNumFeatures(~,~,~)
        numfeatures = str2double(numfeatcontr.String);
        
        % Update edges
            threshinit = round(255/(numfeatures+1):255/(numfeatures+1):255);
            output.minthresh = threshinit(1:end-1)+1;
            output.maxthresh = threshinit(2:end);
        
        if numfeatures < numel(feattab)
            % Remove one or more tabs
            delete(feattab(numfeatures+1:end));
            output.names(numfeatures+1:end) = [];
            output.holesize(numfeatures+1:end) = [];
            
            feattab(numfeatures+1:end)=[];
        elseif numfeatures > numel(feattab)
            % Add one or more tabs
            
            for ix=numel(feattab)+1:numfeatures
                output.names(ix) = {['Feat',num2str(ix)]};
                output.holesize(ix) = 1;
                feattab(ix) = uitab(tabgp,'Title',output.names{ix},'Tag',['Feat' num2str(ix)]);
                
                uifeatnametxt(ix)=uicontrol(feattab(ix),'Style','text','Units','Pixels',...
                    'Position',[tabinset 10+tablineheight*11 tablinewidth tablineheight],...
                    'String','Feature name:','HorizontalAlignment','left');
                uifeatname(ix)=uicontrol(feattab(ix),'Style','edit',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',output.names(ix),'Units','pixels',...
                    'Position',[tabinset 10+tablineheight*10 tablinewidth tablineheight],...
                    'HorizontalAlignment','left');
                
                uithreshmintxt(ix)=uicontrol(feattab(ix),'Style','text','Units','Pixels',...
                    'Position',[tabinset 10+tablineheight*8 tablinewidth tablineheight],...
                    'String',['Min Threshold: ',num2str(output.minthresh(ix))],'HorizontalAlignment','left');
                uithreshmin(ix)=uicontrol(feattab(ix),'Style','slider',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',['Min Threshold Feature ' num2str(ix)],'Units','pixels',...
                    'Position',[tabinset 10+tablineheight*7 tablinewidth tablineheight],...
                    'SliderStep',[threshstepsmall threshsteplarge]);
                
                uithreshmaxtxt(ix)=uicontrol(feattab(ix),'Style','text','Units','Pixels',...
                    'Position',[tabinset 10+tablineheight*5 tablinewidth tablineheight],...
                    'String',['Max Threshold: ',num2str(output.maxthresh(ix))],'HorizontalAlignment','left');
                uithreshmax(ix)=uicontrol(feattab(ix),'Style','slider',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',['Max Threshold Feature ' num2str(ix)],'Units','pixels',...
                    'Position',[tabinset 10+tablineheight*4 tablinewidth tablineheight],...
                    'SliderStep',[threshstepsmall threshsteplarge]);
                
                uisizetxt(ix)=uicontrol(feattab(ix),'Style','text','Units','Pixels',...
                    'Position',[tabinset 10+tablineheight*2 tablinewidth tablineheight],...
                    'String',['Hole size: ',...
                    num2str(output.holesize(ix))],'HorizontalAlignment',...
                    'left','Enable','off');
                uisize(ix)=uicontrol(feattab(ix),'Style','slider',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',['Hole size Feature ' num2str(ix)],'Units','pixels',...
                    'Position',[tabinset 10+tablineheight*1 tablinewidth tablineheight],...
                    'Enable','off');
            end

        else
            % Same number; don't change a thing
        end
        
        % Set new default values
        for ix = 1:numfeatures
            set(uithreshmin(ix),'Min',threshlim1,'Max',threshlim2,'Value',output.minthresh(ix));
            set(uithreshmax(ix),'Min',threshlim1,'Max',threshlim2,'Value',output.maxthresh(ix));
            set(uisize(ix),'Min',minSize,'Max',maxSize,'Value',output.holesize(ix));
        end
        
        updateImage;        
        
    end
%%
    function []=slider1_Callback(slider1,~,~)
        slice=round(get(slider1,'Value'));
        set(slider1,'Value',slice);
        set(htxt1,'String',['Slice: ',num2str(slice)]);
        updateImage
    end
%%

function []=featureChange_Callback(src,~)
    featnum = str2num(src.Parent.Tag(end));
    if strcmp(src.Style,'edit') == 1      % If edit box (name change)
        output.names(featnum) = get(src,'String');
        feattab(featnum).Title = output.names{featnum};
    else
        switch src.String(1:3)
            case 'Min'
                output.minthresh(featnum) = round(src.Value);
                src.Value = output.minthresh(featnum);
                set(uithreshmintxt(featnum),'String',['Min Threshold: ',num2str(src.Value)]);
            case 'Max'
                output.maxthresh(featnum) = round(src.Value);
                src.Value = output.maxthresh(featnum);
                set(uithreshmaxtxt(featnum),'String',['Max Threshold: ',num2str(src.Value)]);
            case 'Hol'
                output.holesize(featnum) = round(src.Value);
                src.Value = output.holesize(featnum);
                set(uisizetxt(featnum),'String',['Hole size: ',num2str(src.Value)]);
        end
        
        updateImage
    end
    end
%%
    function []=slider3_Callback(slider3,~,~)
        medfilt=round(get(slider3,'Value'));
        if mod(medfilt,2)==0  % If medfilt3D is even
            medfilt = medfilt+1;    % Make it odd!
        end
        set(slider3,'Value',medfilt)
        set(htxt3,'String',['2D median filter size: ',num2str(medfilt)]);
        updateImage
    end
%%
    function []=slider4_Callback(slider4,~,~)
        medfilt3D=round(get(slider4,'Value'));
        if mod(medfilt3D,2)==0  % If medfilt3D is even
            medfilt3D = medfilt3D+1;    % Make it odd!
        end
        set(slider4,'Value',medfilt3D);
        set(htxt4,'String',['3D median filter size: ',num2str(medfilt3D)]);
        
        % Set slice to be at least medfilt3D/2 from end for effective 3D
        % filtering
        slice=get(slider1,'Value');
        slice = max(slice,1+(medfilt3D-1)/2);
        set(slider1,'Value',slice);
        set(htxt1,'String',['Slice: ',num2str(slice)]);
        
        updateImage
    end
%%
    function []=button_Callback(button,~,~)
        
        % Remove duplicate curvpoints
        [~,b] = unique(curvpoints(:,1));
        
        output.filt=get(slider3,'Value');
        output.filt3D=get(slider4,'Value');
        output.curves=curvpoints(b,:);
        
        assignin('caller','output',output);
        close; return
    end

    function []=curvbutt_Callback(curvbutt,~,~)
        
        slice=get(slider1,'Value');
        im=IMAGE(:,:,slice);
        [~,curvmap,curvpoints] = curves_adj_mono(im,curvpoints);
        
        updateImage
        
    end

end