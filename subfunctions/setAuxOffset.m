%% Information
% This program (called by ThreeDR_CropPreProc) loads a GUI to select
% desired offset of auxiliary stabilization file

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2021
% Date: August 11th, 2021

function [auxoffset,output] = setAuxOffset(IMAGE,output,option)
% IMAGE is the image stack or a file name
% output is a struct with properties
% option selects which option should be available: 'trim','crop',bg','all'.
% It's also possible to combine comma-separated commands

if ischar(IMAGE)
    imgstack = 0;
    % Initialize videoreader object
    vid = VideoReader(IMAGE);
    % Estimate number of frames
    finish = ceil(vid.FrameRate*vid.Duration);
    currframe = floor(finish/2);
    vid.CurrentTime = currframe/vid.FrameRate;
    im1 = readFrame(vid);                % Read frame
    if isfield(output,'channelmix')
        channelmix = output.channelmix;
    else
        % Get channel mix
        [channelmix] = selectChannelMix(im1);
        output.channelmix = channelmix;
    end
    
    if channelmix>3
        im1 = rgb2gray(im1);
    else
        im1 = im1(:,:,channelmix);
    end
    sizeIm = size(im1);
    sizeIm(3) = finish;
else
    imgstack = 1;
    sizeIm = size(IMAGE);
    if length(sizeIm)==2
        sizeIm(3) = 1;
    end
end
if isfield(output,'auxpath')
    vidaux = VideoReader(output.auxpath);
    sizeImAux = [vidaux.Height, vidaux.Width, vidaux.NumFrames];
end
if ~isfield(output,'motioncorr')
    output.motioncorr = zeros(sizeIm(3),3);
end
sizeImcorr = sizeIm + round([max(output.motioncorr(:,3)),...
    max(output.motioncorr(:,2)), 0]);
sizeImcorrAux = sizeImAux + round([max(output.auxmotioncorr(:,3)),...
    max(output.auxmotioncorr(:,2)), 0]);
if ~isfield(output,'auxoffset')
    auxoffset = 0;
else
    auxoffset = output.auxoffset;
end
currframeaux = currframe - auxoffset;

if nargin < 3
    option = 'all';
end

% Initialize other parameters
if ~isfield(output,'endFrame')
    output.endFrame = sizeIm(3);
end
if ~isfield(output,'startFrame')
    output.startFrame = 1;
end
initFrame = round((output.endFrame + output.startFrame)/2);
tmp = (0:.05:1);
clrmp = [tmp' tmp' tmp'];


%% Initialize window

global fullIMAGE
fullIMAGE = zeros(sizeImcorr(1),sizeImcorr(2),sizeIm(3),'uint8');
calcCorrImage(initFrame);

offsetFig = generalEditorWindow(fullIMAGE);
offsetFig.fig.Name = "Set offset of aux camera";


%% Add controls

offsetFig.sidepanel.Units = 'pixels';
sidepos = offsetFig.sidepanel.Position;

% Reassign callbacks
offsetFig.finishbutton.Callback = @finish_Callback;
offsetFig.frameslider.Callback = @frame_Callback;
offsetFig.brightslider.Callback = @brightness_Callback;

numsliders = 8;
sliderpos = zeros(numsliders,4);     % Array of sidebar slider locations
slidertxtpos = zeros(numsliders,4);     % Array of sidebar slider text locations
% If the number of vertical pixels per slider is greater than 30 (slider +
% text) + 10 (padding), stick to pixel dimensions, otherwise use normalized
% units
leftover = sidepos(4) - 50 * numsliders + 60;
if leftover > 0
    sliderunits = 'pixels';
    
    % Set slider and text location
    sliderpos(:,1) = 0.1 * sidepos(3);
    slidertxtpos(:,1) = 0.15 * sidepos(3);
    sliderpos(:,2) = sidepos(4) - 40 - (1:numsliders)' * 50;
    slidertxtpos(:,2) = sidepos(4) - 40 - (1:numsliders)' * 50 + 20;
    
    % Set slider and text width & height
    sliderpos(:,3) = sidepos(3)*0.8;
    slidertxtpos(:,3) = sidepos(3)*0.8;
    sliderpos(:,4) = 17;
    slidertxtpos(:,4) = 14;
    
    xtraspace = floor(leftover/6);
else
    sliderunits = 'normalized';
    
    disp('This section for very small screens is experimental');
    % Set slider and text location
    sliderpos(:,1) = 0.1;
    slidertxtpos(:,1) = 0.15;
    sliderpos(:,2) = 1 - 0.05 - (1:numsliders)' * (0.9/numsliders) * 1.4;
    slidertxtpos(:,2) = 1 - 0.05 - (1:numsliders)' * (0.9/numsliders);
    
    % Set slider and text width & height
    sliderpos(:,3) = 0.8;
    slidertxtpos(:,3) = 0.8;
    sliderpos(:,4) = (0.9 / numsliders) * 0.55;
    slidertxtpos(:,4) = (0.9 / numsliders) * 0.35;
    
    xtraspace = 0;
end

% Slider for frame selection start
slidernum = 1;
% chDetrendtxt = uicontrol('Parent',offsetFig.sidepanel,...
%     'Style','text',...
%     'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
%     'String',['Start frame: ',num2str(output.startFrame)],...
%     'HorizontalAlignment','left',...
%     'Tag','StartFrameTxt');
chDetrend = uicontrol('Parent',offsetFig.sidepanel,'Style','checkbox',...
    'Callback',@detrend_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Value',0,...
    'String','Remove trend',...
    'Tag','Detrend');

% Slider for frame selection end
slidernum = slidernum + 1;
slOffsettxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Offset: ',num2str(auxoffset)],...
    'HorizontalAlignment','left',...
    'Tag','OffsetTxt');
minoffset = -500;
maxoffset = 500;
if auxoffset<minoffset
    minoffset = auxoffset-30;
end
if auxoffset>maxoffset
    maxoffset = auxoffset+30;
end
slOffset = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',minoffset,'Max',maxoffset,'Value',auxoffset,...
    'SliderStep',[1/(maxoffset-minoffset) 10/(maxoffset-minoffset)],...
    'Tag','Offset');

% Create a little extra space before next sliders
sliderpos(slidernum+1:end,2) = sliderpos(slidernum+1:end,2) - xtraspace;
slidertxtpos(slidernum+1:end,2)=slidertxtpos(slidernum+1:end,2)-xtraspace;

% Slider for left side crop
slidernum = slidernum + 1;
pbPlay = uicontrol('Parent',offsetFig.sidepanel,'Style','togglebutton',...
    'Callback',@play_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Value',0,...
    'String','Play',...
    'Tag','playbutton');

% Slider for right side crop
slidernum = slidernum + 1;
slCropRighttxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Right crop: ',num2str(1)],...
    'HorizontalAlignment','left',...
    'Tag','RightCropTxt');
slCropRight = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(2),'Value',1,...
    'SliderStep',[1/sizeImcorr(2) 30/sizeImcorr(2)],...
    'Tag','RightCrop');

% Slider for top side crop
slidernum = slidernum + 1;
slCropToptxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Top crop: ',num2str(1)],...
    'HorizontalAlignment','left',...
    'Tag','TopCropTxt');
slCropTop = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(1),'Value',1,...
    'SliderStep',[1/sizeImcorr(1) 30/sizeImcorr(1)],...
    'Tag','TopCrop');

% Slider for bottom side crop
slidernum = slidernum + 1;
slCropBottomtxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Bottom crop: ',num2str(1)],...
    'HorizontalAlignment','left',...
    'Tag','BottomCropTxt');
slCropBottom = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(1),'Value',1,...
    'SliderStep',[1/sizeImcorr(1) 30/sizeImcorr(1)],...
    'Tag','BottomCrop');

% Create a little extra space before next sliders
sliderpos(slidernum+1:end,2) = sliderpos(slidernum+1:end,2) - xtraspace;
slidertxtpos(slidernum+1:end,2)=slidertxtpos(slidernum+1:end,2)-xtraspace;

% Slider for minimum filter
slidernum = slidernum + 1;
if ~isfield(output,'bgminfilt')
    output.bgminfilt = 0;
end
slMinFilttxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['BG subtr. minimum filter: ',num2str(output.bgminfilt)],...
    'HorizontalAlignment','left',...
    'Tag','MinFiltTxt');
slMinFilt = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',0,'Max',sizeIm(3),'Value',output.bgminfilt,...
    'SliderStep',[2/(sizeIm(3)+1) 10/(sizeIm(3)+1)],...
    'Tag','MinFilt',...
    'ToolTipString',['Set minimum filter size for background to be ',...
    'subtracted. 0 and 1 mean off.']);

% Slider for median filter
slidernum = slidernum + 1;
slMedFilttxt = uicontrol('Parent',offsetFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['BG subtr. median filter: ',num2str(1)],...
    'HorizontalAlignment','left',...
    'Tag','MedFiltTxt');
slMedFilt = uicontrol('Parent',offsetFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeIm(3),'Value',1,...
    'SliderStep',[2/(sizeIm(3)) 10/(sizeIm(3))],...
    'Tag','MedFilt',...
    'ToolTipString',['Set median filter size for background to be ',...
    'subtracted. 0 and 1 mean off.']);

%% Adjust for requested options
% First, turn controls off
% chDetrendtxt.Visible = 'off';
% chDetrend.Visible = 'off';
% slOffsettxt.Visible = 'off';
% slOffset.Visible = 'off';
% pbPlay.Visible = 'off';
slCropRighttxt.Visible = 'off';
slCropRight.Visible = 'off';
slCropToptxt.Visible = 'off';
slCropTop.Visible = 'off';
slCropBottomtxt.Visible = 'off';
slCropBottom.Visible = 'off';
slMinFilttxt.Visible = 'off';
slMinFilt.Visible = 'off';
slMedFilttxt.Visible = 'off';
slMedFilt.Visible = 'off';

% Then turn on desired controls
optiontmp = strsplit(option,',');
for i = 1:length(optiontmp)
    switch optiontmp{i}
        case 'trim'
            chDetrendtxt.Visible = 'on';
            chDetrend.Visible = 'on';
            slOffsettxt.Visible = 'on';
            slOffset.Visible = 'on';
        case 'crop'
            pbPlay.Visible = 'on';
            slCropRighttxt.Visible = 'on';
            slCropRight.Visible = 'on';
            slCropToptxt.Visible = 'on';
            slCropTop.Visible = 'on';
            slCropBottomtxt.Visible = 'on';
            slCropBottom.Visible = 'on';
        case 'bg'
            slMinFilttxt.Visible = 'on';
            slMinFilt.Visible = 'on';
            slMedFilttxt.Visible = 'on';
            slMedFilt.Visible = 'on';
    end
end


%% Update image objects

offsetFig.axes.Units = 'normalized';
offsetFig.axes.Position = [0.05 0.15 0.3 0.35];
if isfield(output,'vidPath')
    [~,vidfile,~] = fileparts(output.vidPath);
    title(vidfile);
end

% Initialize aux frame
offsetFig.auxaxes = axes('Position',[0.4 0.15 0.3 0.35]);
plot(offsetFig.auxaxes,1:10,sin(1:10));
[~,auxfile,~] = fileparts(output.auxpath);
title(auxfile)

% Initialize offset plot
timex = vid.FrameRate/vidaux.FrameRate;
offsetFig.offsetaxes = axes('Position',[0.05 0.58 0.65 0.27]);
plotMainX = plot(1:size(output.motioncorr,1),output.motioncorr(:,2),'LineWidth',1);
hold on
plotMainY = plot(1:size(output.motioncorr,1),output.motioncorr(:,3),'LineWidth',2);
plotAuxX = plot(timex*(1:size(output.auxmotioncorr,1))+auxoffset,output.auxmotioncorr(:,2),'LineWidth',1);
plotAuxY = plot(timex*(1:size(output.auxmotioncorr,1))+auxoffset,output.auxmotioncorr(:,3),'LineWidth',2);
plotPosition = plot([initFrame initFrame],offsetFig.offsetaxes.YLim,'--','Color',[0.5 0.5 0.5]);
hold off
legend(offsetFig.offsetaxes,{'Main X','Main Y','Aux X','Aux Y'})

global fullIMAGEaux
fullIMAGEaux = [];

mainim = [];
auxim = [];
brightness_Callback;
makeThePlot;
waitfor(offsetFig.fig);

%% Callback functions

% Callback for frame change
    function [] = frame_Callback(~,~,~)
        
        currfr = round(get(offsetFig.frameslider,'Value'));
        set(offsetFig.frameslider,'Value',currfr);
        set(offsetFig.frametxt,'String',['Frame: ',num2str(currfr)]);
        
%         frno = round(offsetFig.frameslider.Value);
%         offsetFig.frameslider.Value = frno;
%         set(offsetFig.frametxt,'String',['Frame: ',num2str(frno)]);

        makeThePlot;
    end

% Callback for play button
    function [] = play_Callback(~,~,~)
        
        currfr = round(offsetFig.frameslider.Value);
        while pbPlay.Value == 1 && (currfr+1 < sizeIm(3))
            pbPlay.String = 'Pause';
            offsetFig.frameslider.Value = currfr + 1;
            frame_Callback;
            drawnow;
            currfr = offsetFig.frameslider.Value;
        end
        pbPlay.String = 'Play';
        pbPlay.Value = 0;
    end

% Callback for detrend change
    function [] = detrend_Callback(~,~,~)
        
        detrendOn = round(chDetrend.Value);
        
        if detrendOn
            plotMainX.YData = detrend(output.motioncorr(:,2));
            plotMainY.YData = detrend(output.motioncorr(:,3));
            plotAuxX.YData = detrend(output.auxmotioncorr(:,2));
            plotAuxY.YData = detrend(output.auxmotioncorr(:,3));
        else
            plotMainX.YData = output.motioncorr(:,2);
            plotMainY.YData = output.motioncorr(:,3);
            plotAuxX.YData = output.auxmotioncorr(:,2);
            plotAuxY.YData = output.auxmotioncorr(:,3);
        end
        axis(offsetFig.offsetaxes,'auto y')
        plotPosition.YData = offsetFig.offsetaxes.YLim;
        
        makeThePlot;
    end

% Callback brightness change
    function [] = brightness_Callback(~,~,~)
        brightness = round(offsetFig.brightslider.Value);
        offsetFig.brightslider.Value = brightness;
        offsetFig.brighttxt.String = ['Brightness: ',num2str(brightness) '%'];
        
         % Apply brightness setting
        bright = 200 - brightness;
        temp = (0:.005:1).^(bright/100);
        axes(offsetFig.axes);
        clrmp = [temp' temp' temp'];
        colormap(gca,clrmp);
        
%         makeThePlot;
    end

% Slider Callback
    function []=slider_Callback(source,~,~)
        sliderval = round(get(source,'Value'));
        
        % If slider is median filter, make sure the value is odd
        if strcmp(source.Tag,slMedFilt.Tag)
            if mod(sliderval,2)==0
                sliderval = sliderval+1;
            end
        elseif strcmp(source.Tag,slMinFilt.Tag)
            if mod(sliderval,2)==0
                sliderval = sliderval+1;
            end
        end
        set(source,'Value',sliderval);
        
        slidertxtobj = findobj(offsetFig.fig,'Tag',[source.Tag 'Txt']);
        sliderstr = get(slidertxtobj,'String');
        temp = strsplit(sliderstr,':');
        sliderstr = [temp{1} ': ' num2str(sliderval)];
        slidertxtobj.String = sliderstr;
        pause(0.002);
        
        makeThePlot;
    end

% Callback to finish
    function [] = finish_Callback(~,~,~)
        readAllParams;
        close(offsetFig.fig); return
    end

%% Other functions

% Plot new image
    function [] = makeThePlot(~)
        slice = offsetFig.frameslider.Value;
        auxoffset = slOffset.Value;
        output.auxoffset = auxoffset;
        %         readAllParams;
        
        % Prep main image
        % Calculate and subtract background if needed
        calcCorrImage(slice);
        dispimg = fullIMAGE(:,:,slice);
        
        % Show main image
        if isempty(mainim)
            axes(offsetFig.axes);
            mainim = imshow(dispimg,clrmp);
            title(vidfile);
        else
            mainim.CData = dispimg;
        end
        
        % Prep aux image
        sliceaux = round((slice-auxoffset)/timex);
        if (sliceaux)>0 && (sliceaux)<sizeImAux(3)
            calcCorrImageAux(sliceaux);
            dispimgaux = fullIMAGEaux(:,:,sliceaux);
        else
            dispimgaux = uint8(zeros(sizeImcorrAux(1),sizeImcorrAux(2)));
        end
        
        % Show aux image
        if isempty(auxim)
            axes(offsetFig.auxaxes);
            auxim = imshow(dispimgaux,clrmp);
        else
            auxim.CData = dispimgaux;
        end
%         axes(offsetFig.auxaxes);
%         imshow(dispimgaux,clrmp);

        % Update plot (move marker)
        timex = vid.FrameRate/vidaux.FrameRate;
        plotAuxX.XData = timex*(1:size(output.auxmotioncorr,1))+auxoffset;
        plotAuxY.XData = plotAuxX.XData;
        plotPosition.XData = [slice slice];
    end

% Calculate new motion-corrected frames on demand
    function [] = calcCorrImage(corrFrame)
        % Loop through all requested frames
        for Fr = corrFrame
            % Only recalculate if frame has not been calculated
            if sum(sum(fullIMAGE(:,:,Fr))) == 0
%                 [X,Y] = meshgrid((1:sizeIm(2)) + ...
%                     max(output.motioncorr(:,2)) - ...
%                     output.motioncorr(Fr,2),...
%                     (1:sizeIm(1)) + max(output.motioncorr(:,3)) - ...
%                     output.motioncorr(Fr,3));

                newx = (1:sizeIm(2)) + ...
                    output.motioncorr(Fr,2);
                newy = (1:sizeIm(1)) + output.motioncorr(Fr,3);
%                 [X,Y] = meshgrid((1:sizeIm(2)) + ...
%                     output.motioncorr(Fr,2),...
%                     (1:sizeIm(1)) + output.motioncorr(Fr,3));
%                 [Xq,Yq] = meshgrid(1:sizeImcorr(2),1:sizeImcorr(1));
%                 initIMAGE = interp2(X,Y,getFrame(Fr),Xq,Yq,'nearest',0);
%                 fullIMAGE(:,:,Fr) = (initIMAGE);
                fullIMAGE(round(newy),round(newx),Fr) = getFrame(Fr);
            end
        end
        
    end

% Calculate new motion-corrected frames on demand for aux
    function [] = calcCorrImageAux(corrFrame)
        if isempty(fullIMAGEaux)
            fullIMAGEaux = zeros(sizeImcorrAux(1),sizeImcorrAux(2),sizeImAux(3),'uint8');
        end

        % Loop through all requested frames
        for Fr = corrFrame
            % Only recalculate if frame has not been calculated
            if sum(sum(fullIMAGEaux(:,:,Fr))) == 0
%                 [X,Y] = meshgrid((1:sizeIm(2)) + ...
%                     max(output.motioncorr(:,2)) - ...
%                     output.motioncorr(Fr,2),...
%                     (1:sizeIm(1)) + max(output.motioncorr(:,3)) - ...
%                     output.motioncorr(Fr,3));
                [X,Y] = meshgrid((1:sizeImAux(2)) + ...
                    output.auxmotioncorr(Fr,2),...
                    (1:sizeImAux(1)) + output.auxmotioncorr(Fr,3));
                [Xq,Yq] = meshgrid(1:sizeImcorrAux(2),1:sizeImcorrAux(1));
                initIMAGE = interp2(X,Y,getAuxFrame(Fr),Xq,Yq,'nearest',0);
                fullIMAGEaux(:,:,Fr) = (initIMAGE);
            end
        end
    end

    function readAllParams(~)
        % This function reads out all typical gui parameters
%         output.startFrame = round(chDetrend.Value);
%         output.endFrame = round(slOffset.Value);
%         output.LCrop = round(pbPlay.Value);
%         output.RCrop = round(slCropRight.Value);
%         output.TCrop = round(slCropTop.Value);
%         output.BCrop = round(slCropBottom.Value);
%         output.bgmedfilt = round(slMedFilt.Value);
%         output.bgminfilt = round(slMinFilt.Value);
    end

    function imx = getFrame(frno)
        % This function provides an image frame
        if imgstack == 1
            imx = IMAGE(:,:,frno);
        else
            if frno == (currframe+1)
                imx = readFrame(vid);                % Read frame
            else
                vid.CurrentTime = (frno-1)/vid.FrameRate;
                currframe = frno;
                imx = readFrame(vid);                % Read frame
            end
            imx = imx(:,:,1);                   % Make gray
        end
        currframe = frno;
    end

    function imx = getAuxFrame(frno)
        % This function provides an image frame
        
        if frno == (currframeaux+1)
            imx = readFrame(vidaux);                % Read frame
        else
            vidaux.CurrentTime = (frno-1)/vidaux.FrameRate;
            currframeaux = frno;
            imx = readFrame(vidaux);                % Read frame
        end
        imx = imx(:,:,1);                   % Make gray
        currframeaux = frno;
    end



end