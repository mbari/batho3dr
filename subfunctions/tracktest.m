% Track test

% This program is a test to analyze particle track speed in 3DR footage, in
% an attempt to (1) estimate vehicle Z speed and (2) improve image
% stabilization. First, I will focus on (1).

% Assume you have an image stack named IMAGEcrop (uint8). We will use
% simpletracker, which needs a cell array of point locations, so we will
% find the particles first.
% Steps to follow:
% - threshold image
% - find particles
% - find particle locations
% - pass to simpletracker

% Settings
threshold = 0.1;
maxpixsize = 20;
currimage = IMAGEcrop;

% Initialize
threshold = threshold*max(currimage(:));

% Remove all data below threshold
currimage(currimage<threshold) = 0;

% Loop through frames
for fr=1:size(currimage,3)
    % fr = 100;
    tempim = currimage(:,:,fr);
    figure(1)
    imagesc(tempim)
    cc = bwconncomp(currimage(:,:,fr));     % Get all unique connected blobs
    numPixels = cellfun(@numel,cc.PixelIdxList);    % Number of pixels for each blob
    
    % Set all non-relevant pixels to zero
    inds = find(numPixels>maxpixsize);
    for i=1:numel(inds)
        tempim(cc.PixelIdxList{inds(i)}) = 0;
    end
    
    S = regionprops(tempim>0,'centroid'); % Centroid of all small items
    
    trackpointlocs{fr} = cell2mat(struct2cell(S)');
    pause(0.005)
end

%% Now use simpletracker to track points
tracks = simpletracker(trackpointlocs,'debug',1);



