%% Information
% This program (called by ThreeDR_CropPreProc) loads the temporary folder
% and allows selection of a pre-loaded video file, or a different file. If
% a new file is selected, it will be copied to the temporary folder and a
% *.mat file will be created.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 2nd, 2018

%%
function [matPath,newVidPath] = loadVideoFile(~)
global temppath

% Detect files in temporary folder
fcont = dir(temppath);
ind = [fcont(:).isdir]; %# returns logical vector
seqlist = {fcont(ind).name}';
seqlist(ismember(seqlist,{'.','..'})) = [];

% Create file name overview
listname = seqlist;
for i2 = 1:size(seqlist,1)
    listname{i2} = strrep(seqlist{i2},'_temp','');
end

% Retrieve current screen size
wa = images.internal.getWorkArea;
screenwidth = wa.width;
screenheight = wa.height;

% Default window dimensions
windowW = 400;  % window width in pixels
windowH = 400;  % window height in pixels
windowX = 0.05*screenwidth+400;  % window position from left in pixels
windowY = 400;  % window position from bottom in pixels

% Create file selection dialog
qdlg = figure('Toolbar','none','Menubar','none','Name','Choose file',...
    'NumberTitle','on','IntegerHandle','off','Units','Pixels',...
    'Position',[windowX windowY windowW windowH]);

uicontrol('Parent',qdlg,'Style','text','String',...
    ['Choose a sequence from the temporary file folder or browse to ',...
    'a different file'],...
    'Units','normalized','Position',[0.1,0.85,0.8,0.1],...
    'HorizontalAlignment','left');

qlist  = uicontrol('Parent',qdlg,'Style','listbox','String',listname,...
    'Units','normalized','Position',[0.1,0.2,0.8,0.6]);

uicontrol('Parent',qdlg,'Style','pushbutton','String','OK',...
    'Units','normalized','Position',[0.1,0.05,0.25,0.1],...
    'Callback',@selectFile);

% qcancel  = uicontrol('Parent',qdlg,'Style','pushbutton',...
%     'String','Cancel',...
%     'Units','normalized','Position',[0.30,0.05,0.25,0.1]);

uicontrol('Parent',qdlg,'Style','pushbutton',...
    'String','Browse...',...
    'Units','normalized','Position',[0.60,0.05,0.3,0.1],...
    'Callback',@browseFile);

% Result at end of program (causes by closing in 'browse' or 'ok' callback
matPath = 'null';
% vidFileName = 'null';
uiwait(qdlg);   % Wait until figure closes
clear qdlg

%% Some nested functions

    function browseFile(~,~)
        % Find a new file and copy it
        % global temppath
        % global qdlg
        
        [VideoFilename,VideoPathName,~] = uigetfile({'*.mov';'*.avi';'*.mpg'},...
            'Select a 3DR video file');
        VideoPath = [VideoPathName filesep VideoFilename];
        
        % Copy relevant files to local folder for easy access
        [~,fname,ext] = fileparts(VideoPath);
%         vidFileName = fname;
        newfoldername = [temppath filesep fname '_temp' filesep];
        if exist(newfoldername,'dir')==0
            % If folder does not exist, put it there and add relevant files
            mkdir(newfoldername);
            %     clips = [dir dataset '.mov'];
            %     if exist(clips,'file')==0
            %         clips = [dir 'clips' filesep dataset '.mov'];
            %     end
            fprintf(' Copying file to temporary folder...');
            copyfile(VideoPath,newfoldername);
            fprintf(' Done.\n');
        else
            fprintf(' Temporary file already exists.\n');
        end
        
        matPath = [newfoldername fname '_IMAGE.mat'];     % Get filename for *.mat file
        newVidPath = [newfoldername fname ext];     % Get filename for video file
        
%         createMatFile(matPath,newVidPath);
        warning(['Image stack not created; this is intentional, '...
            'but needs to be tested.'])
        
        close(qdlg);
        
    end

    function createMatFile(matPath,newVidPath)
        % Turn video file into adjacent .mat file if it does not exist yet
        
        % Disable buttons
        for i=1:size(qdlg.Children,1)
            qdlg.Children(i).Enable = 'off';
        end
        
        [~,fname,~] = fileparts(newVidPath);
        
        if exist(matPath,'file')==2
            disp(['  Image stack for ', fname,' already exists.'])
        else
            % Load video object
            vid = VideoReader(newVidPath);
            % Estimate number of frames
            finish = ceil(vid.FrameRate*vid.Duration);
            im = readFrame(vid);                % Read first frame
            IMAGE(:,:,finish) = im(:,:,1).*0;     % Pre-allocate
            currFrame = 1;
%             IMAGE(:,:,currFrame) = rgb2gray(im);        % Fill first frame
            IMAGE(:,:,currFrame) = im(:,:,1);        % Fill first frame
            
            progtxtsz=0;
            fprintf('  Loading video... ');
            while hasFrame(vid)
                currFrame = currFrame + 1;
                im = readFrame(vid);
%                 imgray = rgb2gray(im);
                imgray = im(:,:,1);
                %         imgray=rgb2lab(im);
                IMAGE(:,:,currFrame)=imgray;
                if progtxtsz~=0
                    fprintf(repmat('\b',1,progtxtsz+1));
                end
                progtxt = sprintf('%3.1f',min(100*(currFrame-1)/(finish-1),100));
                progtxtsz = size(progtxt,2);
                updatetxt = [progtxt '%%'];
                fprintf(updatetxt);
                pause(0.001)
            end
            fprintf(['\n  Saving image stack to ' fname '_IMAGE.mat...']);
            save(matPath,'IMAGE','-v7.3');        %save image stack
            fprintf(' Done.\n');
        end
    end

    function selectFile(~,~)
        % Get selected file and load
        
        filename = qlist.String{qlist.Value};
        foldername = [temppath,filesep,filename,'_temp',filesep];
        matPath = [foldername, filename, '_IMAGE.mat'];
%         newVidPath = [foldername filename '.mov'];     % Get filename for video file
        
        % If the .mat file does not exist, search for video file
        if exist(matPath,'file')==0
%             disp('  Requested .mat file does not exist.')
            
            % Find video file
            % Get folder content
            cont = dir(foldername);
            newVidPath = 'null';
            for i = 1:size(cont,1)
                [~,~,ext] = fileparts(cont(i).name);
                if sum(strcmp(ext,{'.mov';'.avi';'.mpg'}))>0
                    newVidPath = [foldername, cont(i).name];
                end
            end
            
            % Error if no video file found, generate .mat if video file found
            if strcmp(newVidPath,'null')
                error('No video file found, please remove folder and try again');
            else
%                 createMatFile(matPath,newVidPath);
            end
        end
        
        close(qdlg);
        
    end

end


