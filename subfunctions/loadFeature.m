%% Information
% This program (called by ThreeDR_FeatureSelect) loads the folder of the
% footage and shows existing features to be edited. Based on the function
% 'loadVideoFile'.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 15th, 2018

%%
function [featPath] = loadFeature(filepath,searchname,searchtype)
% Enter searchname as string, or leave empty: ''
% Searchtype is the extension. Provide with or without period. Cell arrays
% accepted, e.g. {'.stl','.ply'}

if nargin < 3
    searchtype = {''};
else
    % If search extension is provided, normalize format by adding period
    % and turning into cell array
    if iscell(searchtype)
        % Add periods if needed
        for i = 1:length(searchtype)
            if searchtype{i}(1) ~= '.'
                searchtype(i) = {['.' searchtype{i}]};
            end
        end
    else
        if searchtype(1) ~= '.'
            searchtype = {['.' searchtype]};
        else
            searchtype = {searchtype};
        end
    end
end
if nargin < 2
    searchname = 'params';
end

% Detect files in folder
fcont = dir(filepath);
seqlist = {fcont.name}';

features = contains(seqlist,'feature') .* contains(seqlist,searchname) ...
    .* (~contains(seqlist,'sm.mat')) .* (~contains(seqlist,'val_fv.mat'))...
    .* contains(seqlist,searchtype,'IgnoreCase',true) > 0;
hits = find(features);

% Create overview
listname = seqlist(features);

for i2 = 1:size(listname,1)
    temp = strsplit(listname{i2},{'feature_','_params.mat'});
    listname{i2} = temp{2};
end

% Retrieve current screen size
wa = images.internal.getWorkArea;
screenwidth = wa.width;

% Default window dimensions
windowW = 400;  % window width in pixels
windowH = 400;  % window height in pixels
windowX = 0.05*screenwidth+400;  % window position from left in pixels
windowY = 400;  % window position from bottom in pixels

% Create file selection dialog
qdlg = figure('Toolbar','none','Menubar','none','Name','Choose feature',...
    'NumberTitle','on','IntegerHandle','off','Units','Pixels',...
    'Position',[windowX windowY windowW windowH]);

if strcmp(searchtype{1},'')
    typetxt = 'file';
else
    typetxt = strjoin(searchtype,'/');
end
uicontrol('Parent',qdlg,'Style','text','String',...
    ['Choose an existing ' typetxt ' to load'],...
    'Units','normalized','Position',[0.1,0.85,0.8,0.1],...
    'HorizontalAlignment','left');

qlist  = uicontrol('Parent',qdlg,'Style','listbox','String',listname,...
    'Units','normalized','Position',[0.1,0.2,0.8,0.6]);

uicontrol('Parent',qdlg,'Style','pushbutton','String','OK',...
    'Units','normalized','Position',[0.1,0.05,0.25,0.1],...
    'Callback',@selectFile);


% Result at end of program (causes by closing in 'browse' or 'ok' callback
featPath = 'null';
uiwait(qdlg);   % Wait until figure closes
clear qdlg


%% Some nested functions

    function selectFile(~,~)
        % Get selected file and build path
        
%         filename = qlist.String{qlist.Value};
%         tmp = (contains(seqlist,[filename,'_params.mat']));

        filename = seqlist{hits(qlist.Value)};
        featPath = [filepath, filesep, filename];
        
        close(qdlg);
        
    end

end


