function [newmask] = editblobs3D(mask1,basemask,action,slice,slrange,slicedir)
% action should be 'remove'/'delete' or 'add'/'include'
% slice is only required when using 3D masks
if nargin < 5
    slrange = 40;
end
if nargin < 6
    slicedir = 1;   % Forward extent is default [symmetric = 0, forward = 1, backward = -1]
end
imh = gca;
M = imfreehand(imh,'Closed','true');
hc = get(M,'Children');

title('Calculating new mask...');
if size(M,1)~=0
    pts=(M.getPosition);    % Get points on line
    size(pts)
    if size(pts,1)==1   % If only a single point selected
        ptsx = pts(1);
        ptsy = pts(2);
    else
        N = regionfill(uint8(M.createMask),pts(:,2),pts(:,1));  % Fill freehand object
        [ptsy, ptsx] = find(N>0);       % Find selected region
    end
    
    if slicedir==0  % If symmetric extent
        slstrt = max(ceil(slice-slrange/2),1);
        slend = min(floor(slice+slrange/2),size(mask1,3));
    elseif slicedir==1    % If in forward direction
        slstrt = slice;
        slend = min((slice+slrange-1),size(mask1,3));
    elseif slicedir==-1    % If in backward direction
        slstrt = max(ceil(slice-slrange+1),1);
        slend = slice;
    end
    
    title('Calculating new mask...');
    if or(strcmp(action,'remove'),strcmp(action,'delete'))
        if numel(size(basemask)) == 3       % If 3D mask
            title('Calculating...');pause(0.005);
            tempmask=bwselect(logical(mask1(:,:,slice)),ptsx,ptsy,4);  % Select blobs within selection in 2D
            [ptsy, ptsx] = find(tempmask>0);       % Find selected region
            tempmask2=bwselect3(logical(mask1(:,:,slstrt:slend)),ptsx,ptsy,ptsx*0+slice-slstrt+1,18);
            % Select connected areas
            mask1(:,:,slstrt:slend) = mask1(:,:,slstrt:slend)-tempmask2;
            newmask = mask1;
            title('');pause(0.003);
            toc
        else
            tempmask=bwselect(logical(mask1),ptsx,ptsy,4);  % Select connected areas
            newmask=mask1-tempmask;   % Remove areas
        end
        
    elseif or(strcmp(action,'add'),strcmp(action,'include'))
        if numel(size(basemask)) == 3       % If 3D mask
            title('Calculating...');pause(0.005);
            tempmask=bwselect(logical(basemask(:,:,slice)),ptsx,ptsy,4);  % Select blobs within selection in 2D
            [ptsy, ptsx] = find(tempmask>0);       % Find selected region
            tempmask2=bwselect3(logical(basemask(:,:,slstrt:slend)),ptsx,ptsy,round(ptsx*0+slice-slstrt+1),18);
            % Select connected areas
            mask1(:,:,slstrt:slend) = mask1(:,:,slstrt:slend)+tempmask2;
            newmask = mask1;
            title('');
            pause(0.003);
        else
            tempmask=bwselect(logical(basemask),ptsx,ptsy,4);  % Select connected areas
            newmask=mask1+tempmask;   % Add areas
        end
    else
        disp('You have to specify what you want to do with this shape. Check the code!');
    end
else
    newmask = mask1>0;
end
title('');
