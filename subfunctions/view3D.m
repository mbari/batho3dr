%% Information
% This program (called by ThreeDR_ViewSTL) allows the user to visualize
% multiple STL and PLY files.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: February 22nd, 2019

function [] = view3D(fpath)
global temppath             % Temporary folder location
global alldata

% fpath is a path string, or cell array of strings pointing to STL or PLY
% files. Can be left empty.


%% Initialize window

IMAGE = zeros(900,900);
viewFig = generalEditorWindow(IMAGE,400);
viewFig.fig.Name = "View STL/PLY files";
viewFig.updtxt.String = 'Select files to view';
viewFig.updtxt.Visible = 'on';
viewFig.axes.OuterPosition = viewFig.axes.Position;
viewFig.axes.Units = 'normalized';

% Allow rotations
rot = rotate3d(viewFig.fig);
rot.Enable = 'on';
setAllowAxesRotate(rot,viewFig.axes,true);
rot.ActionPostCallback = @rotCallback;

% Reassign callbacks
% viewFig.finishbutton.Callback = @finish_Callback;
viewFig.frameslider.Callback = @frame_Callback;
viewFig.brightslider.Visible = 'off';
viewFig.brighttxt.Visible = 'off';
viewFig.frameslider.Visible = 'off';
viewFig.frametxt.Visible = 'off';
viewFig.toppanel.Visible = 'off';

alldata = struct;


%% Add controls

viewFig.sidepanel.Units = 'normalized';

% Load button
uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Callback',@loadFile,'String','Load file...',...
    'Units','normalized',...
    'Position',[0.05 0.91 0.28 0.04],...
    'Fontsize',12);

% Delete button
uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Callback',@deletefile_Callback,'String','Delete',...
    'Units','normalized',...
    'Position',[0.36 0.91 0.28 0.04],...
    'Fontsize',12);

% Refresh button
uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Callback',@refreshfile_Callback,'String','Refresh',...
    'Units','normalized',...
    'Position',[0.67 0.91 0.28 0.04],...
    'Fontsize',12);

% File table
ftable = uitable(viewFig.sidepanel,...
    'Units','normalized',...
    'Position',[0.05 0.55 0.9 0.35],...
    'Fontsize',12,...
    'CellSelectionCallback',@selectObj);
ftable.ColumnName = {'','Filename                           ','Opacity'};
ftable.ColumnFormat = {'logical', 'char', 'char'};
ftable.ColumnEditable = [true,false,false];
ftable.RowName = [];
ftable.BackgroundColor = [1 1 1];
ftable.ForegroundColor = [0 0 0];
ftable.ColumnWidth = {30,260,60};

% Pop out image
uicontrol('Parent',viewFig.botpanel,'Style','pushbutton',...
    'Callback',@popout_Callback,'String','Open in new window',...
    'Units','normalized',...
    'Position',[0 0 0.2 1],...
    'Fontsize',12);

% Animate button
animButton = uicontrol('Parent',viewFig.botpanel,'Style','togglebutton',...
    'Callback',@animate_Callback,'String','Animate',...
    'Units','normalized',...
    'Position',[0.35 0 0.13 1],...
    'Fontsize',12,'Value',0);

% Lighting button
uicontrol('Parent',viewFig.botpanel,'Style','pushbutton',...
    'Callback',@lighting_Callback,'String','Lighting...',...
    'Units','normalized',...
    'Position',[0.52 0 0.13 1],...
    'Fontsize',12,'Value',0);

% Save GIF/MP4
uicontrol('Parent',viewFig.botpanel,'Style','pushbutton',...
    'Callback',@saveGIF_Callback,'String','Save GIF/MP4...',...
    'Units','normalized',...
    'Position',[0.71 0 0.12 1],...
    'Fontsize',12);

% Save figure
uicontrol('Parent',viewFig.botpanel,'Style','pushbutton',...
    'Callback',@savefile_Callback,'String','Save image as...',...
    'Units','normalized',...
    'Position',[0.84 0 0.16 1],...
    'Fontsize',12);


% BG Color picker
bgColor = [0.8 0.8 0.8];
uicontrol('Parent',viewFig.sidepanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.05 0.09 0.25 0.03],...
    'String','Background Color:',...
    'HorizontalAlignment','left');
bgColorPanel = uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Units','normalized','Position',[0.28 0.09 0.18 0.03],...
    'BackgroundColor',bgColor,...
    'Callback',@bgColorPicker_Callback);


% Scale bar text
uicontrol('Parent',viewFig.sidepanel,'Style','text',...
    'String','Scale:',...
    'Units','normalized',...
    'Position',[0.05 0.05 0.10 0.03],...
    'Fontsize',12);

% Scale bar settings button
scaleBarEdit = uicontrol('Parent',viewFig.sidepanel,'Style','edit',...
    'Callback',@updateScale,'String','20',...
    'Units','normalized',...
    'Position',[0.15 0.05 0.10 0.03],...
    'Fontsize',12);

% Scale bar text
uicontrol('Parent',viewFig.sidepanel,'Style','text',...
    'String','mm',...
    'Units','normalized',...
    'Position',[0.25 0.05 0.1 0.03],...
    'Fontsize',12);


% Load settings button
uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Callback',@loadsettings_Callback,'String','Load settings...',...
    'Units','normalized',...
    'Position',[0.05 0.01 0.4 0.03],...
    'Fontsize',12);

% Save settings button
SaveSettButton = uicontrol('Parent',viewFig.sidepanel,'Style','pushbutton',...
    'Callback',@savesettings_Callback,'String','Save settings...',...
    'Units','normalized',...
    'Position',[0.55 0.01 0.4 0.03],...
    'Fontsize',12,...
    'Enable','off');



%% Item panel

% Panel to contain settings
itempanel = uipanel('Parent',viewFig.sidepanel,...
    'Units','normalized','Position',[0.05 0.13 0.9 0.4]);

% Filename label
fnameLabel = uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized','Position',[0.1 0.85 0.8 0.10],...
    'String','Filename.stl',...
    'HorizontalAlignment','left','Fontsize',12,...
    'FontWeight','bold');

% Color picker
currColor = [0.6 0.8 1];
uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.1 0.79 0.8 0.05],...
    'String','Color:',...
    'HorizontalAlignment','left');
colorPanel = uicontrol('Parent',itempanel,'Style','pushbutton',...
    'Units','normalized','Position',[0.1 0.71 0.3 0.07],...
    'BackgroundColor',currColor,...
    'Callback',@colorPicker_Callback);

% Opacity slider
opacPerc = 100;
opacTxt = uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.1 0.61 0.8 0.05],...
    'String',['Opacity: ' num2str(opacPerc) '%'],...
    'HorizontalAlignment','left');
opacSlider = uicontrol('Parent',itempanel,'Style','slider',...
    'Callback',@editprops_Callback,...
    'String','Opacity','Units','normalized',...
    'Position',[0.1 0.50 0.8 0.1],...
    'Min',0,'Max',100,'Value',opacPerc,...
    'SliderStep',[1/101 10/101]);

% Rotation sliders
rot = [0,0,0];
rotUTxt = uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.1 0.44 0.8 0.05],...
    'String',['U rotation: ' num2str(rot(1)) ' deg'],...
    'HorizontalAlignment','left');
rotUSlider = uicontrol('Parent',itempanel,'Style','slider',...
    'Callback',@editprops_Callback,...
    'String','U rotation','Units','normalized',...
    'Position',[0.1 0.34 0.8 0.1],...
    'Min',-180,'Max',180,'Value',rot(1),...
    'SliderStep',[1/361 10/361]);
rotVTxt = uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.1 0.33 0.8 0.05],...
    'String',['V rotation: ' num2str(rot(2)) ' deg'],...
    'HorizontalAlignment','left');
rotVSlider = uicontrol('Parent',itempanel,'Style','slider',...
    'Callback',@editprops_Callback,...
    'String','V rotation','Units','normalized',...
    'Position',[0.1 0.23 0.8 0.1],...
    'Min',-180,'Max',180,'Value',rot(2),...
    'SliderStep',[1/361 10/361]);
rotWTxt = uicontrol('Parent',itempanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.1 0.22 0.8 0.05],...
    'String',['W rotation: ' num2str(rot(3)) ' deg'],...
    'HorizontalAlignment','left');
rotWSlider = uicontrol('Parent',itempanel,'Style','slider',...
    'Callback',@editprops_Callback,...
    'String','W rotation','Units','normalized',...
    'Position',[0.1 0.12 0.8 0.1],...
    'Min',-180,'Max',180,'Value',rot(3),...
    'SliderStep',[1/361 10/361]);

% Align all button
uicontrol('Parent',itempanel,'Style','pushbutton',...
    'Callback',@alignall_Callback,'String','Align all',...
    'Units','normalized',...
    'Position',[0.65 0.05 0.25 0.08],...
    'Fontsize',12);


%% Create image

if nargin == 1
    loadFile(fpath);
end


%% Callback functions

% Callback to load new file
    function loadFile(obj,~)
        
        % If called by load file button
        if isa(obj,'matlab.ui.control.UIControl')
            % Open 3DR file selector, filter for STL and PLY files
%             matPath = loadVideoFile;   % First get base path
            
%             inppath = {loadFeature(fileparts(temppath),'',{'.stl','.ply'})};
            % Allow manual selection of different file
            [filename, pathname] = ...
                uigetfile({'*.stl;*.ply','STL/PLY files';'*.stl',...
                'STL files';'*.ply',  'PLY files'},...
                'Select STL/PLY file',temppath);
            inppath = {[pathname filesep filename]};
        else
            % Check if it's alreadya  cell array
            if iscell(obj)
                inppath = obj;
            else
                inppath = {obj};
            end
        end
        
            
        % Loop through files
        for ff = 1:length(inppath)
        viewFig.updtxt.String = 'Loading file...';
        viewFig.updtxt.Visible = 'on';
        pause(0.005);
        
        % Read actual file and save in struct(?)
        [~,nme,ext] = fileparts(inppath{ff});
        
        % Put name in struct
        if isfield(alldata,'name')
            nxt = length(alldata.name)+1;
        else
            nxt = 1;
        end
        alldata.name{nxt} = [nme,ext];
        alldata.opacity{nxt} = 20;
        alldata.visible{nxt} = true;
        alldata.color{nxt} = [0.5,0.5,0.5];
        alldata.rotation{nxt} = [0,0,0];
        alldata.activeObj = nxt;
        
        switch lower(ext)
            case '.stl'
                [F,V] = stlread(inppath{ff});
                alldata.faces{nxt} = F;
                alldata.vertices{nxt} = V;
                alldata.extX{nxt,1} = min(alldata.vertices{nxt}(:,1));
                alldata.extX{nxt,2} = max(alldata.vertices{nxt}(:,1));
                alldata.extY{nxt,1} = min(alldata.vertices{nxt}(:,2));
                alldata.extY{nxt,2} = max(alldata.vertices{nxt}(:,2));
                alldata.extZ{nxt,1} = min(alldata.vertices{nxt}(:,3));
                alldata.extZ{nxt,2} = max(alldata.vertices{nxt}(:,3));
                disp(['File ' alldata.name{nxt} ' loaded.']);
            case '.ply'
                [vertex,face] = read_ply(inppath{ff});
                alldata.faces{nxt} = face;
                alldata.vertices{nxt} = vertex;
%                 ptCloud = pcread(inppath{ff});  % This would require Computer vision Toolbox
%                 alldata.ptCloud{nxt} = ptCloud;
                alldata.extX{nxt,1} = min(alldata.vertices{nxt}(:,1));
                alldata.extX{nxt,2} = max(alldata.vertices{nxt}(:,1));
                alldata.extY{nxt,1} = min(alldata.vertices{nxt}(:,2));
                alldata.extY{nxt,2} = max(alldata.vertices{nxt}(:,2));
                alldata.extZ{nxt,1} = min(alldata.vertices{nxt}(:,3));
                alldata.extZ{nxt,2} = max(alldata.vertices{nxt}(:,3));
                disp(['File ' alldata.name{nxt} ' loaded.']);
            otherwise
                error('File not recognized');
        end
        
        updateTable;
        loadRowProps(nxt);
        
        viewFig.updtxt.String = 'File loaded. Updating view...';
        viewFig.updtxt.Visible = 'on';
        pause(0.005);
        end
        
        updateFig;
        SaveSettButton.Enable = 'on';
    end


% Callback to delete file
    function deletefile_Callback(~,~)
        numdelete = alldata.activeObj;
        alldata.activeObj = max(numdelete-1,1);
        
        % Remove entries
        alldata.name = alldata.name((1:end)~=numdelete);
        alldata.opacity = alldata.opacity((1:end)~=numdelete);
        alldata.rotation = alldata.rotation((1:end)~=numdelete);
        alldata.visible = alldata.visible((1:end)~=numdelete);
        alldata.color = alldata.color((1:end)~=numdelete);
        alldata.faces = alldata.faces((1:end)~=numdelete);
        alldata.vertices = alldata.vertices((1:end)~=numdelete);
        alldata.extX = alldata.extX((1:end)~=numdelete);
        alldata.extY = alldata.extY((1:end)~=numdelete);
        alldata.extZ = alldata.extZ((1:end)~=numdelete);
        
        % Remove handles
        alldata = rmfield(alldata,'handles');
        
        % Update figure
        updateFig;
        
        % Update table & info
        updateTable;
        loadRowProps;
    end


% Callback to refresh view
    function refreshfile_Callback(~,~)
        updateFig;
    end


% Callback to load settings
    function loadsettings_Callback(~,~)
        % Select file
        [FileName, PathName] = ...
            uigetfile({'*.mat','MAT files'},...
            'Select settings file',temppath);
        
        % Load file
        if PathName(1)~=0
            load([PathName FileName],'alldata');
        end
        
        SaveSettButton.Enable = 'on';
        
        % Update window panels
        updateTable;
        loadRowProps;
        updateFig;
    end


% Callback to save settings
    function savesettings_Callback(~,~)
        % Select file name and location
        [FileName,PathName,~] = ...
            uiputfile([temppath filesep '3Dview_settings.mat'],...
            'Save settings file as...');
        
        % Save settings if valid path was selected
        if PathName(1)~=0
            save([PathName FileName],'alldata');
        end
    end


% Callback to pop out figure
    function popout_Callback(~,~)
        newfig = figure(10);
        copyobj(viewFig.axes,newfig);
        newfig.Color = bgColorPanel.BackgroundColor;
    end


% Callback after change of parameters in mini-window
    function editprops_Callback(src,~,~)
        val = round(src.Value);
        src.Value = val;
        
        if src == opacSlider
            opacTxt.String = ['Opacity: ' num2str(val) '%'];
            alldata.opacity{alldata.activeObj} = val;
            if alldata.visible{alldata.activeObj}
                alpha(alldata.handles(alldata.activeObj),val/100);
            end
            updateTable;
        else
            ii = alldata.activeObj;
            rr = alldata.rotation{ii};  % Old value
            
            alldata.rotation{ii} = [rotUSlider.Value,...
                    rotVSlider.Value,rotWSlider.Value];
                
            if isgraphics(alldata.handles(alldata.activeObj))
%             if alldata.visible{alldata.activeObj}
                % Unrotate object first
                rotate(alldata.handles(ii),[0 0 1],-rr(3),[0,0,0]);
                rotate(alldata.handles(ii),[0 1 0],-rr(2),[0,0,0]);
                rotate(alldata.handles(ii),[1 0 0],-rr(1),[0,0,0]);
                
                rr = alldata.rotation{ii};
                
                % Then rotate in proper order
                rotate(alldata.handles(ii),[1 0 0],rr(1),[0,0,0]);
                rotate(alldata.handles(ii),[0 1 0],rr(2),[0,0,0]);
                rotate(alldata.handles(ii),[0 0 1],rr(3),[0,0,0]);
            end
            
            % Update slider and property values
            switch src
                case rotUSlider
                    rotUTxt.String = ['U rotation: ' num2str(val) ' deg'];
                case rotVSlider
                    rotVTxt.String = ['V rotation: ' num2str(val) ' deg'];
                case rotWSlider
                    rotWTxt.String = ['W rotation: ' num2str(val) ' deg'];
            end
            
            
        end
    end


% Select object
    function selectObj(~,eventdata,~)
        
        if ~isempty(eventdata.Indices)
            rowno = eventdata.Indices(1);
            colno = eventdata.Indices(2);
            alldata.activeObj = rowno;
            if colno==1
                alldata.visible{rowno} = ~alldata.visible{rowno};
                if alldata.visible{rowno}
                    try
                        alldata.handles(rowno).Visible = 'on';
                    catch
                        updateFig;
                    end
                else
                    try
                        alldata.handles(rowno).Visible = 'off';
                    catch
                    end
                end
            end
        end
        updateTable;
        loadRowProps;
    end


% Color picker
    function colorPicker_Callback(~,~,~)
        c = uisetcolor(alldata.color{alldata.activeObj});
        colorPanel.BackgroundColor = c;
        alldata.color{alldata.activeObj} = c;
        updateTable;
        if isgraphics(alldata.handles(alldata.activeObj))
            alldata.handles(alldata.activeObj).FaceColor = c;
        end
    end

% BG Color picker
    function bgColorPicker_Callback(~,~,~)
        c = uisetcolor(bgColorPanel.BackgroundColor);
        bgColorPanel.BackgroundColor = c;
        viewFig.fig.Color = c;
    end


% Animate view
    function animate_Callback(~,~,~)
        if isvalid(alldata.scale.handle)
            alldata.scale.handle.Visible = 'off';
        end
        if strcmp(alldata.lighting.type,'camera')
            mod = 1;
%             [az,el] = lightangle(alldata.lighting.handle);
%             alldata.lighting.az = az;
%             alldata.lighting.el = el;
            az = alldata.lighting.az;
            el = alldata.lighting.el;
        else
            mod = 0;
        end
        while animButton.Value == true
            animButton.String = 'Stop animation';
%             for i=1:300
                camorbit(viewFig.axes,5,0,'camera')
                if mod == 1
                    camlight(alldata.lighting.handle,az,el);
                end
                drawnow
                pause(0.005)
%             end
        end
        animButton.String = 'Animate';
        if isvalid(alldata.scale.handle)
            alldata.scale.handle.Visible = 'on';
        end
    end


% Callback after rotation
    function rotCallback(~,~)
        % Save view angles
        [alldata.view.az,alldata.view.el] = view;
        if strcmp(alldata.lighting.type,'camera')
            az = alldata.lighting.az;
            el = alldata.lighting.el;
            camlight(alldata.lighting.handle,az,el);
        else
            [az,el] = lightangle(alldata.lighting.handle);
            alldata.lighting.az = az;
            alldata.lighting.el = el;
        end
        updateScale;
    end

% Callback to save GIF
    function saveGIF_Callback(~,~)
        
        % Turn off scale and additional text
        viewFig.updtxt.Visible = 'off';
        if isfield(alldata.scale,'handle')
            if isvalid(alldata.scale.handle)
                alldata.scale.handle.Visible = 'off';
            end
        end
            
        [fname,pname,~] = uiputfile(...
            {'*.gif','GIF file';'*.mp4','MP4 file'},...
            'Save as GIF/MP4...',...
            [temppath filesep 'rotating_model.gif']);
        [~,~,ext] = fileparts(fname);
        % Set rotation per frame dependent on file type
        if strcmp(ext,'.mp4')
            int = 3;
        else
            int = 5;
        end
        if strcmp(alldata.lighting.type,'camera')
            mod = 1;
            az = alldata.lighting.az;
            el = alldata.lighting.el;
        else
            mod = 0;
        end
        viewFig.axes.Units = 'pixels';
        viewport = viewFig.axes.OuterPosition;
        tempcolor = viewFig.fig.Color;
        if tempcolor == [0.8 0.8 0.8]
            viewFig.fig.Color = [1 1 1];
        end
        f = getframe(viewFig.fig,viewport);
        [im,map] = rgb2ind(f.cdata,256,'dither');
        k = 1;
        im(1,1,1,floor(360/int)) = 0;
        for i=1:360/int
            camorbit(viewFig.axes,int,0,'camera');
                if mod == 1
                    camlight(alldata.lighting.handle,az,el);
                end
                drawnow
                viewFig.updtxt.Visible = 'off';
                pause(0.005)
                % Collect frame
                f(i) = getframe(viewFig.fig,viewport);
%                 size(rgb2ind(f.cdata,map,'dither'))
%                 size(im(:,:,1,k))
                tempK = rgb2ind(f(i).cdata,map,'dither');
                im(1:size(tempK,1),1:size(tempK,2),1,k) = tempK;
                k=k+1;
        end
        % If mp4 selected, create video object
        if strcmp(ext,'.mp4')
            vid = VideoWriter([pname fname],'MPEG-4');
            open(vid);
            writeVideo(vid,f);
            close(vid);
        else
            % Fix size off image with a border
            pad = 50;
            imtemp = sum(double(im)-double(im(1)),4);
            yext = sum(imtemp,1)~=0;
            yst = max(1,find(yext,1,'first')-pad);
            yend = min(length(yext),find(yext,1,'last')+pad);
            xext = sum(imtemp,2)~=0;
            xst = max(1,find(xext,1,'first')-pad);
            xend = min(length(xext),find(xext,1,'last')+pad);
            
            imwrite(im(xst:xend,yst:yend,:,:),map,[pname filesep fname],...
                'DelayTime',0,...
                'LoopCount',inf);
        end
        viewFig.axes.Units = 'normalized';
        viewFig.fig.Color = tempcolor;
        updateScale;
    end


%% Change lighting callbacks
% Main lighting button
    function lighting_Callback(~,~)
        % Get current settings
        currP = get (gcf, 'CurrentPoint');
        currPos = get(gcf,'Position');
        newPos = [currP(1) + currPos(1) + 50, currP(2) + currPos(2) + 50,...
            200, 400];
        amb = alldata.handles(1).AmbientStrength;
        diffs = alldata.handles(1).DiffuseStrength;
        spec = alldata.handles(1).SpecularStrength;
        
        if strcmp(alldata.lighting.type,'camera')
            az = alldata.lighting.az;
            el = alldata.lighting.el;
        else
            [az,el] = lightangle(alldata.lighting.handle);
        end
        
%         [az,el] = lightangle(alldata.lighting.handle);

        if az<-180
            az = 180 + mod(az,-180);
        elseif az>180
            az = -180 + mod(az,180);
        end
        if el<-180
            el = 180 + mod(el,-180);
        elseif el>180
            el = -180 + mod(el,180);
        end
        %         lightType = alldata.handles.s
        
        % Build new window
        lightingFig = figure('Menubar','none','Toolbar','none','Name',...
            'Change lighting','NumberTitle','on','IntegerHandle','off',...
            'Units','Pixels','Position',newPos);
        xpos = 20;
        hpos = 20;
        wpos = lightingFig.Position(3)-2*xpos;
        sp = 10;
        ypos = lightingFig.Position(4)-hpos;
        
        % Create toggle button for lighting style
        ypos = ypos-hpos;
        uicontrol('Style', 'text', 'String', 'Light relative to...',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        lightTypeControl = uicontrol('Style', 'togglebutton', 'String', ...
            alldata.lighting.type,...
            'Position', [xpos ypos wpos hpos],...
            'Callback', @updateLightType);
        lightTypeControl.Value = strcmp(alldata.lighting.type,'object');
        
        % Azimuth light slider
        ypos = ypos - hpos - sp - sp;
        uicontrol('Style', 'text', 'String', 'Light azimuth',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        azControl = uicontrol('Style', 'slider', 'TooltipString', ...
            num2str(az),...
            'Position', [xpos ypos wpos hpos],...
            'Min',-180,'Max',180,'Value',az,...
            'Tag','az',...
            'Callback', @updateLightSource);
        
        % elevation light slider
        ypos = ypos - hpos - sp;
        uicontrol('Style', 'text', 'String', 'Light elevation',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        elControl = uicontrol('Style', 'slider', 'TooltipString', ...
            num2str(el),...
            'Position', [xpos ypos wpos hpos],...
            'Min',-180,'Max',180,'Value',el,...
            'Tag','el',...
            'Callback', @updateLightSource);        
        
        
        % Ambient light slider
        ypos = ypos - hpos - sp - sp;
        uicontrol('Style', 'text', 'String', 'Ambient',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        uicontrol('Style', 'slider', 'TooltipString', ...
            num2str(100*amb),...
            'Position', [xpos ypos wpos hpos],...
            'Min',0,'Max',100,'Value',100*amb,...
            'Tag','Ambient',...
            'Callback', @updateLight);
        
        % Diffuse light slider
        ypos = ypos - hpos - sp;
        uicontrol('Style', 'text', 'String', 'Diffuse',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        uicontrol('Style', 'slider', 'TooltipString', ...
            num2str(100*diffs),...
            'Position', [xpos ypos wpos hpos],...
            'Min',0,'Max',100,'Value',100*diffs,...
            'Tag','Diffuse',...
            'Callback', @updateLight);
        
        % Specular light slider
        ypos = ypos - hpos - sp;
        uicontrol('Style', 'text', 'String', 'Specular',...
            'Position', [xpos ypos wpos hpos], ...
            'HorizontalAlignment','left');
        ypos = ypos-hpos;
        uicontrol('Style', 'slider', 'TooltipString', ...
            num2str(100*spec),...
            'Position', [xpos ypos wpos hpos],...
            'Min',0,'Max',100, 'Value',100*spec,...
            'Tag','Specular',...
            'Callback', @updateLight);
        
        % Reset button
        ypos = ypos - hpos - sp - sp;
        uicontrol('Style', 'pushbutton', 'String', 'Reset',...
            'Position', [xpos ypos 0.4*wpos hpos],...
            'Callback', @resetLight);
        
        % Close button
        uicontrol('Style', 'pushbutton', 'String', 'Close',...
            'Position', [xpos+wpos-0.4*wpos ypos 0.4*wpos hpos],...
            'Callback', 'close(gcf)');
        
        function resetLight(~,~)
            alldata.lighting.AmbientStrength = amb;
            alldata.lighting.DiffuseStrength = diffs;
            alldata.lighting.SpecularStrength = spec;
            for i = 1:length(alldata.handles)
                alldata.handles(i).AmbientStrength = amb;
                alldata.handles(i).DiffuseStrength = diffs;
                alldata.handles(i).SpecularStrength = spec;
            end
        end
        
        function updateLight(src,~)
            val = round(src.Value);
            src.Value = val;
            src.TooltipString = num2str(val);
            
            val = val/100; %#ok<NASGU>
            for i=1:length(alldata.handles)
                eval(['alldata.handles(i).' src.Tag 'Strength = val;']);
                eval(['alldata.lighting.' src.Tag 'Strength = val;']);
            end
        end
        
        function updateLightType(src,~)
            [az,el] = lightangle(alldata.lighting.handle);
            switch (src.Value)
                case 0
                    alldata.lighting.type = 'camera';
                    [camaz,camel] = view(viewFig.axes);
                    az = az-camaz;
                    el = el-camel;
                case 1
                    alldata.lighting.type = 'object';
                    
            end
            src.String = alldata.lighting.type;
            azControl.Value = az;
            azControl.TooltipString = num2str(az);
            
            if az<-180
                az = 180 + mod(az,-180);
            elseif az>180
                az = -180 + mod(az,180);
            end
            if el<-180
                el = 180 + mod(el,-180);
            elseif el>180
                el = -180 + mod(el,180);
            end
            
            azControl.Value = az;
            azControl.TooltipString = num2str(az);
            elControl.Value = el;
            elControl.TooltipString = num2str(el);
            pause(0.005);
            
            alldata.lighting.az = azControl.Value;
            alldata.lighting.el = elControl.Value;
        end
        
        function updateLightSource(src,~)
            val = round(src.Value);
            src.Value = val;
            src.TooltipString = num2str(val);
            
            if strcmp(alldata.lighting.type,'camera')
                camlight(alldata.lighting.handle,azControl.Value,...
                    elControl.Value);
            else
                lightangle(alldata.lighting.handle,azControl.Value,...
                    elControl.Value);
            end
            alldata.lighting.az = azControl.Value;
            alldata.lighting.el = elControl.Value;
            pause(0.005);
        end
    end

% Callback to align all objects
    function alignall_Callback(~,~)
        % Get current rotation values
        rotU = rotUSlider.Value;
        rotV = rotVSlider.Value;
        rotW = rotWSlider.Value;
        
        % Loop through all objects
        for i=1:size(alldata.handles,2)
            % Apply rotation values
            alldata.rotation(i) = {[rotU rotV rotW]};
        end
        updateFig;
    end


%% Other functions
% Make plot
    function [] = updateFig(~,~)
        if isfield(alldata,'lighting')
            az = alldata.lighting.az;
            el = alldata.lighting.el;
        end
        cla;
        viewFig.updtxt.String = 'Updating figure...';
        viewFig.updtxt.Visible = 'on';
        pause(0.005);
        
        
        % Update settings if needed
        
        % Draw
        axes(viewFig.axes);
        for i=1:length(alldata.name)
            if alldata.visible{i}
                alldata.handles(i) = patch('Faces',alldata.faces{i},...
                    'Vertices',alldata.vertices{i},...
                    'FaceColor',alldata.color{i},'EdgeColor','none');
                alpha(alldata.handles(i),alldata.opacity{i}/100);
                if sum(abs(alldata.rotation{i}))>0
                    rr = alldata.rotation{i};
                    rotate(alldata.handles(i),[1 0 0],rr(1),[0,0,0]);
                    rotate(alldata.handles(i),[0 1 0],rr(2),[0,0,0]);
                    rotate(alldata.handles(i),[0 0 1],rr(3),[0,0,0]);
                    
                    % Fix limits
                    alldata.extX{i,1} = min(alldata.handles(i).XData(:));
                    alldata.extX{i,2} = max(alldata.handles(i).XData(:));
                    alldata.extY{i,1} = min(alldata.handles(i).YData(:));
                    alldata.extY{i,2} = max(alldata.handles(i).YData(:));
                    alldata.extZ{i,1} = min(alldata.handles(i).ZData(:));
                    alldata.extZ{i,2} = max(alldata.handles(i).ZData(:));
                end
                hold on
            end
        end
        hold off
        
        ok = [alldata.visible{1:end}];
        viewFig.axes.XLim = [min([alldata.extX{ok,1}]) max([alldata.extX{ok,2}])];
        viewFig.axes.YLim = [min([alldata.extY{ok,1}]) max([alldata.extY{ok,2}])];
        viewFig.axes.ZLim = [min([alldata.extZ{ok,1}]) max([alldata.extZ{ok,2}])];
        
        % Set view orientation
        viewFig.axes.CameraViewAngleMode = 'Manual';
        %         isonormals(IMAGEsm,hiso)
%         alpha(0.2)  %sets level of transparency
        if isfield(alldata,'view') == 1
            view([alldata.view.az,alldata.view.el]);
        else
            view(3);
        end
        lighting gouraud
        grid on
        axis xy
%         daspect([1 1 1]);
        axis equal
        % % savefig([outdata(1:end-4),'.fig']);
        
        % Set lighting
        if exist('az','var') == 1
            alldata.lighting.handle = camlight(viewFig.axes,az,el);
            if isfield(alldata.lighting,'AmbientStrength')
                indx = find(cell2mat(alldata.visible)>0);
                for i=indx
                    alldata.handles(i).AmbientStrength = alldata.lighting.AmbientStrength;
                    alldata.handles(i).DiffuseStrength = alldata.lighting.DiffuseStrength;
                    alldata.handles(i).SpecularStrength = alldata.lighting.SpecularStrength;
                end
            end
        else
            alldata.lighting.handle = camlight(viewFig.axes,'right');
            alldata.lighting.type = 'camera';
            [az,el] = lightangle(alldata.lighting.handle);
            alldata.lighting.az = az;
            alldata.lighting.el = el;
        end
        
        [alldata.view.az,alldata.view.el] = view;
        
        try
            updateScale;
        catch
        end

        viewFig.updtxt.String = 'Figure updated.';
        pause(0.3);
        viewFig.updtxt.Visible = 'off';
    end

% Update scale bar
    function updateScale(~,~)
        alldata.scale.length = str2double(scaleBarEdit.String);
        if alldata.scale.length > 0
            axes(viewFig.axes);
            [azi,ele] = view;
            [Z,X,Y] = cylinder(0.5);
            zlims = viewFig.axes.ZLim;
            alldata.scale.zoffset = -0.15*diff(zlims) + zlims(1);
            if isfield(alldata.scale,'handle')
                if isvalid(alldata.scale.handle)
                    alldata.scale.handle.XData = X;
                    alldata.scale.handle.YData = Y*alldata.scale.length;
                    alldata.scale.handle.ZData = Z + alldata.scale.zoffset;
                else
                    hold on
                    alldata.scale.handle = surf(X,Y*alldata.scale.length,...
                        Z + alldata.scale.zoffset,...
                        'FaceColor',[0 0 0]);
                    hold off
                end
                alldata.scale.handle.Visible = 'on';
            else
                hold on
                alldata.scale.handle = surf(X,Y*alldata.scale.length,...
                    Z + alldata.scale.zoffset,...
                    'FaceColor',[0 0 0]);
                hold off
            end
            rotate(alldata.scale.handle,[0 1 0],newele);
            newazi = azi-90;
            rotate(alldata.scale.handle,[0 0 1],newazi);
            alldata.scale.handle.SpecularStrength = 0;
        else
            if isfield(alldata.scale,'handle')
                if isvalid(alldata.scale.handle)
                    alldata.scale.handle.Visible = 'off';
                end
            end
        end
        
    end

% Update table
    function updateTable(~,~)
        vis = alldata.visible';
        names = alldata.name';
        color = cellfun(@(x) [num2str(x) '%'], alldata.opacity, 'un', 0)';
        
        % Adjust name if not visible
        for i = 1:size(names,1)
            if ~alldata.visible{i}
                names{i} = ['<html><p style="color:999999">'...
                    names{i} '</p></html>'];
            end
        end
        
        
        T = table(vis,names,color);
        data = table2cell(T);
        ftable.Data = data;
        bgcolor = ones(length(alldata.name),3);
        bgcolor(alldata.activeObj,1:3) = [0.9 0.9 0.9];
        ftable.BackgroundColor = bgcolor;
        
        % Update bg colors
        for i = 1:size(data,1)
            data{i,3} = ['<html><p style="background-color:rgb('...
                num2str(alldata.color{i}(1)*255) ',' ...
                num2str(alldata.color{i}(2)*255) ',' ...
                num2str(alldata.color{i}(3)*255)...
                ');">&nbsp;' num2str(alldata.opacity{i}) '%&nbsp;</p></html>'];
        end
        ftable.Data = data;
    end


% Switch selected component
    function loadRowProps(~)
        rowno = alldata.activeObj;
        fnameLabel.String = alldata.name{rowno};
        opacTxt.String = ['Opacity: ' num2str(alldata.opacity{rowno}) '%'];
        opacSlider.Value = alldata.opacity{rowno};
        colorPanel.BackgroundColor = alldata.color{rowno};
        
        rr = alldata.rotation{rowno};
        rotUTxt.String = ['U rotation: ' num2str(rr(1)) ' deg'];
        rotUSlider.Value = rr(1);
        rotVTxt.String = ['V rotation: ' num2str(rr(2)) ' deg'];
        rotVSlider.Value = rr(2);
        rotWTxt.String = ['W rotation: ' num2str(rr(3)) ' deg'];
        rotWSlider.Value = rr(3);
    end


end
