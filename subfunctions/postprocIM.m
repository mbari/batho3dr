%% Information
% This program (called by ThreeDR_CropPreProc) takes the crop and
% adjustment settings determined previously (and saved in the struct
% 'output'), and calculates the entire resulting stack of frames,
% performing trim, stabilization, crop and background subtraction, where
% the background is based on a median filter.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 11th, 2018

function newIMAGE=postprocIM(IMAGE,output,exportfile)

if ischar(IMAGE)
    imgstack = 0;
    % Initialize videoreader object
    vid = VideoReader(IMAGE);
    % Estimate number of frames
    %     numfr = ceil(vid.FrameRate*vid.Duration);
    im1 = readFrame(vid);                % Read frame
    if isfield(output,'channelmix')
        channelmix = output.channelmix;
    else
        % Get channel mix
        [channelmix] = selectChannelMix(im1);
        output.channelmix = channelmix;
    end
    if channelmix>3
        im1 = rgb2gray(im1);
    else
        im1 = im1(:,:,channelmix);
    end
    sizeIm = size(im1);
    sizeIm(3) = vid.NumFrames;
else
    imgstack = 1;
    sizeIm = size(IMAGE);
    if length(sizeIm)<3
        sizeIm(3) = 1;
    end
end

sizeImcorr = sizeIm + round([max(output.motioncorr(:,3)),...
    max(output.motioncorr(:,2)), 0]);
interpMethod = 'linear';
if isfield(output,'autostab')
    if isfield(output.autostab,'interpMethod')
        interpMethod = output.autostab.interpMethod;
    end
end

if isfield(output,'TCrop')
    tcrop=output.TCrop;
    bcrop=output.BCrop;
    lcrop=output.LCrop;
    rcrop=output.RCrop;
else
    tcrop=1;
    bcrop=sizeImcorr(1);
    lcrop=1;
    rcrop=sizeImcorr(2);
end

if isfield(output,'startFrame')
    reqframes = output.startFrame:1:output.endFrame;    % Requested frames
    if ~imgstack
        vid.CurrentTime = (output.startFrame-1)/vid.FrameRate;
    end
else
    reqframes = 1:sizeIm(3);
    vid.CurrentTime = 0;
end

% Generate new empty image of correct size
newIMAGE = zeros(bcrop-tcrop+1,rcrop-lcrop+1,...
    numel(reqframes),'uint8');

% Initialize for stabilization
[Xq,Yq] = meshgrid(1:sizeImcorr(2),1:sizeImcorr(1));
Xq = Xq(tcrop:bcrop,lcrop:rcrop);
Yq = Yq(tcrop:bcrop,lcrop:rcrop);

% % If requested, prepare file for export
% if nargin<3
%     exportfile = 0;
% else
%     [PathName,FileName,ext] = fileparts(FileName);
%     if strcmp('.tif',lower(ext))         % If exporting image stack
%         exportimstack = 1;
%         mkdir([PathName filesep FileName]);
%         baseFileName = [PathName, filesep, FileName, filesep, FileName, ...
%             '_'];
%     else                        % If exporting video
%         exportimstack = 0;
%         newFileName = [PathName, filesep, FileName];
%         
%         % Build video file
%         switch lower(ext)
%             case '.mp4'
%                 v = VideoWriter(newFileName,'MPEG-4' );
%                 v.Quality = 90;
%             case '.avi'
%                 v = VideoWriter(newFileName,'Uncompressed AVI' );
%             case '.mj2'
%                 v = VideoWriter(newFileName,'Archival' );
%         end
%         v.FrameRate = frrate;
%         open(v);
%     end
% end

% Loop through all requested frames
for currfr = 1:numel(reqframes)
    
    % Retrieve relevant frame
    if imgstack
        currim = IMAGE(:,:,reqframes(currfr));
    else
        if hasFrame(vid)
            currim = readFrame(vid);
            if channelmix>3
                currim = rgb2gray(currim);
            else
                currim = currim(:,:,channelmix);
            end
        end
    end
    
    % Interpolate for stabilization
    try
        [X,Y] = meshgrid((1:sizeIm(2)) + ...
            output.motioncorr(reqframes(currfr),2),...
            (1:sizeIm(1)) + output.motioncorr(reqframes(currfr),3));
        newIMAGE(:,:,currfr) = interp2(X,Y,double(currim),Xq,Yq,interpMethod,0);
    catch
        disp('catch error');
    end
end

if isfield(output,'bgsubblend')
    bgblend = output.bgsubblend;
else
    bgblend = 100;
end
bg = [];

if isfield(output,'bgminfilt')
    if output.bgminfilt>1
        % Make sure it is integer
        output.bgminfilt = round(output.bgminfilt);
        bg = movmin(newIMAGE,output.bgminfilt,3);
    end
end

if isfield(output,'bgmedfilt')
    if output.bgmedfilt>1
        %         % Make sure it is odd
        %         output.bgmedfilt = 2*round((output.bgmedfilt-1)/2)+1;
        % Make sure it is integer
        output.bgmedfilt = round(output.bgmedfilt);
        if isempty(bg)
            bg = medfilt3(newIMAGE,[1 1 output.bgmedfilt],'symmetric');
        else
            bg = bg + medfilt3(newIMAGE-bg,[1 1 output.bgmedfilt],'symmetric');
        end
    end
end

if ~isempty(bg)
    newIMAGE = newIMAGE - bg*(bgblend/100);
end


% for currfr = 1:numel(reqframes)
%     if exportfile == 1
%         if exportimstack == 1
%             % Make new file name
%             newFileName = [baseFileName, num2str(currfr-1,...
%                 ['%0' num2str(ceil(log10(numel(reqframes)))) 'u']), ext];
%             % Save as image
%             imwrite(newfr,newFileName);
%         else
%             writeVideo(v,newfr);
%             if currfr == numel(reqframes)
%                 close(v);
%             end
%             
%         end
%     end
% end

end