% Plan:
% - GUI with mask overlaid on original
% - 3D median filter
% - Buttons to apply median filter or go back to editing filter
% - buttons to start removing blobs or pixels (pixels are 1D, blobs should
% be 3D)
% - removed areas turn red, remaining areas are green
% - Finish (apply) button
% - Output is an updated 3D mask

function outputmask = manualMaskEditor(immaskIN,IMAGEcrop,output)

if nargin < 3
    curvpoints = [0 0; 127 127; 255 255];
else
    curvpoints = output.curves;
end
if nargin<2
    IMAGEcrop = immaskIN*255;
end
immaskIN = immaskIN>0;      % Make logical
immaskMOD = immaskIN;       % Name of the latest modified mask
immaskMEDFILT = immaskIN;   % Name of the original mask, but with median filter applied
modactive = 0;              % Freeform modifications are inactive

%% Initialize window settings

% Create curves mapping
curvmap = create_curvmap(curvpoints);

sizeIm=size(immaskIN);
sizeIm(3) = size(immaskIN,3);   % Add explicitly in case of single frames
wa=get(0,'screensize');
screenwidth=wa(3);screenheight=wa(4);
minSlice=1;maxSlice=sizeIm(3);slice=round(mean([minSlice maxSlice]));
smallStep=1/sizeIm(3);
largeStep=10/sizeIm(3);
sliderlength=300;
textlength=200;
spacer=130;

% Set initial OUTER window size
% if or(sizeIm(2)>screenwidth/2,sizeIm(1)>screenheight/2)
outerwidth = sizeIm(2)+sliderlength + 150;
outerheight = sizeIm(1)+150;
if or(outerwidth>screenwidth,outerheight>screenheight)
    outerwidth=screenwidth-70;
    outerheight=screenheight-120;
    xpos = 50;
    ypos = 100;
elseif and(outerwidth<screenwidth/2,outerheight<screenheight/2)
    outerwidth = sizeIm(2)*2+sliderlength + 150;
    outerheight = sizeIm(1)*2+150;
    xpos = (screenwidth-outerwidth)/2;
    ypos = (screenheight-outerheight)/2;
else
    xpos = (screenwidth-outerwidth)/2;
    ypos = (screenheight-outerheight)/2;
end
% 
% if screenwidth<sizeIm(2)+60+sliderlength
%     width=screenwidth;
%     height=screenheight;
%     xpos = 0;
%     ypos = 0;
% else
%     width=sizeIm(2) + 60 + sliderlength;
%     height=sizeIm(1) + 100;
%     xpos = (screenwidth-sizeIm(2))/2;
%     ypos = (screenheight-sizeIm(1))/2;
% end
% sliderleft = width - sliderlength - 20;


%% Build main window and controls

% Build main window
hfig = figure('Menubar','none','Toolbar','none','Name','Manual Mask Editor',...
    'NumberTitle','on','IntegerHandle','off','Units','Pixels',...
    'InnerPosition',[xpos ypos outerwidth outerheight]);

% Determine inner size and set up other dimensions
temp = get(hfig,'InnerPosition');
width = temp(3); height = temp(4);
sliderleft = width - sliderlength - 20;
figsizex = sliderleft - 40;
figsizey = height - 80 - 60;
if and(sizeIm(2)*1.8<figsizex,sizeIm(1)*1.8<figsizey)
    figsizex = 2*sizeIm(2);
    figsizey = 2*sizeIm(1);
elseif or(sizeIm(2)<figsizex,sizeIm(1)<figsizey)
    figsizex = sizeIm(2);
    figsizey = sizeIm(1);
end

% Build axes
haxes = axes(hfig,'Units','Pixels','Position',[20 80 figsizex figsizey]);
haxes.XLim = [1 sizeIm(2)];
haxes.YLim = [1 sizeIm(1)];
hfigZoom = zoom(hfig);
hfigPan = pan(hfig);
set(hfigZoom,'ActionPostCallback',@(x,y)getZoomVal());

topbuttony = 80+figsizey+15;
topbuttonh = 30;
topbuttonleft = 20+figsizex-310;
zoomval = 100;

% Zoom toggle button
buttonZoom=uicontrol('Style','togglebutton',...
    'Callback',@buttonZoom_Callback,...
    'String','Zoom OFF [100%]','Units','pixels','Fontsize',11,...
    'Position',[topbuttonleft topbuttony 120 topbuttonh],...
    'Enable','on','Value',0);

% Reset zoom push button
buttonResetZoom=uicontrol('Style','pushbutton',...
    'Callback',@buttonResetZoom_Callback,...
    'String','Reset zoom','Units','pixels','Fontsize',11,...
    'Position',[topbuttonleft+130 topbuttony 90 topbuttonh],...
    'Enable','off');

% Pan toggle button
buttonPan=uicontrol('Style','togglebutton',...
    'Callback',@buttonPan_Callback,...
    'String','Pan OFF','Units','pixels','Fontsize',11,...
    'Position',[topbuttonleft+230 topbuttony 80 topbuttonh],...
    'Enable','off','Value',0);

% "Load primary mask" button
buttonLoadMask1=uicontrol('Style','pushbutton',...
    'Callback',@buttonLoadMask1_Callback,...
    'String','Load mask...',...
    'ToolTipString','Load different primary mask...',...
    'Units','pixels','Fontsize',12,...
    'Position',[sliderleft height-50 sliderlength*0.47 30]);

% "Load BG image" button
buttonLoadBG=uicontrol('Style','pushbutton',...
    'Callback',@buttonLoadBG_Callback,...
    'String','Load BG image...',...
    'ToolTipString','Load a different background image to aid in mask painting',...
    'Units','pixels','Fontsize',12,...
    'Position',[sliderleft+sliderlength*0.53 height-50 sliderlength*0.47 30]);

% "Add mask" button
buttonAddMask=uicontrol('Style','pushbutton','Callback',@addMask_Callback,...
    'String','Add mask...','Units','pixels','Fontsize',12,...
    'Position',[sliderleft+(sliderlength/2-spacer)/2 height-90 spacer 30]);

% "Subtract mask" button
buttonSubMask=uicontrol('Style','pushbutton','Callback',@addMask_Callback,...
    'String','Subtract mask...','Units','pixels','Fontsize',12,...
    'Position',[sliderleft+(3*sliderlength/2-spacer)/2 height-90 spacer 30]);

totheight = 140+60-30+100;
numsliders = 7;
lineheight = totheight / (3*numsliders);
topheight = height-100;

% Background opacity text and slider
slidernum = 0;
bgopacitytxt=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Background opacity: ',num2str(50),'%'],...
    'HorizontalAlignment','left','Fontsize',9);
sliderbgopacity=uicontrol('Parent',hfig,'Style','slider','Callback',@bgopacity_Callback,...
    'String','Threshold','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[0.02 0.2]);
set(sliderbgopacity,'Min',0,'Max',150,'Value',50);

% Overlay opacity text and slider
slidernum = slidernum+1;
overlayopacitytxt=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Overlay opacity: ',num2str(80),'%'],...
    'HorizontalAlignment','left','Fontsize',9);
slideroverlayopacity=uicontrol('Parent',hfig,'Style','slider','Callback',@overlayopacity_Callback,...
    'String','Threshold','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[0.02 0.2]);
set(slideroverlayopacity,'Min',0,'Max',100,'Value',80);

% Median filter text and slider
slidernum = slidernum + 1;
medfilttxt=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['3D median filter size: ',num2str(1)],...
    'HorizontalAlignment','left','Fontsize',9);
slidermedfilt=uicontrol('Parent',hfig,'Style','slider','Callback',@medfilt_Callback,...
    'String','Threshold','Units','pixels','Position',[sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[2/(33-1) 5/(33-1)]);
set(slidermedfilt,'Min',1,'Max',33,'Value',1);

% Imclose filter text and slider
slidernum = slidernum + 1;
txtimclose=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Imclose (dilation + erosion) size: ',num2str(0)],...
    'HorizontalAlignment','left','Fontsize',9);
sliderimclose=uicontrol('Parent',hfig,'Style','slider','Callback',@imclose_Callback,...
    'String','Threshold','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[1/(200) 5/(200)]);
set(sliderimclose,'Min',0,'Max',200,'Value',0);

% Imopen filter text and slider
slidernum = slidernum + 1;
txtimopen=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Imopen (erosion + dilation) size: ',num2str(0)],...
    'HorizontalAlignment','left','Fontsize',9);
sliderimopen=uicontrol('Parent',hfig,'Style','slider','Callback',@imopen_Callback,...
    'String','Threshold','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[1/(30) 5/(30)]);
set(sliderimopen,'Min',0,'Max',30,'Value',0);

% Imdilate filter text and slider
slidernum = slidernum + 1;
txtimdilate=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Dilation size: ',num2str(0)],...
    'HorizontalAlignment','left','Fontsize',9);
sliderimdilate=uicontrol('Parent',hfig,'Style','slider','Callback',@imdilate_Callback,...
    'String','Sphere size','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[1/(30) 5/(30)]);
set(sliderimdilate,'Min',0,'Max',30,'Value',0);

% bwareaopen filter text and slider
slidernum = slidernum + 1;
txtbwareaopen=uicontrol('Style','text','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight],...
    'String',['Minimum feature size: ',num2str(0)],...
    'HorizontalAlignment','left','Fontsize',9);
sliderbwareaopen=uicontrol('Parent',hfig,'Style','slider','Callback',@bwareaopen_Callback,...
    'String','Threshold','Units','pixels','Position',...
    [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight],...
    'SliderStep',[1/200 5/100]);
set(sliderbwareaopen,'Min',0,'Max',50,'Value',0);

% "Apply median filter" button
buttonApplyMedian=uicontrol('Parent',hfig,'Style','pushbutton','Callback',@medfiltApply_Callback,...
    'String','Apply filter to stack','Units','pixels','Fontsize',12,...
    'Position',[sliderleft+(sliderlength/2-spacer)/2 height-400 spacer 30]);
medfiltapplytxt=uicontrol('Parent',hfig,'Style','text','Units','pixels',...
    'Position',[sliderleft+(sliderlength/2-spacer)/2+10 height-415 spacer 15],...
    'Fontsize',8,'String','Calculating...','HorizontalAlignment','left',...
    'Visible','off');

% "Reset median filter" button
buttonResetMedian=uicontrol('Parent',hfig,'Style','pushbutton','Callback',@medfiltReset_Callback,...
    'String','Edit filter','Units','pixels','Fontsize',12,...
    'Position',[sliderleft+(3*sliderlength/2-spacer)/2 height-400 spacer 30],...
    'Enable','off');

%% Panel for mask operations
% Create panel for mask operations

modpanel = uipanel('Title','Manual operations','FontSize',12,...
    'Units','Pixels','Position',[sliderleft 140+150 sliderlength max(height-320-140-150-100,150)],...
    'Visible','off');

% "Remove blobs" button
buttonRemoveBlobs=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@RemoveBlobs_Callback,...
    'String','Exclude Blobs','Units','normalized','Fontsize',12,...
    'Position',[0.05 0.89 0.43 0.09],'BusyAction','cancel',...
    'Tooltipstring','Remove blobs by clicking / selection');
% txtRemoveBlobs=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
%     'Position',[0.1 0.81 0.8 0.08],'Fontsize',10,...
%     'String','Remove blobs by clicking / selection','HorizontalAlignment','center');

% "Include blobs" button
buttonIncludeBlobs=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@IncludeBlobs_Callback,...
    'String','Include Blobs','Units','normalized','Fontsize',12,...
    'Position',[0.52 0.89 0.43 0.09],... % [0.1 0.72 0.8 0.08]
    'Tooltipstring','Include blobs by clicking');
% txtIncludeBlobs=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
%     'Position',[0.1 0.64 0.8 0.08],'Fontsize',10,...
%     'String','Include blobs by clicking','HorizontalAlignment','center');

% "Remove pixels" button
buttonRemovePix=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@RemovePix_Callback,...
    'String','Remove Pixels','Units','normalized','Fontsize',12,...
    'Position',[0.05 0.79 0.43 0.09],... %[0.1 0.55 0.8 0.08]
    'Tooltipstring','Remove pixels by drawing a freeform shape');
% txtRemovePix=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
%     'Position',[0.1 0.47 0.8 0.08],'Fontsize',10,...
%     'String','Remove pixels by drawing a freeform shape','HorizontalAlignment','center');

% "Include pixels" button
buttonIncludePix=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@IncludePix_Callback,...
    'String','Include Pixels','Units','normalized','Fontsize',12,...
    'Position',[0.52 0.79 0.43 0.09],...
    'Tooltipstring','Include pixels from secondary mask by drawing a freeform shape');
% txtIncludePix=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
%     'Position',[0.1 0.47 0.8 0.08],'Fontsize',10,...
%     'String','Include pixels from secondary mask by drawing a freeform shape','HorizontalAlignment','center');

% "Add manual pixels" button
buttonAddManPix=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@AddManPix_Callback,...
    'String','Add Manual Pixels','Units','normalized','Fontsize',12,...
    'Position',[0.05 0.69 0.43 0.09],...
    'Tooltipstring','Add individual pixels manually by clicking in the image');

% "Fill holes" button
buttonFillHoles=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@FillHoles_Callback,...
    'String','Fill holes','Units','normalized','Fontsize',12,...
    'Position',[0.52 0.69 0.43 0.09]);

% Brush size
txtbrushsize=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
    'Position',[0.1 0.61 0.8 0.04],...
    'String',['Brush size: ',num2str(4)],...
    'HorizontalAlignment','left','Fontsize',9);
sliderbrushsize=uicontrol('Parent',modpanel,'Style','slider','Callback',@brushsize_Callback,...
    'String','Slice extent','Units','normalized','Position',[0.1 0.48 0.8 0.08],...
    'SliderStep',[1/(29) 5/(29)],'Min',1,'Max',30,'Value',4);

% Slice extent of edit - text and slider
txtextent=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
    'Position',[0.1 0.42 0.8 0.04],...
    'String',['z extent of actions: ',num2str(1),' slices'],...
    'HorizontalAlignment','left','Fontsize',9);
sliderextent=uicontrol('Parent',modpanel,'Style','slider','Callback',@sliderextent_Callback,...
    'String','Slice extent','Units','normalized','Position',[0.1 0.29 0.8 0.08],...
    'SliderStep',[1/sizeIm(3) 20/sizeIm(3)]);
set(sliderextent,'Min',1,'Max',sizeIm(3),'Value',1);

% "Extent direction" button
buttonExtentDir=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@ExtentDir_Callback,...
    'String','Forward extent','Units','normalized','Fontsize',11,...
    'Position',[0.2 0.17 0.6 0.07],'Tooltipstring',...
    'Direction of slice extent: forward or symmetric around current slice');

% % "This slice only" checkbox
% checkSliceOnly=uicontrol('Parent',modpanel,'Style','checkbox',...
%     'String','Modify only the current slice [obsolete]','Units','normalized','Fontsize',12,...
%     'Position',[0.1 0.11 0.8 0.08],'Value',0,'Min',0,'Max',1);

% "Delete from secondary mask" checkbox
checkDeleteSecondary=uicontrol('Parent',modpanel,'Style','checkbox',...
    'String','Delete from secondary mask','Units','normalized','Fontsize',12,...
    'Position',[0.1 0.07 0.8 0.08],'Value',0,'Min',0,'Max',1);

% "Deselect all" button
buttonDeselectAll=uicontrol('Parent',modpanel,'Style','togglebutton','Callback',@DeselectAll_Callback,...
    'String','Deselect all features','Units','normalized','Fontsize',12,...
    'Position',[0.05 0.01 0.41 0.05]);
% txtDeselectAll=uicontrol('Parent',modpanel,'Style','text','Units','normalized',...
%     'Position',[0.1 0.31 0.8 0.08],'Fontsize',10,...
%     'String','','HorizontalAlignment','center');


%% Panel for loading and saving masks
% Create panel for mask management
savepanel = uipanel('Title','Mask management','FontSize',12,...
    'Units','Pixels','Position',[sliderleft 120 sliderlength 150],...
    'Visible','off');

% "Load new secondary mask" button
buttonLoadMask2=uicontrol('Parent',savepanel,'Style','pushbutton','Callback',@buttonLoadMask2_Callback,...
    'String','Load new secondary mask','Units','normalized','Fontsize',12,...
    'Position',[0.1 0.7 0.8 0.2],'BusyAction','cancel');
txtLoadMask2=uicontrol('Parent',savepanel,'Style','text','Units','normalized',...
    'Position',[0.1 0.55 0.8 0.15],'Fontsize',10,...
    'String','Load mask (red) to add parts to primary mask','HorizontalAlignment','center');

% "Include blobs" button
buttonSaveMask=uicontrol('Parent',savepanel,'Style','pushbutton','Callback',@buttonSaveMask_Callback,...
    'String','Save mask as...','Units','normalized','Fontsize',12,...
    'Position',[0.1 0.3 0.8 0.2]);
txtSaveMask=uicontrol('Parent',savepanel,'Style','text','Units','normalized',...
    'Position',[0.1 0.15 0.8 0.15],'Fontsize',10,...
    'String','Saves selected (green) mask to file','HorizontalAlignment','center');


%% Other GUI parts
% Slice selector
htxt1=uicontrol('Style','text','Units','pixels',...
    'Position',[20+(sizeIm(2)-sliderlength)/2 40 textlength 20],...
    'String',['Slice: ',num2str(slice)],'HorizontalAlignment','left');
slider1=uicontrol('Parent',hfig,'Style','slider','Callback',@slider1_Callback,...
    'String','Slice Number','Units','pixels',...
    'Position',[20+(sizeIm(2)-sliderlength)/2 20 sliderlength 20],'SliderStep',[smallStep largeStep]);
set(slider1,'Min',minSlice,'Max',maxSlice,'Value',slice)

% Finish button
button=uicontrol('Parent',hfig,'Style','pushbutton','Callback',@Finish_Callback,...
    'String','Finish','Units','pixels','Fontsize',15,...
    'Position',[sliderleft+(sliderlength-spacer)/2 40 spacer 40]);

% Updating preview indicator
updtxt = uicontrol('Style','text','Position',[20+(sizeIm(2)-sliderlength)/2 height-60 sliderlength 30],...
    'String','Updating preview...','BackgroundColor',[0.8 0.8 0.8],...
    'HorizontalAlignment','center','FontWeight','bold','visible','off','Fontsize',16);
uistack(updtxt,'top')


updateImage;

set(hfig,'ResizeFcn',@windowResizeFcn);


%% Window resize function

    function windowResizeFcn(~,~,~)
        
        temp2 = get(hfig,'InnerPosition');
        width = temp2(3); height = temp2(4);
        
        sliderleft = width - sliderlength - 20;
        figsizex = sliderleft - 40;
        figsizey = height - 80 - 60;
        ratio = min(figsizex/sizeIm(2),figsizey/sizeIm(1));
        ratio = max(ratio,0.5);
        figsizex = sizeIm(2)*ratio;
        figsizey = sizeIm(1)*ratio;
%         if and(sizeIm(2)*1.8<figsizex,sizeIm(1)*1.8<figsizey)
%             figsizex = 2*sizeIm(2);
%             figsizey = 2*sizeIm(1);
%         elseif or(sizeIm(2)<figsizex,sizeIm(1)<figsizey)
%             figsizex = sizeIm(2);
%             figsizey = sizeIm(1);
%         end
%         figsizex = max(figsizex,sizeIm(2));
%         figsizey = max(figsizey,sizeIm(1));
%         sliderleft = width - sliderlength - 20;
        
        sliderleft = max(figsizex+40,width - sliderlength - 20);
        
        
        % Update main viewport locations
        % axes
haxes.Position = [20 80 figsizex figsizey];
% haxes.XLim = [1 sizeIm(2)];
% haxes.YLim = [1 sizeIm(1)];

topbuttony = 80+figsizey+15;
topbuttonh = 30;
topbuttonleft = 20+figsizex-310;
% zoomval = 100;

% Zoom toggle button
buttonZoom.Position = [topbuttonleft topbuttony 120 topbuttonh];
% Reset zoom push button
buttonResetZoom.Position = [topbuttonleft+130 topbuttony 90 topbuttonh];
% Pan toggle button
buttonPan.Position = [topbuttonleft+230 topbuttony 80 topbuttonh];
        
        % Update primary button locations
        
        % "Load primary mask" button
        set(buttonLoadMask1,'Position',[sliderleft height-50 sliderlength*0.47 30]);
        % "Load BG image" button
        set(buttonLoadBG,'Position',[sliderleft+sliderlength*0.53 height-50 sliderlength*0.47 30]);
        % "Add mask" button
        set(buttonAddMask,'Position',[sliderleft+(sliderlength/2-spacer)/2 height-90 spacer 30]);
        % "Subtract mask" button
        set(buttonSubMask,'Position',[sliderleft+(3*sliderlength/2-spacer)/2 height-90 spacer 30]);
        
        totheight = 140+60-30+100;
        numsliders = 7;
        lineheight = max(totheight / (3*numsliders),0);
        topheight = height-100;
        
        % Background opacity text and slider
        slidernum = 0;
        set(bgopacitytxt,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(sliderbgopacity,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % Overlay opacity text and slider
        slidernum = slidernum+1;
        set(overlayopacitytxt,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(slideroverlayopacity,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % Median filter text and slider
        slidernum = slidernum + 1;
        set(medfilttxt,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(slidermedfilt,'Position',[sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % Imclose filter text and slider
        slidernum = slidernum + 1;
        set(txtimclose,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(sliderimclose,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % Imopen filter text and slider
        slidernum = slidernum + 1;
        set(txtimopen,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(sliderimopen,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % Imdilate filter text and slider
        slidernum = slidernum + 1;
        set(txtimdilate,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(sliderimdilate,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % bwareaopen filter text and slider
        slidernum = slidernum + 1;
        set(txtbwareaopen,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+1) textlength lineheight]);
        set(sliderbwareaopen,'Position',...
            [sliderleft topheight-lineheight*(3*slidernum+2) sliderlength lineheight]);
        % "Apply median filter" button
        set(buttonApplyMedian,'Position',[sliderleft+(sliderlength/2-spacer)/2 height-400 spacer 30]);
        set(medfiltapplytxt,'Position',[sliderleft+(sliderlength/2-spacer)/2+10 height-415 spacer 15]);
        % "Reset median filter" button
        set(buttonResetMedian,'Position',[sliderleft+(3*sliderlength/2-spacer)/2 height-400 spacer 30]);
        
        modpanel.Position = [sliderleft 140+150 sliderlength ...
            height-320-140-150-100];
        savepanel.Position = [sliderleft 120 sliderlength 150];
        button.Position = [sliderleft+(sliderlength-spacer)/2 40 spacer 40];
    end

%% Zoom button actions
function []=buttonZoom_Callback(~,~,~)
    getZoomVal;
    
    if buttonZoom.Value==1  % Zoom now enabled
        hfigZoom.Enable = 'on';
        buttonPan.Value = 0;    % Panning off
        buttonPan.String = 'Pan OFF';
    else    % Zoom disabled
        hfigZoom.Enable = 'off';
    end
    
end

    function getZoomVal(~)
        lims = haxes.XLim;
        xextent = diff(lims)+1;
        zoomval = 100*double(sizeIm(2))/xextent;
        
        if (zoomval > 98) && (zoomval < 102)
            haxes.XLim = [1 sizeIm(2)];
            haxes.YLim = [1 sizeIm(1)];
            zoomval = 100;
        end
        
        if buttonZoom.Value==1  % Zoom enabled
            buttonZoom.String = ['Zoom ON [' num2str(zoomval,'%3.4g'), '%]'];
        else    % Zoom disabled
            buttonZoom.String = ['Zoom OFF [' num2str(zoomval,'%3.4g'), '%]'];
        end
        if zoomval~=100
            buttonResetZoom.Enable = 'on';
            buttonPan.Enable = 'on';
        else
            buttonResetZoom.Enable = 'off';
            buttonPan.Enable = 'off';
            buttonPan.Value = 0;
        end
    end

%% Zoom reset actions

function []=buttonResetZoom_Callback(~,~,~)
    getZoomVal;
    
    haxes.XLim = [1 sizeIm(2)];
    haxes.YLim = [1 sizeIm(1)];
    buttonResetZoom.Enable = 'off';
    buttonPan.Enable = 'off';
    buttonPan.Value = 0;
    getZoomVal;
end


%% Pan toggle button actions
function []=buttonPan_Callback(~,~,~)
    if buttonPan.Value == 1
        buttonZoom.Value = 0;    % String is adjusted in getZoomVal
        hfigPan.Enable = 'on';
        buttonPan.String = 'Pan ON';
    else
        hfigPan.Enable = 'off';
        buttonPan.String = 'Pan OFF';
    end
    getZoomVal;
end


%% Background opacity slider actions
    function []=bgopacity_Callback(sliderbgopacity,~,~)
        bgopacity=round(get(sliderbgopacity,'Value'));
        set(sliderbgopacity,'Value',bgopacity)
        set(bgopacitytxt,'String',['Background opacity: ',num2str(bgopacity) '%']);
        
        updateImage;
    end

%% Overlay opacity slider actions
    function []=overlayopacity_Callback(~,~,~)
        overlayopacity=round(get(slideroverlayopacity,'Value'));
        set(slideroverlayopacity,'Value',overlayopacity)
        set(overlayopacitytxt,'String',['Overlay opacity: ',num2str(overlayopacity) '%']);
        
        updateImage;
    end


%% Median filter slider actions
    function []=medfilt_Callback(slidermedfilt,~,~)
        medfilt=round(get(slidermedfilt,'Value'));
        if mod(medfilt,2)==0  % If medfilt3D is even
            medfilt = medfilt+1;    % Make it odd!
        end
        set(slidermedfilt,'Value',medfilt)
        set(medfilttxt,'String',['3D median filter size: ',num2str(medfilt)]);
%         if medfilt == 1
%             set(buttonApplyMedian,'String','Skip median filter');
%         else
%             set(buttonApplyMedian,'String','Apply filter to stack');
%         end
        
        % Set slice to be at least medfilt3D/2 from end for effective 3D
        % filtering
        slice=get(slider1,'Value');
        slice = max(slice,1+(medfilt-1)/2);
        set(slider1,'Value',slice);
        set(htxt1,'String',['Slice: ',num2str(slice)]);
        
        updateImage;
    end

%% imclose filter slider actions
    function []=imclose_Callback(caller,~,~)
        filtsize=round(get(caller,'Value'));
        set(caller,'Value',filtsize)
        set(txtimclose,'String',['Imclose (dilation + erosion) size: ',num2str(filtsize)]);
        
        updateImage;
    end

%% imdilate filter slider actions
    function []=imdilate_Callback(caller,~,~)
        filtsize=round(get(caller,'Value'));
        set(caller,'Value',filtsize)
        set(txtimdilate,'String',['Dilation size: ',num2str(filtsize)]);
        
        updateImage;
    end

%% imopen filter slider actions
    function []=imopen_Callback(caller,~,~)
        filtsize=round(get(caller,'Value'));
        set(caller,'Value',filtsize)
        set(txtimopen,'String',['Imopen (erosion + dilation) size: ',num2str(filtsize)]);
        
        updateImage;
    end


%% bwareaopen filter slider actions
    function []=bwareaopen_Callback(caller,~,~)
        filtsize=round(get(caller,'Value'));
        set(caller,'Value',filtsize)
        set(txtbwareaopen,'String',['Minimum feature size: ',num2str(filtsize^2)]);
        
        updateImage;
    end


%% Apply median filter actions
    function []=medfiltApply_Callback(~,~,~)
        % Turn on/off controls
        set(medfilttxt,'Enable','off');
        set(slidermedfilt,'Enable','off');
        set(txtimclose,'Enable','off');
        set(sliderimclose,'Enable','off');
        set(txtimopen,'Enable','off');
        set(sliderimopen,'Enable','off');
        set(txtimdilate,'Enable','off');
        set(sliderimdilate,'Enable','off');
        set(txtbwareaopen,'Enable','off');
        set(sliderbwareaopen,'Enable','off');
        set(buttonApplyMedian,'Enable','off');
        set(medfiltapplytxt,'Visible','on');
        pause(0.002);
        
%         goherenow
        
        % Apply median filter (create immaskMOD)
        medfilt = get(slidermedfilt,'Value');
        if medfilt>1        % Only if >1, perform filter
            immaskMEDFILT = medfilt3(immaskIN>0,[medfilt medfilt medfilt],'symmetric');
        else
            immaskMEDFILT = immaskIN>0;
        end
        disp('  Median filter applied.')
        
        % Apply imclose operation
        filtimclose = get(sliderimclose,'Value');
        if filtimclose > 0
            se = strel('disk',filtimclose);
            immaskMEDFILT = imclose(immaskMEDFILT,se);
        end
        
        % Apply imopen operation
        filtimopen = get(sliderimopen,'Value');
        if filtimopen > 0
            se = strel('disk',filtimopen);
            immaskMEDFILT = imopen(immaskMEDFILT,se);
        end
        
        % Apply imdilate operation
        filtimdilate = get(sliderimdilate,'Value');
        if filtimdilate > 0
            se = strel('sphere',filtimdilate);
            immaskMEDFILT = imdilate(immaskMEDFILT,se);
        end
        
        % bwareaopen filter
        filtbwarea = get(sliderbwareaopen,'Value');
        if filtbwarea > 0
            slcs = 4:4:sizeIm(3);       % Set positions to choose
            sliceextent = 6;
            for slc = slcs
%                 disp(['slice ' num2str(slc) '/' num2str(max(slcs))]);
                slstrt = max(slc-(sliceextent-1),1);
                slend = min(slc+(sliceextent-1),sizeIm(3));
                immaskMEDFILT(:,:,slstrt:slend) = bwareaopen(immaskMEDFILT(:,:,slstrt:slend),filtbwarea^2,18);
            end
        end
        
        immaskMOD = immaskMEDFILT;
        
        set(medfiltapplytxt,'Visible','off');
        set(buttonResetMedian,'Enable','on');
        set(modpanel,'Visible','on');
        set(savepanel,'Visible','on');
        pause(0.002);
        
        % Update image
        updateImage;
    end


%% Reset median filter actions
    function []=medfiltReset_Callback(~,~,~)
        % Turn on/off controls
        set(modpanel,'Visible','off');
        set(savepanel,'Visible','off');
        set(medfilttxt,'Enable','on');
        set(slidermedfilt,'Enable','on');
        set(txtimclose,'Enable','on');
        set(sliderimclose,'Enable','on');
        set(txtimopen,'Enable','on');
        set(sliderimopen,'Enable','on');
        set(txtimdilate,'Enable','on');
        set(sliderimdilate,'Enable','on');
        set(txtbwareaopen,'Enable','on');
        set(sliderbwareaopen,'Enable','on');
        set(buttonApplyMedian,'Enable','on');
        set(buttonResetMedian,'Enable','off');
        pause(0.002);
        
        % Remove all edits
        immaskMOD = immaskIN;
        
        % Update image
        updateImage;
    end


%% Apply slice selector change
    function []=slider1_Callback(slider1,~,~)
        slice=round(get(slider1,'Value'));
        set(slider1,'Value',slice);
        set(htxt1,'String',['Slice: ',num2str(slice)]);
        
        buttonsOff;
        
        modactive = 0;
        updateImage;
        
%         switch modactive
%             case 1
%                 RemoveBlobs_Callback('slicemove');
%             case 2
%                 IncludeBlobs_Callback('slicemove');
%             case 3
%                 RemovePix_Callback('slicemove');
%         end
        
    end


%% Update image
% Note there are two different functions! updateImageMedFilt applied only
% when still selecting filters, the other applies when the median
% filter has been applied by the user

    function [] = updateImageMedFilt(~,~,~)
        % Let depend on existence of immaskMOD!
        set(updtxt,'Visible','on');
        pause(0.005);
        
        % Initialize parameters and image
        tempXLim = haxes.XLim;
        tempYLim = haxes.YLim;
        medfilt=round(get(slidermedfilt,'Value'));
        filtimclose = get(sliderimclose,'Value');
        filtimopen = get(sliderimopen,'Value');
        filtimdilate = get(sliderimdilate,'Value');
        filtbwarea = get(sliderbwareaopen,'Value');
        sliceextent = max([medfilt filtimclose filtimopen]);
        if filtbwarea>1
            sliceextent = 6;   % Set feature sizes based on 11 slices (yes, 6 = 13)
        elseif sliceextent > 1      % Set condition if any filter is active
            sliceextent = 2*sliceextent;
        end
        slstrt = max(slice-(sliceextent-1),1);
        slend = min(slice+(sliceextent-1),sizeIm(3));
        % Create submask to perform preview operations
        msk=immaskIN(:,:,(slstrt):(slend))>0;    % Makes logical mask
        
        % 3D median filter
        if medfilt > 1
        msk = medfilt3(msk,[medfilt medfilt medfilt],'symmetric');
        end
        
        % imclose operation
        if filtimclose > 0
        se = strel('disk',filtimclose);
        msk = imclose(msk,se);
        end
        
        % imopen operation
        if filtimopen > 0
        se = strel('disk',filtimopen);
        msk = imopen(msk,se);
        end
        
        % imdilate operation
        if filtimdilate > 0
        se = strel('sphere',filtimdilate);
        msk = imdilate(msk,se);
        end
        
        % Update maximum feature size
        maxsize=max(cell2mat(struct2cell(regionprops(msk,'Area'))));
        if ~(size(maxsize)>0)
            maxsize=100;
        end
        set(sliderbwareaopen,'max',max(sqrt(maxsize),sliderbwareaopen.Max));
        
        % bwareaopen filter
        if filtbwarea > 1
        msk = bwareaopen(msk,filtbwarea^2,18);
        end
        
        % Final mask
        msk = msk(:,:,slice-slstrt+1);     % Show only relevant slice
        
        % Apply curves to base image
        Imtemp = IMAGEcrop(:,:,slice);
        sizeImtemp = size(Imtemp);
        curvimg = double(Imtemp);            % Allows double and not just integer values for better bit depth
        [uniqvals,~,ic] = unique(curvimg);
        newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
        for i=1:numel(uniqvals)
            newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
        end
        imORIG1D = double(reshape(newuniqvals(ic),sizeImtemp));
        opacity = get(sliderbgopacity,'Value');
        overlayopacity = get(slideroverlayopacity,'Value');
        imORIG1D = (opacity/100)*(imORIG1D / max(max(imORIG1D))).^(.8);
        
        im = zeros(sizeImtemp(1),sizeImtemp(2),3);
        im(:,:,1) = imORIG1D - 0.3*double(msk).*(imORIG1D)*overlayopacity/100;
        im(:,:,2) = imORIG1D + double(msk).*(imORIG1D+0.5)*overlayopacity/100;
        im(:,:,3) = imORIG1D - 0.3*double(msk).*(imORIG1D)*overlayopacity/100;
        
        imshow(im);
        haxes.XLim = tempXLim;
        haxes.YLim = tempYLim;
        
        set(updtxt,'Visible','off');
    end

    function [] = updateImage(~,~,~)
        % If window has been closed, ignore input
        if ~exist('hfig','var')
            return;
        end
        
        % If median filter has not been applied, perform other updateImage
        % routine
        if strcmp(get(modpanel,'Visible'),'off') == 1
            updateImageMedFilt;
            return;
        end
        
        set(updtxt,'Visible','on');
        pause(0.005);
        
        tempXLim = haxes.XLim;
        tempYLim = haxes.YLim;
        % Initialize parameters and image
        slice = round(get(slider1,'Value'));
        msk = immaskMOD(:,:,slice);
        
        % Apply curves to base image
        Imtemp = IMAGEcrop(:,:,slice);
        sizeImtemp = size(Imtemp);
        curvimg = double(Imtemp);            % Allows double and not just integer values for better bit depth
        [uniqvals,~,ic] = unique(curvimg);
        newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
        for i=1:numel(uniqvals)
            newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
        end
        imORIG1D = double(reshape(newuniqvals(ic),sizeImtemp));
        opacity = get(sliderbgopacity,'Value');
        overlayopacity = get(slideroverlayopacity,'Value');
        imORIG1D = (opacity/100)*(imORIG1D / max(max(imORIG1D))).^(.8);
        
        % Calculate deleted areas
        immaskMEDFILT = immaskMEDFILT>0;    % Gohere, added without bug check
        immaskdelete = immaskMEDFILT(:,:,slice) - msk;
        
        im = zeros(sizeImtemp(1),sizeImtemp(2),3);
        im(:,:,1) = imORIG1D - 0.3*double(msk).*(imORIG1D)*overlayopacity/100 + double(immaskdelete).*(imORIG1D+0.5)*overlayopacity/100;
        im(:,:,2) = imORIG1D + double(msk).*(imORIG1D+0.5)*overlayopacity/100;
        im(:,:,3) = imORIG1D - 0.3*double(msk).*(imORIG1D)*overlayopacity/100;
        
        imshow(im);
        haxes.XLim = tempXLim;
        haxes.YLim = tempYLim;
        
        % Make sure all the buttons are correct
        set(buttonRemovePix,'Fontweight','normal');
        set(buttonRemovePix,'foregroundcolor','black');
        set(buttonAddManPix,'Fontweight','normal');
        set(buttonAddManPix,'foregroundcolor','black');
        set(buttonIncludePix,'Fontweight','normal');
        set(buttonIncludePix,'foregroundcolor','black');
        set(buttonRemoveBlobs,'Fontweight','normal');
        set(buttonRemoveBlobs,'foregroundcolor','black');
        set(buttonIncludeBlobs,'Fontweight','normal');
        set(buttonIncludeBlobs,'foregroundcolor','black');
        switch modactive
            case 0  % Do nothing; all off
            case 1  % Remove Blobs
                set(buttonRemoveBlobs,'Fontweight','bold');
                set(buttonRemoveBlobs,'foregroundcolor','blue');
            case 2  % Include Blobs
                set(buttonIncludeBlobs,'Fontweight','bold');
                set(buttonIncludeBlobs,'foregroundcolor','blue');
            case 3  % Remove Pix
                set(buttonRemovePix,'Fontweight','bold');
                set(buttonRemovePix,'foregroundcolor','blue');
            case 4  % Include Pix
                set(buttonIncludePix,'Fontweight','bold');
                set(buttonIncludePix,'foregroundcolor','blue');
            case 5  % Include Pix
                set(buttonAddManPix,'Fontweight','bold');
                set(buttonAddManPix,'foregroundcolor','blue');
        end
        pause(0.002);
        
        
        set(updtxt,'Visible','off');
    end


%% Activate remove blobs function
    function []=RemoveBlobs_Callback(caller,~,~)
        buttonsOff;
        if modactive == 1 && ~strcmp(caller,'slicemove')
            modactive = 0;  % Modification mode off
        else
            modactive = 1;  % Modification mode #1 (remove blobs)
            set(buttonRemoveBlobs,'Fontweight','bold');
            set(buttonRemoveBlobs,'foregroundcolor','blue');
            pause(0.002)
        end
        
        while modactive == 1
            checkslice = get(sliderextent,'Value');
            slicedir = getExtentDir;
            checksecondary = get(checkDeleteSecondary,'Value');
            if checkslice == 1
                blobslice = slice;
            else
                blobslice = 1:sizeIm(3);
            end
            if checksecondary
                newmask = editblobs3D(immaskMEDFILT(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'remove',slice,checkslice,slicedir);
            else
            newmask = editblobs3D(immaskMOD(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'remove',slice,checkslice,slicedir);
            end
%         immaskMOD(:,:,slice) = removeblobsdraw3D(immaskMOD(:,:,slice));
        if and(modactive == 1,checkslice==get(sliderextent,'Value'))       
            % This weird trick ignores the freehand after modactive is
            % turned off or check slice only changed
            if checksecondary
                immaskMEDFILT(:,:,blobslice) = newmask;
            else
                immaskMOD(:,:,blobslice) = newmask;
            end
        end
        
        updateImage
        pause(0.002);
        end
        set(buttonRemoveBlobs,'Fontweight','normal');
        set(buttonRemoveBlobs,'foregroundcolor','black');
    end

%% Activate include blobs function
    function [] = IncludeBlobs_Callback(caller,~,~)
        buttonsOff;
        
        if modactive == 2 && ~strcmp(caller,'slicemove')
            modactive = 0;  % Modification mode off
        else
            modactive = 2;  % Modification mode #2 (include blobs)
            set(buttonIncludeBlobs,'Fontweight','bold');
            set(buttonIncludeBlobs,'foregroundcolor','blue');
            pause(0.002)
        end
        
        while modactive == 2
            sliceextent = get(sliderextent,'Value');
            slicedir = getExtentDir;
            if sliceextent == 1
                blobslice = slice;
            else
                blobslice = 1:sizeIm(3);
            end
        newmask = editblobs3D(immaskMOD(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'add',slice,sliceextent,slicedir);
        if and(modactive == 2,sliceextent==get(sliderextent,'Value'))
            % This weird trick ignores the freehand after modactive is
            % turned off or check slice only status changed
            immaskMOD(:,:,blobslice) = newmask;
        end
        
        updateImage
        pause(0.002);
        end
        set(buttonIncludeBlobs,'Fontweight','normal');
        set(buttonIncludeBlobs,'foregroundcolor','black');
    end


%% Activate pixel removal
    function [] = RemovePix_Callback(caller,~,~)
        buttonsOff;
        if modactive == 3 && ~strcmp(caller,'slicemove')
            modactive = 0;  % Modification mode off
        else
            modactive = 3;  % Modification mode #3 (remove pixels)
            set(buttonRemovePix,'Fontweight','bold');
            set(buttonRemovePix,'foregroundcolor','blue');
            pause(0.002)
        end
        
        while modactive == 3
            sliceextent = get(sliderextent,'Value');
            slicedir = getExtentDir;
            checksecondary = get(checkDeleteSecondary,'Value');
            if sliceextent == 1
                blobslice = slice;
            else
                blobslice = 1:sizeIm(3);
            end
            
            if checksecondary
                newmask = editpix3D(immaskMEDFILT(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'remove',slice,sliceextent,slicedir);
            else
            newmask = editpix3D(immaskMOD(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'remove',slice,sliceextent,slicedir);
            end
%         immaskMOD(:,:,slice) = removeblobsdraw3D(immaskMOD(:,:,slice));
        if and(modactive == 3,sliceextent==get(sliderextent,'Value'))       
            % This weird trick ignores the freehand after modactive is
            % turned off or check slice only changed
            if checksecondary
                immaskMEDFILT(:,:,blobslice) = newmask;
            else
                immaskMOD(:,:,blobslice) = newmask;
            end
        end
        
        updateImage
        pause(0.002);
        end
        set(buttonRemovePix,'Fontweight','normal');
        set(buttonRemovePix,'foregroundcolor','black');
    end

%% Activate pixel inclusion
    function [] = IncludePix_Callback(caller,~,~)
        buttonsOff;
        if modactive == 4 && ~strcmp(caller,'slicemove')
            modactive = 0;  % Modification mode off
        else
            modactive = 4;  % Modification mode #4 (include pixels)
            set(buttonIncludePix,'Fontweight','bold');
            set(buttonIncludePix,'foregroundcolor','blue');
            pause(0.002)
        end
        
        while modactive == 4
            sliceextent = get(sliderextent,'Value');
            slicedir = getExtentDir;
            if sliceextent == 1
                blobslice = slice;
            else
                blobslice = 1:sizeIm(3);
            end
%             if checksecondary
%                 newmask = editpix3D(immaskMEDFILT(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'remove',slice,sliceextent,slicedir);
%             else
            newmask = editpix3D(immaskMOD(:,:,blobslice),immaskMEDFILT(:,:,blobslice),'include',slice,sliceextent,slicedir);
%             end
%         immaskMOD(:,:,slice) = removeblobsdraw3D(immaskMOD(:,:,slice));
        if and(modactive == 4,sliceextent==get(sliderextent,'Value'))       
            % This weird trick ignores the freehand after modactive is
            % turned off or check slice only changed
%             if checksecondary
%                 immaskMEDFILT(:,:,blobslice) = newmask;
%             else
                immaskMOD(:,:,blobslice) = newmask;
%             end
        end
        
        updateImage
        pause(0.002);
        end
        set(buttonIncludePix,'Fontweight','normal');
        set(buttonIncludePix,'foregroundcolor','black');
    end

%% Add pixels manually
    function [] = AddManPix_Callback(caller,~,~)
        buttonsOff;
        if modactive == 5 && ~strcmp(caller,'slicemove')
            modactive = 0;  % Modification mode off
        else
            modactive = 5;  % Modification mode #4 (include pixels)
            set(buttonAddManPix,'Fontweight','bold');
            set(buttonAddManPix,'foregroundcolor','blue');
            pause(0.002)
        end
        
        while modactive == 5
            sliceextent = get(sliderextent,'Value');
            slicedir = getExtentDir;
            if sliceextent == 1
                blobslice = slice;
            else
                blobslice = 1:sizeIm(3);
            end
            brushsize = get(sliderbrushsize,'Value');
            newmask = editpix3D(immaskMOD(:,:,blobslice),...
                immaskMEDFILT(:,:,blobslice),['addmanual',num2str(brushsize)],...
                slice,sliceextent,slicedir);
        if and(modactive == 5,sliceextent==get(sliderextent,'Value'))       
            % This weird trick ignores the freehand after modactive is
            % turned off or check slice only changed
                immaskMOD(:,:,blobslice) = newmask;
        end
        
        updateImage;
        pause(0.002);
        end
        set(buttonAddManPix,'Fontweight','normal');
        set(buttonAddManPix,'foregroundcolor','black');
    end

%% Fill holes button action
    function [] = FillHoles_Callback(~,~,~)
        
            sliceextent = get(sliderextent,'Value');
            slicedir = getExtentDir;
            if sliceextent == 1
                blobslice = slice;
                immaskMOD(:,:,blobslice) = imfill(immaskMOD(:,:,blobslice),4,'holes');
            else
                if slicedir==0  % If symmetric extent
                    slstrt = max(ceil(slice-sliceextent/2),1);
                    slend = min(floor(slice+sliceextent/2),sizeIm(3));
                elseif slicedir==1    % If in forward direction
                    slstrt = slice;
                    slend = min((slice+sliceextent-1),sizeIm(3));
                elseif slicedir==-1    % If in backward direction
                    slstrt = max(ceil(slice-sliceextent+1),1);
                    slend = slice;
                end
                
                blobslice = slstrt:slend;
                for i = 1:length(blobslice)
                    immaskMOD(:,:,blobslice(i)) = imfill(immaskMOD(:,:,blobslice(i)),4,'holes');
                end
            end
        updateImage;
        pause(0.002);
    end


%% Deselect/select all button action
    function [] = DeselectAll_Callback(~,~,~)
        strn = get(buttonDeselectAll,'String');
        switch strn
            case 'Deselect all features'
                immaskMOD = immaskMEDFILT*0>0;
                size(immaskMOD)
                set(buttonDeselectAll,'String','Select all features');
            case 'Select all features'
                immaskMOD = immaskMEDFILT>0;
                set(buttonDeselectAll,'String','Deselect all features');
        end
        updateImage;
    end


%% Slice extent slider action
    function [] = sliderextent_Callback(~,~,~)
        extent=round(get(sliderextent,'Value'));
        set(sliderextent,'Value',extent);
        stext = 's';
        set(txtextent,'String',['z extent of actions: ',num2str(extent),' slice',stext(extent>1)]);
    end


%% Brush size slider action
    function [] = brushsize_Callback(~,~,~)
        sz=round(get(sliderbrushsize,'Value'));
        set(sliderbrushsize,'Value',sz);
        set(txtbrushsize,'String',['Brush size: ',num2str(sz)]);
    end


%% Extent direction button action
    function [] = ExtentDir_Callback(~,~,~)
        strn = get(buttonExtentDir,'String');
        switch strn(1:4)
            case 'Symm'
                set(buttonExtentDir,'String','Forward extent');
            case 'Forw'
                set(buttonExtentDir,'String','Backward extent');
            case 'Back'
                set(buttonExtentDir,'String','Symmetric extent');
        end
%         updateImage;
    end

%% Retrieve extent direction status
% 0 = symmetric, +1 = forward, -1 = backward
    function exdir=getExtentDir()
        strn = get(buttonExtentDir,'String');
        switch strn(1:4)
            case 'Symm'
                exdir = 0;
            case 'Forw'
                exdir = 1;
            case 'Back'
                exdir = -1;
        end
    end

%% Load primary mask button
    function [] = buttonLoadMask1_Callback(~,~,~)
        [filename, pathname] = ...
            uigetfile('*.mat','Select mask file');
        
        % Load that file
        load([pathname filesep filename],'MASKfilter');
        immaskIN = MASKfilter;
        
        % Initialize settings
        immaskMOD = immaskIN;       % Name of the latest modified mask
        immaskMEDFILT = immaskIN;   % Name of the original mask, but with median filter applied
        modactive = 0;              % Freeform modifications are inactive
        
        medfiltReset_Callback;
        
    end

%% Load background image button
    function [] = buttonLoadBG_Callback(~,~,~)
        [filename, pathname] = ...
            uigetfile('*.mat','Select image file');
        
        % Save update text
        temptxt = get(updtxt,'String');
        
        % Load that file
        set(updtxt,'Visible','on');
        set(updtxt,'String','Loading background image...');
        pause(0.002);
        newIM = load([pathname filesep filename]);
        b = fieldnames(newIM);
        c = strfind(b{:},'IMAGE')==1;
        if strfind(b{:},'IMAGE')>0
            eval(['newIM = newIM.',b{c},';']);
            
            % Verify correct image size
            if sum(abs(size(newIM)-size(IMAGEcrop)))>0
                % Wrong image size
                set(updtxt,'String','Image dimensions do not match!');
                pause(1);
            else
                IMAGEcrop = uint8(newIM);
                clear newIM
                set(updtxt,'String','Background loaded');
                pause(1);
            end
        else
            set(updtxt,'String','No suitable image variable found!');
            pause(1);
        end
        
        set(updtxt,'Visible','off');
        set(updtxt,'String',temptxt);
        updateImage;
        
    end

%% Add/subtract mask buttons
    function [] = addMask_Callback(caller,~,~)
        [filename, pathname] = ...
            uigetfile('*.mat','Select mask file');
        
        % Load that file
        load([pathname filesep filename],'MASKfilter');
        tempmask = MASKfilter>0;
        
        if sum(abs(size(tempmask)-size(immaskIN)))~=0
            error('Please select a mask with the same dimensions');
        end
        modactive = 0;              % Freeform modifications are inactive
        immaskIN = immaskIN>0;
        immaskMEDFILT = immaskMEDFILT>0;
        immaskMOD = immaskMOD>0;
        % Do the math
        if strcmp(get(caller,'String'),'Add mask...')
            immaskIN = (immaskIN + tempmask)>0;
            immaskMEDFILT = (immaskMEDFILT + tempmask)>0;
            immaskMOD = (immaskMOD + tempmask)>0;
        else
            immaskIN = (immaskIN - tempmask)>0;
            immaskMEDFILT = (immaskMEDFILT - tempmask)>0;
            immaskMOD = (immaskMOD - tempmask)>0;
        end
        updateImage;
        
    end

%% Load secondary mask button
    function [] = buttonLoadMask2_Callback(~,~,~)
        [filename, pathname] = ...
            uigetfile('*.mat','Select mask file');
        
        % Load that file
        load([pathname filesep filename],'MASKfilter');
        immaskMEDFILT = MASKfilter;
        
        % Initialize settings
        modactive = 0;              % Freeform modifications are inactive
        
        updateImage;
        
    end


%% Save mask button
    function [] = buttonSaveMask_Callback(~,~,~)
%         [filename, pathname] = ...
%             uiputfile('*.mat','Select mask file');
%         
        MASKfilter = immaskMOD>0; %#ok<NASGU>
        
        uisave('MASKfilter','mask.mat');
        title('Mask saved.');
        % Save that file
%         [pathname filename]
%         save([pathname filename],'MASKfilter')
        pause(0.005);
    end


%%  Finish button
    function []=Finish_Callback(~,~,~)
        try
            buttonSaveMask_Callback;
        catch
        end
        outputmask = immaskMOD;
        close; return
    end

%% Turn off all selection buttons
    function []=buttonsOff()
        set(buttonRemovePix,'Fontweight','normal');
        set(buttonRemovePix,'foregroundcolor','black');
        set(buttonAddManPix,'Fontweight','normal');
        set(buttonAddManPix,'foregroundcolor','black');
        set(buttonIncludePix,'Fontweight','normal');
        set(buttonIncludePix,'foregroundcolor','black');
        set(buttonRemoveBlobs,'Fontweight','normal');
        set(buttonRemoveBlobs,'foregroundcolor','black');
        set(buttonIncludeBlobs,'Fontweight','normal');
        set(buttonIncludeBlobs,'foregroundcolor','black');
        buttonZoom.Value = 0;
        buttonZoom.String = ['Zoom OFF [' num2str(zoomval,'%3.4g'), '%]'];
        buttonPan.Value = 0;
        buttonPan.String = 'Pan OFF';
    end
end