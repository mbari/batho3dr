function [imnew,mask]=apply_imops3D(im,output,feature)

% im is the image input
% output is the array with adjustment settings
% feature (optional!) is the number of the feature to analyze.

if nargin<3
    feature = numel(output.minthresh);    % If no feature specified, analyze the last one
end

minthresh=output.minthresh(feature);
maxthresh=output.maxthresh(feature);
medfilt=output.filt;
medfilt3D=output.filt3D;
curvpoints = output.curves;

% slice=(size(im,3)-1)/2+1;
% im=im(:,:,(slice-(medfilt3D-1)/2):(slice+(medfilt3D-1)/2));     % Should be unnecessary
sizeImtemp = size(im);

% Apply curves
if sum(curvpoints-[0 0; 127 127; 255 255])~=0
curvmap = create_curvmap(curvpoints);
curvimg = double(im);            % Allows double and not just integer values for better bit depth
[uniqvals,~,ic] = unique(curvimg);
newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
for i=1:numel(uniqvals)
    newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
end
im = reshape(newuniqvals(ic),sizeImtemp);
end

% 3D median filter
if or(medfilt>1,medfilt3D>1)
    im = medfilt3(im,[medfilt medfilt medfilt3D],'symmetric');
    im = im(:,:,medfilt3D);     % Show only middle part
end

% Mask out sections
mask = bwmorph(setthresh(im,minthresh,maxthresh),'spur');
imnew = im.*double(mask);
