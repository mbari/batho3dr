%% Information
% This program (called by various of the ThreeDR functions) updates the
% button status in the 3DR main window.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 11th, 2018

function updateMainButtons(ThreeDRCurrent)
global cfig

if isfield(cfig,'Children')
% Loop through cfig children
cfigch = cfig.Children;

for numch = 1:size(cfigch,1)    % Loop through cfig children
    stepno = str2num(cfigch(numch).Tag(5));  % Get step # if exists in Tag
    % If this cfig child is one of the step buttons
    if ~isempty(stepno)
        origstr = cfigch(numch).String;
        used = 0;   % Indicate whether this button was activated before
        
        % Strip existing html tags
        while strfind(origstr,'<')>0
            used = 1;
            tagstrt = strfind(origstr,'<');
            tagend = strfind(origstr,'>');
            origstr(tagstrt(1):tagend(1))=[];
        end
        
        % If currently selected, make bold and red
        if stepno == ThreeDRCurrent
            cfigch(numch).String = ['<html><FONT color="red"><b>',...
                origstr,'</b></html>'];
        % If previously selected, make grey
        elseif used == 1
            cfigch(numch).String = ['<html><FONT color="#999999">',...
                origstr,'</html>'];
        % Otherwise, return plain string
        else
            cfigch(numch).String = origstr;
        end
    end
end
end

pause(0.005);

end