%% Information
% This program (called by several other 3DR programs) determines the
% correct size for a figure window that shows the image / mask based on the
% image dimensions, and returns the position array [x, y, w, h] for both
% the window and the image. There is space on the right for buttons and
% controls, and space on the bottom for frame selection.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 7th, 2018

function [figh] = generalEditorWindow(IMAGE,sidebarsize)
% Output 'figh' contains all relevant handles and info

global cfig         % ThreeDR_Main figure handle
sizeIm = size(IMAGE);

% If undefined, set minimum side bar width in pixels
if nargin < 2
    sidebarsize = 300;
end

% Get dimensions
[windowPos, imagePos, dims] = generalEditorWindowSize(sizeIm,sidebarsize);


%% Build main window and controls

% Build main window
figh.fig = figure('Menubar','none','Toolbar','none','Name',...
    '3DR window','NumberTitle','on','IntegerHandle','off',...
    'Units','Pixels',...
    'OuterPosition',windowPos);
figInPos = figh.fig.InnerPosition;

% Build axes
figh.axes = axes(figh.fig,'Units','Pixels','Position',imagePos);

% panelborder = 'etchedin';
panelborder = 'none';

% Create top bar panel
figh.toppanel = uipanel('Title','','FontSize',12,'Units','Pixels',...
    'BorderType',panelborder,'Position',[imagePos(1), ...
    imagePos(2) + imagePos(4) + dims.axespad(3),...
    imagePos(3), dims.topbar]);

% Create bottom bar panel
figh.botpanel = uipanel('Title','','FontSize',12,'Units','Pixels',...
    'BorderType',panelborder,'Position',[imagePos(1), dims.vpadding,...
    imagePos(3), dims.bottombar]);

% Create sidebar panel
figh.sidepanel = uipanel('Title','','FontSize',12,'Units','Pixels',...
    'Position',[imagePos(1) + imagePos(3) + dims.axespad(2),...
    imagePos(2), figInPos(3) - imagePos(1) - imagePos(3) - ...
    dims.hpadding - dims.axespad(2), figInPos(4) - imagePos(2) - ...
    dims.vpadding],'BorderType',panelborder);


%% Add controls

% Finish button
figh.finishbutton=uicontrol('Parent',figh.fig,'Style','pushbutton',...
    'Callback',@Finish_Callback,...
    'String','Finish','Units','pixels','Fontsize',15,...
    'Position',[imagePos(1) + imagePos(3) + dims.axespad(2) + 20,...
    dims.vpadding, dims.sidebar - 40, ...
    imagePos(2) - dims.vpadding - 5],...
    'Tag','finishbutton');

% Slice selector
frame = round(size(IMAGE,3)/2);
figh.frametxt = uicontrol('Parent',figh.botpanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.4 0.6 0.2 0.40],...
    'String',['Frame: ',num2str(frame)],'HorizontalAlignment','center');
figh.frameslider=uicontrol('Parent',figh.botpanel,'Style','slider',...
    'Callback',@frame_Callback,...
    'String','Frame number','Units','normalized',...
    'Position',[0.2 0.05 0.6 0.5],...
    'Min',1,'Max',size(IMAGE,3),'Value',frame,...
    'SliderStep',[1/size(IMAGE,3) 10/size(IMAGE,3)]);

% Updating preview indicator
figh.updtxt = uicontrol('Parent',figh.fig,'Style','text',...
    'Position',[imagePos(1) + 0.3*(imagePos(3) - imagePos(1)),...
    imagePos(2) + imagePos(4) - 30, ...
    0.4*(imagePos(3) - imagePos(1)), 30],...
    'String','Updating preview...','BackgroundColor',[0.8 0.8 0.8],...
    'HorizontalAlignment','center','FontWeight','bold',...
    'visible','on','Fontsize',16);
uistack(figh.updtxt,'top')


% Brightness text and slider
figh.brighttxt = uicontrol('Parent',figh.toppanel,'Style','text',...
    'Units','normalized',...
    'Position',[0.8 0.6 0.2 0.40],...
    'String',['Brightness: ',num2str(100),'%'],...
    'HorizontalAlignment','center');
figh.brightslider = uicontrol('Parent',figh.toppanel,'Style','slider',...
    'Units','normalized',...
    'Position',[0.8 0.05 0.2 0.5],...
    'Callback',@brightness_Callback,...
    'String','Brightness',...
    'SliderStep',[5/200 0.1],'Min',0,'Max',200,'Value',100);

updateImage(figh);

%% Callbacks

% Change frame
    function [] = frame_Callback(source,~,~)
        
        currframe = round(get(source,'Value'));
        set(source,'Value',currframe);
        set(figh.frametxt,'String',['Frame: ',num2str(currframe)]);
        
        updateImage(figh);
    end

% Apply brightness change
    function [] = brightness_Callback(source,~,~)
        brightness = round(get(source,'Value'));
        set(source,'Value',brightness);
        figh.brighttxt.String = ['Brightness: ',num2str(brightness) '%'];
        
        updateImage(figh);
    end

% Close window
    function [] = Finish_Callback(~,~,~)
        close(figh.fig); return
    end


%% Other functions

% Update image
function updateImage(figh)
        % TBD??
        figh.updtxt.Visible = 'on';
        pause(0.005);
        
        % Initialize parameters and image
        currframe = figh.frameslider.Value;
        im = IMAGE(:,:,currframe);
        axes(figh.axes);
        imshow(im);
        
        pause(0.002);
        
        set(figh.updtxt,'Visible','off');
end


function [windowPos, imagePos, dims] = generalEditorWindowSize(sizeIm,sidebarsize)
% This function determines the
% correct size for a figure window that shows the image / mask based on the
% image dimensions, and returns the position array [x, y, w, h] for both
% the window and the image. There is space on the right for buttons and
% controls, and space on the bottom for frame selection.
% global cfig         % ThreeDR_Main figure handle

% Define desired window parameters [pixels]
sidebar = sidebarsize;
bottombar = 40;
topbar = 40;
hpadding = 20;
vpadding = 40;
axespad = [20 10 10 20];   % Padding around axes [left right top bottom]
% Below minimum distances work for Joost on external screen. Should become
% general setting.
mindistbot = 80;
mindisttop = 50;
mindistright = 25;
mindistleft = 25;

% Assign settings to output
dims = struct('sidebar',sidebar,'bottombar',bottombar,'topbar',topbar,...
    'hpadding',hpadding,'vpadding',vpadding,'mindistbot',mindistbot,...
    'mindistright',mindistright,'mindistleft',mindistleft,...
    'axespad',axespad);

% Obtain screen size
wa = get(0,'screensize');
screenwidth = wa(3);
screenheight = wa(4);

% Get ThreeDR_Main figure location
if ~isempty(cfig)  % Only use cfig if the window exists
    tmp = cfig.Units;
    cfig.Units = 'pixels';
    cfigPos = cfig.OuterPosition;
    cfigright = cfigPos(1) + cfigPos(3) + 50;   % Left edge of window when
    % put 50 px to right of cfig
    menubar = cfigPos(4) - cfig.InnerPosition(4);
    cfig.Units = tmp;
else
    % If cfig (the Main window) does not exist, make up its hypothetical
    % position
    cfigPos = [200 200 300 600];
    cfigright = cfigPos(1) + cfigPos(3) + 50;   % Left edge of window when
    % put 50 px to right of cfig
    menubar = 35;
end

% Set initial OUTER window size
outerwidth = sizeIm(2) + sidebar + 2 * hpadding + axespad(1) + axespad(2);
outerheight = sizeIm(1) + topbar + bottombar + 2 * vpadding + ...
    menubar + axespad(3) + axespad(4);

%% Change window size dependent on screen size

scalefactor = 1;
% If window too big for screen, make smaller
if or(outerwidth + mindistright + mindistleft > screenwidth, ...
        outerheight + mindistbot + ...
        mindisttop > screenheight)
    widthscale = sizeIm(2) / ...
        (screenwidth - mindistleft - mindistright - 2 * hpadding - ...
        sidebar - axespad(1) - axespad(2));
    heightscale = sizeIm(1) / (screenheight - mindistbot - ...
        mindisttop - 2 * vpadding - topbar - bottombar - menubar - ...
        axespad(3) - axespad(4));
    scalefactor = ceil(max([widthscale, heightscale]));
    
    % Reset outer window size
    outerwidth = sizeIm(2)/scalefactor + sidebar + 2 * hpadding + ...
        axespad(1) + axespad(2);
    outerheight = sizeIm(1)/scalefactor + topbar + bottombar + ...
        2 * vpadding + menubar + axespad(3) + axespad(4);
end

% If window small enough, put next to ThreeDR_Main
if and(outerwidth + mindistright + cfigright < screenwidth, ...
        outerheight + 50 + cfigPos(2) < screenheight)
    xpos = cfigright;
    ypos = cfigPos(2);
% Try a little lower, too
elseif and(outerwidth + mindistright + cfigright < screenwidth, ...
        outerheight + mindistbot + mindisttop < screenheight)
    xpos = cfigright;
    ypos = mindistbot;
else
    xpos = mindistleft;
    ypos = mindistbot;
end
    
windowPos = [xpos, ypos, outerwidth, outerheight];
imagePos = [hpadding + axespad(1), vpadding+bottombar + axespad(4), ...
    sizeIm(2)/scalefactor, ...
    sizeIm(1)/scalefactor ];
end

end