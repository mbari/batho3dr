%% Information
% This program (called by ThreeDR_CropPreProc) loads a GUI to select
% desired trim, crop, and background subtraction settings.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 11th, 2018

function [cropFig,output] = setCrop(IMAGE,output,option)
% IMAGE is the image stack or a file name
% output is a struct with properties
% option selects which option should be available: 'trim','crop',bg','all'.
% It's also possible to combine comma-separated commands

if ischar(IMAGE)
    imgstack = 0;
    % Initialize videoreader object
    vid = VideoReader(IMAGE);
    % Estimate number of frames
    finish = ceil(vid.FrameRate*vid.Duration);
    currframe = floor(finish/2);
    vid.CurrentTime = currframe/vid.FrameRate;
    im1 = readFrame(vid);                % Read frame
    if isfield(output,'channelmix')
        channelmix = output.channelmix;
    else
        % Get channel mix
        [channelmix] = selectChannelMix(im1);
        output.channelmix = channelmix;
    end
    % Make gray
    if channelmix>3
        im1 = rgb2gray(im1);
    else
        im1 = im1(:,:,channelmix);
    end
    sizeIm = size(im1);
    sizeIm(3) = finish;
else
    imgstack = 1;
    sizeIm = size(IMAGE);
    if length(sizeIm)==2
        sizeIm(3) = 1;
    end
end
if ~isfield(output,'motioncorr')
    output.motioncorr = zeros(sizeIm(3),3);
elseif isempty(output.motioncorr)
    output.motioncorr = zeros(sizeIm(3),3);
end
    
sizeImcorr = sizeIm + round([max(output.motioncorr(:,3)),...
    max(output.motioncorr(:,2)), 0]);

% If no crop settings provided, initialize
if ~isfield(output,'TCrop')
    output.LCrop = 1;
    output.RCrop = sizeImcorr(2);
    output.TCrop = 1;
    output.BCrop = sizeImcorr(1);
    output.bgmedfilt = 1;
    output.bgminfilt = 1;
end
if ~isfield(output,'startFrame')
    output.startFrame = 1;
    output.endFrame = sizeIm(3);
end

if nargin < 3
    option = 'all';
end
if strcmp(option,'all')
    option = 'trim,crop,bg';
end

% Initialize other parameters
initFrame = round((output.endFrame + output.startFrame)/2);
tmp = (0:.05:1);
clrmp = [tmp' tmp' tmp'];


%% Initialize window

global fullIMAGE
fullIMAGE = zeros(sizeImcorr(1),sizeImcorr(2),sizeIm(3),'uint8');
calcCorrImage(initFrame);

cropFig = generalEditorWindow(fullIMAGE);
cropFig.fig.Name = "Trim and crop video";


%% Add controls

cropFig.sidepanel.Units = 'pixels';
sidepos = cropFig.sidepanel.Position;

% Reassign callbacks
cropFig.finishbutton.Callback = @finish_Callback;
cropFig.frameslider.Callback = @frame_Callback;
cropFig.brightslider.Callback = @brightness_Callback;

numsliders = 10;
sliderpos = zeros(numsliders,4);     % Array of sidebar slider locations
slidertxtpos = zeros(numsliders,4);     % Array of sidebar slider text locations
% If the number of vertical pixels per slider is greater than 30 (slider +
% text) + 10 (padding), stick to pixel dimensions, otherwise use normalized
% units
leftover = sidepos(4) - 50 * numsliders + 60;
if leftover > 0
    sliderunits = 'pixels';
    
    % Set slider and text location
    sliderpos(:,1) = 0.1 * sidepos(3);
    slidertxtpos(:,1) = 0.15 * sidepos(3);
    sliderpos(:,2) = sidepos(4) - 40 - (1:numsliders)' * 50;
    slidertxtpos(:,2) = sidepos(4) - 40 - (1:numsliders)' * 50 + 20;
    
    % Set slider and text width & height
    sliderpos(:,3) = sidepos(3)*0.8;
    slidertxtpos(:,3) = sidepos(3)*0.8;
    sliderpos(:,4) = 17;
    slidertxtpos(:,4) = 14;
    
    xtraspace = floor(leftover/6);
else
    sliderunits = 'normalized';
    
    disp('This section for very small screens is experimental');
    % Set slider and text location
    sliderpos(:,1) = 0.1;
    slidertxtpos(:,1) = 0.15;
    sliderpos(:,2) = 1 - 0.05 - (1:numsliders)' * (0.9/numsliders) * 1.4;
    slidertxtpos(:,2) = 1 - 0.05 - (1:numsliders)' * (0.9/numsliders);
    
    % Set slider and text width & height
    sliderpos(:,3) = 0.8;
    slidertxtpos(:,3) = 0.8;
    sliderpos(:,4) = (0.9 / numsliders) * 0.55;
    slidertxtpos(:,4) = (0.9 / numsliders) * 0.35;
    
    xtraspace = 0;
end

% Slider for frame selection start
slidernum = 1;
slFrameStarttxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Start frame: ',num2str(output.startFrame)],...
    'HorizontalAlignment','left',...
    'Tag','StartFrameTxt');
slFrameStart = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeIm(3),'Value',output.startFrame,...
    'SliderStep',[1/(sizeIm(3)-1) 50/(sizeIm(3)-1)],...
    'Tag','StartFrame');

% Slider for frame selection end
slidernum = slidernum + 1;
slFrameEndtxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['End frame: ',num2str(output.endFrame)],...
    'HorizontalAlignment','left',...
    'Tag','EndFrameTxt');
slFrameEnd = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeIm(3),'Value',output.endFrame,...
    'SliderStep',[1/(sizeIm(3)-1) 50/(sizeIm(3)-1)],...
    'Tag','EndFrame');

% Create a little extra space before next sliders
sliderpos(slidernum+1:end,2) = sliderpos(slidernum+1:end,2) - xtraspace;
slidertxtpos(slidernum+1:end,2)=slidertxtpos(slidernum+1:end,2)-xtraspace;

% Slider for left side crop
slidernum = slidernum + 1;
slCropLefttxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Left crop: ',num2str(output.LCrop)],...
    'HorizontalAlignment','left',...
    'Tag','LeftCropTxt');
slCropLeft = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(2),'Value',output.LCrop,...
    'SliderStep',[1/(sizeImcorr(2)-1) 30/(sizeImcorr(2)-1)],...
    'Tag','LeftCrop');

% Slider for right side crop
slidernum = slidernum + 1;
if output.RCrop > sizeImcorr(2)
    output.RCrop = sizeImcorr(2);
elseif output.RCrop < 1
    output.RCrop = 1;
end
slCropRighttxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Right crop: ',num2str(output.RCrop)],...
    'HorizontalAlignment','left',...
    'Tag','RightCropTxt');
slCropRight = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(2),'Value',output.RCrop,...
    'SliderStep',[1/(sizeImcorr(2)-1) 30/(sizeImcorr(2)-1)],...
    'Tag','RightCrop');

% Slider for top side crop
slidernum = slidernum + 1;
if output.TCrop > sizeImcorr(1)
    output.TCrop = sizeImcorr(1);
elseif output.TCrop < 1
    output.TCrop = 1;
end
slCropToptxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Top crop: ',num2str(output.TCrop)],...
    'HorizontalAlignment','left',...
    'Tag','TopCropTxt');
slCropTop = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(1),'Value',output.TCrop,...
    'SliderStep',[1/sizeImcorr(1) 30/sizeImcorr(1)],...
    'Tag','TopCrop');

% Slider for bottom side crop
slidernum = slidernum + 1;
if output.BCrop > sizeImcorr(1)
    output.BCrop = sizeImcorr(1);
elseif output.BCrop < 1
    output.BCrop = 1;
end
slCropBottomtxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['Bottom crop: ',num2str(output.BCrop)],...
    'HorizontalAlignment','left',...
    'Tag','BottomCropTxt');
slCropBottom = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeImcorr(1),'Value',output.BCrop,...
    'SliderStep',[1/sizeImcorr(1) 30/sizeImcorr(1)],...
    'Tag','BottomCrop');

% Create a little extra space before next sliders
sliderpos(slidernum+1:end,2) = sliderpos(slidernum+1:end,2) - xtraspace;
slidertxtpos(slidernum+1:end,2)=slidertxtpos(slidernum+1:end,2)-xtraspace;

% Channel selector
slidernum = slidernum + 1;
if ~isfield(output,'channelmix')
    output.channelmix = 4;
end
popupChannelMixtxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String','Channel selection:',...
    'HorizontalAlignment','left',...
    'Tag','ChannelMixTxt');
popupChannelMix = uicontrol('Parent',cropFig.sidepanel,'Style','popupmenu',...
    'Callback',@channelmix_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'String',{'1','2','3','4 (mix)'},...
    'Value',output.channelmix,...
    'Tag','ChannelMix');

% Slider for minimum filter
slidernum = slidernum + 1;
if ~isfield(output,'bgminfilt')
    output.bgminfilt = 0;
end
if ~isfield(output,'bgsubblend')
    output.bgsubblend = 100;
end
slMinFilttxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['BG subtr. minimum filter: ',num2str(output.bgminfilt)],...
    'HorizontalAlignment','left',...
    'Tag','MinFiltTxt');
slMinFilt = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',0,'Max',sizeIm(3),'Value',output.bgminfilt,...
    'SliderStep',[2/(sizeIm(3)+1) 10/(sizeIm(3)+1)],...
    'Tag','MinFilt',...
    'ToolTipString',['Set minimum filter size for background to be ',...
    'subtracted. 0 and 1 mean off.']);

% Slider for median filter
slidernum = slidernum + 1;
slMedFilttxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['BG subtr. median filter: ',num2str(output.bgmedfilt)],...
    'HorizontalAlignment','left',...
    'Tag','MedFiltTxt');
slMedFilt = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',1,'Max',sizeIm(3),'Value',output.bgmedfilt,...
    'SliderStep',[2/(sizeIm(3)) 10/(sizeIm(3))],...
    'Tag','MedFilt',...
    'ToolTipString',['Set median filter size for background to be ',...
    'subtracted. 0 and 1 mean off.']);

% Slider for BG filter blend
slidernum = slidernum + 1;
slBgBlendtxt = uicontrol('Parent',cropFig.sidepanel,...
    'Style','text',...
    'Units',sliderunits,'Position',slidertxtpos(slidernum,:),...
    'String',['BG subtraction strength: ',num2str(output.bgsubblend),'%'],...
    'HorizontalAlignment','left',...
    'Tag','BGBlendTxt');
slBgBlend = uicontrol('Parent',cropFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'Units',sliderunits, 'Position',sliderpos(slidernum,:),...
    'Min',0,'Max',100,'Value',output.bgsubblend,...
    'SliderStep',[0.01 0.1],...
    'Tag','BGBlend',...
    'ToolTipString',['Set blend percentage of background subtraction, ',...
    '100% is full subtraction, 0% is no BG subtraction.']);


%% Adjust for requested options
% First, turn controls off

slFrameStarttxt.Visible = 'off';
slFrameStart.Visible = 'off';
slFrameEndtxt.Visible = 'off';
slFrameEnd.Visible = 'off';
slCropLefttxt.Visible = 'off';
slCropLeft.Visible = 'off';
slCropRighttxt.Visible = 'off';
slCropRight.Visible = 'off';
slCropToptxt.Visible = 'off';
slCropTop.Visible = 'off';
slCropBottomtxt.Visible = 'off';
slCropBottom.Visible = 'off';
popupChannelMixtxt.Visible = 'off';
popupChannelMix.Visible = 'off';
slMinFilttxt.Visible = 'off';
slMinFilt.Visible = 'off';
slMedFilttxt.Visible = 'off';
slMedFilt.Visible = 'off';
slBgBlendtxt.Visible = 'off';
slBgBlend.Visible = 'off';

% Then turn on desired controls
optiontmp = strsplit(option,',');
for i = 1:length(optiontmp)
    switch optiontmp{i}
        case 'trim'
            slFrameStarttxt.Visible = 'on';
            slFrameStart.Visible = 'on';
            slFrameEndtxt.Visible = 'on';
            slFrameEnd.Visible = 'on';
        case 'crop'
            slCropLefttxt.Visible = 'on';
            slCropLeft.Visible = 'on';
            slCropRighttxt.Visible = 'on';
            slCropRight.Visible = 'on';
            slCropToptxt.Visible = 'on';
            slCropTop.Visible = 'on';
            slCropBottomtxt.Visible = 'on';
            slCropBottom.Visible = 'on';
            popupChannelMixtxt.Visible = 'on';
            popupChannelMix.Visible = 'on';
        case 'bg'
            slMinFilttxt.Visible = 'on';
            slMinFilt.Visible = 'on';
            slMedFilttxt.Visible = 'on';
            slMedFilt.Visible = 'on';
            slBgBlendtxt.Visible = 'on';
            slBgBlend.Visible = 'on';
    end
end


%% Update image

brightness_Callback;
makeThePlot;
waitfor(cropFig.fig);


%% Callback functions

% Callback for frame change
    function [] = frame_Callback(source,~,~)
        
        currframe = round(get(source,'Value'));
        set(source,'Value',currframe);
        set(cropFig.frametxt,'String',['Frame: ',num2str(currframe)]);
        
        makeThePlot;
    end

% Callback brightness change
    function [] = brightness_Callback(~,~,~)
        brightness = round(cropFig.brightslider.Value);
        cropFig.brightslider.Value = brightness;
        cropFig.brighttxt.String = ['Brightness: ',num2str(brightness) '%'];
        
         % Apply brightness setting
        bright = 200 - brightness;
        temp = (0:.005:1).^(bright/100);
        axes(cropFig.axes);
        clrmp = [temp' temp' temp'];
        colormap(gca,clrmp);
        
%         makeThePlot;
    end

% Slider Callback
    function []=slider_Callback(source,~,~)
        sliderval = floor(get(source,'Value'));
        
        % If slider is median filter, make sure the value is odd
        if strcmp(source.Tag,slMedFilt.Tag)
            if mod(sliderval,2)==0
                sliderval = sliderval+1;
            end
            sliderval = min(sliderval,sizeIm(3));
        elseif strcmp(source.Tag,slMinFilt.Tag)
            if mod(sliderval,2)==0
                sliderval = sliderval+1;
            end
        end
        set(source,'Value',sliderval);
        
        slidertxtobj = findobj(cropFig.fig,'Tag',[source.Tag 'Txt']);
        sliderstr = get(slidertxtobj,'String');
        temp = strsplit(sliderstr,':');
        sliderstr = [temp{1} ': ' num2str(sliderval)];
        if strcmp(source.Tag,slBgBlend.Tag)
            sliderstr = [sliderstr '%'];
        end
        slidertxtobj.String = sliderstr;
        pause(0.002);
        
        makeThePlot;
    end

% Channelmix callback
function []=channelmix_Callback(~,~,~)
        output.channelmix = popupChannelMix.Value;
        % Clear cached image
        fullIMAGE = zeros(sizeImcorr(1),sizeImcorr(2),sizeIm(3),'uint8');
        pause(0.002);
        makeThePlot;
end


% Callback to finish
    function [] = finish_Callback(~,~,~)
        readAllParams;
        close(cropFig.fig); return
    end

%% Other functions

% Plot new image
    function [] = makeThePlot(~,~,~)
        slice = cropFig.frameslider.Value;
        readAllParams;
        
        % Calculate and subtract background if needed
        numfrneed = max([output.bgmedfilt,output.bgminfilt,1]);
        if output.bgsubblend < 1
            numfrneed = 1;
        end
        if numfrneed > 1
            startslice = round(max((slice-(numfrneed-1)/2),1));
            endslice = round(min((slice+(numfrneed-1)/2),sizeIm(3)));
            calcCorrImage(startslice:endslice);
            if output.bgminfilt>1 && output.bgmedfilt<=1    % Only min filt
                bg = min(fullIMAGE(:,:,startslice:endslice),[],3);
                dispimg = fullIMAGE(:,:,slice)-bg*(output.bgsubblend/100);
            elseif output.bgminfilt<=1 && output.bgmedfilt>1    % Only med filt
                bg = median(fullIMAGE(:,:,startslice:endslice),3);
                dispimg = fullIMAGE(:,:,slice)-bg*(output.bgsubblend/100);
            elseif output.bgminfilt>1 && output.bgmedfilt>1 % Both filters: slower!
                % Minimum filter is performed first
                bg = movmin(fullIMAGE(:,:,startslice:endslice),output.bgminfilt,3);
                dispimg = fullIMAGE(:,:,startslice:endslice) - bg;
%                 bg = medfilt3(dispimg,[1 1 output.bgmedfilt],'symmetric');
                tempslice = slice-startslice;
                startslice = round(max((tempslice-(output.bgmedfilt-1)/2),1));
                endslice = round(min((tempslice+(output.bgmedfilt-1)/2),size(dispimg,3)));
                bg = bg(:,:,tempslice) + median(dispimg(:,:,startslice:endslice),3);
                dispimg = fullIMAGE(:,:,slice)-bg*(output.bgsubblend/100);
            else
                dispimg = fullIMAGE(:,:,slice);
            end
            dispimg(dispimg<0)=0;
        else
            calcCorrImage(slice);
            dispimg = fullIMAGE(:,:,slice);
        end
        
        % Make grey if outside of selected frame range
        if or(slice<output.startFrame,slice>output.endFrame)
            dispimg = dispimg + 50;
        end
        
        % Show image
        axes(cropFig.axes);
        imshow(dispimg,clrmp);
        
        % Plot crop lines
        hold on
        plot([output.LCrop,output.LCrop,output.RCrop,...
            output.RCrop,output.LCrop],[output.TCrop,...
            output.BCrop,output.BCrop,output.TCrop,output.TCrop],'w--');
        hold off
    end

% Calculate new motion-corrected frames on demand
    function [] = calcCorrImage(corrFrame)
        % Loop through all requested frames
        for Fr = corrFrame
            % Only recalculate if frame has not been calculated
            if sum(sum(fullIMAGE(:,:,Fr))) == 0
%                 [X,Y] = meshgrid((1:sizeIm(2)) + ...
%                     max(output.motioncorr(:,2)) - ...
%                     output.motioncorr(Fr,2),...
%                     (1:sizeIm(1)) + max(output.motioncorr(:,3)) - ...
%                     output.motioncorr(Fr,3));
                [X,Y] = meshgrid((1:sizeIm(2)) + ...
                    output.motioncorr(Fr,2),...
                    (1:sizeIm(1)) + output.motioncorr(Fr,3));
                [Xq,Yq] = meshgrid(1:sizeImcorr(2),1:sizeImcorr(1));
                initIMAGE = interp2(X,Y,getFrame(Fr),Xq,Yq,'nearest',0);
                fullIMAGE(:,:,Fr) = (initIMAGE);
            end
        end
        
    end

    function readAllParams(~)
        % This function reads out all typical gui parameters
        output.startFrame = round(slFrameStart.Value);
        output.endFrame = round(slFrameEnd.Value);
        output.LCrop = round(slCropLeft.Value);
        output.RCrop = round(slCropRight.Value);
        output.TCrop = round(slCropTop.Value);
        output.BCrop = round(slCropBottom.Value);
        output.bgmedfilt = round(slMedFilt.Value);
        output.bgminfilt = round(slMinFilt.Value);
        output.bgsubblend = round(slBgBlend.Value);
        output.channelmix = popupChannelMix.Value;
    end

    function imx = getFrame(frno)
        % This function provides an image frame
        if imgstack == 1
            imx = IMAGE(:,:,frno);
        else
            if frno == (currframe+1)
                if hasFrame(vid)
                    imx = readFrame(vid);                % Read frame
                else
                    imx = zeros(vid.Height,vid.Width);
                end
            else
                vid.CurrentTime = (frno-1)/vid.FrameRate;
                currframe = frno;
                if hasFrame(vid)
                    imx = readFrame(vid);                % Read frame
                else
                    imx = zeros(vid.Height,vid.Width);
                end
            end
            
            % Make gray
            if output.channelmix>3
                imx = rgb2gray(imx);
            else
                imx = imx(:,:,output.channelmix);
            end
        end
end



end