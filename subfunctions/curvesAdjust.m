%% Information
% This program (called by selectFeatures) allows the user to set a curves
% adjustment to be applied before mask creation.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 13th, 2018

function [curvimg,curvmap,selectpoints] = curvesAdjust(IMAGE,selectpoints)

if nargin<2
    selectpoints = [0 0; 127 127; 255 255];
elseif selectpoints==0
    selectpoints = [0 0; 127 127; 255 255];
end
sizeIm = size(IMAGE);

%% Initialize window

curvFig = generalEditorWindow(IMAGE,500);
curvFig.fig.Name = "Curves adjustment";

% Reassign callbacks
curvFig.finishbutton.Callback = @finish_Callback;
curvFig.frameslider.Callback = @frame_Callback;
curvFig.brightslider.Visible = 'off';
curvFig.brighttxt.Visible = 'off';


%% Add controls

curvFig.sidepanel.Units = 'normalized';

% Curves label
uicontrol('Parent',curvFig.sidepanel,'Style','text',...
    'Units','normalized','Position',[0.1 0.90 0.8 0.03],...
    'String','Curves (click on curve to add points / drag to move)',...
    'HorizontalAlignment','left','Fontsize',14);

% Curves axes
caxes = axes(curvFig.sidepanel,'Units','normalized',...
    'Position',[0.05 0.45 0.9 0.45]);
lims = [0 255];
xlim(lims);
ylim(lims);
ConstrFcn = makeConstrainToRectFcn('impoint',lims,lims);

% Initialize some global variables for histogram
cc1 = 1;
curvimg = IMAGE;
histoldc = 0; edges = 0; histold = 0; histcurr = 0; histcurrc = 0;

% Histogram label
uicontrol('Parent',curvFig.sidepanel,'Style','text',...
    'Units','normalized','Position',[0.1 0.35 0.8 0.03],...
    'String','Histogram',...
    'HorizontalAlignment','left','Fontsize',14);

% Histogram axes
histaxes = axes(curvFig.sidepanel,'Units','normalized',...
    'Position',[0.05 0.05 0.9 0.3]);
histedges = 0:1:256;
initHist;

% Adjust finish button size
finishPosUnits = curvFig.finishbutton.Units;
finishPos = curvFig.finishbutton.Position;
curvFig.finishbutton.Position = [finishPos(1) + finishPos(3)/4,...
    finishPos(2), finishPos(3)/2, finishPos(4)];

% Reset button
uicontrol('Parent',curvFig.fig,'Style','pushbutton',...
    'Callback',@reset_Callback,'String','Reset',...
    'Units',finishPosUnits,...
    'Position',[finishPos(1) + 3.1*finishPos(3)/4,...
    finishPos(2) + 0.2*finishPos(4), finishPos(3)/4.3,...
    0.6*finishPos(4)],...
    'Fontsize',12);

axes(caxes);
xaxx = 0:1:255; % x axis for curves

curvplot = plot(caxes,selectpoints(:,1),selectpoints(:,2),...
    'ButtonDownFcn',@ImageClickCallback);
xlim([0 255]); ylim([0 255]);

for ix=1:size(selectpoints,1)
    pp(ix) = impoint(caxes,selectpoints(ix,1),selectpoints(ix,2));
    setPositionConstraintFcn(pp(ix),ConstrFcn);
    addNewPositionCallback(pp(ix),@plotCurve);
end

% If 2D image instead of 3D stack, remove frame slider capability
if size(IMAGE,3) == 1
    curvFig.frameslider.Visible = 'off';
    curvFig.frametxt.Visible = 'off';
end


%% Create image
axes(curvFig.axes);
currfr = curvFig.frameslider.Value;
previmg = imshow(IMAGE(:,:,currfr));
plotCurve;
waitfor(curvFig.fig);


%% Callback functions

% Callback for frame change
    function [] = frame_Callback(source,~,~)
        currframe = round(get(source,'Value'));
        set(source,'Value',currframe);
        set(curvFig.frametxt,'String',['Frame: ',num2str(currframe)]);
        previmg.CData = IMAGE(:,:,currframe);
        initHist;
        plotCurve;
    end

% Callback to reinitialize curves
function []=reset_Callback(~,~,~)
        selectpoints = [0 0; 127 127; 255 255];
        % Remove existing points from graph
        for ixx=1:numel(pp)
            delete(pp(ixx));
        end
        % Remove entries beyond initial points 
        if numel(pp)>size(selectpoints,1)
            pp(size(selectpoints,1)+1:end)=[];
        end
        % Add default points
        for ixx=1:size(selectpoints,1)
            pp(ixx) = impoint(caxes,selectpoints(ixx,1),selectpoints(ixx,2));
            setPositionConstraintFcn(pp(ixx),ConstrFcn);
            addNewPositionCallback(pp(ixx),@plotCurve);
        end
        plotCurve;
        initHist;
end

% Callback to finish
    function [] = finish_Callback(~,~,~)
        curvmap = edges';
        curvmap(:,2) = cc1;
        
        curvFig.finishbutton.String = 'Processing...';
        curvFig.finishbutton.BackgroundColor = [0.7 0.7 1];
        pause(0.005);
        
        % Allows double and not just integer values for better bit depth
        curvimg = double(IMAGE);
         % Get unique values to make this quicker
        [uniqvals,~,ic] = unique(IMAGE);
        % Double here is critical to maintain bitdepth of curves adjustment
        newuniqvals = double(uniqvals);
        for i=1:numel(uniqvals)
            newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
        end
        curvimg = reshape(newuniqvals(ic),sizeIm);
        
        close(curvFig.fig); return
    end


%% Other functions

% Initialize the histograms upon frame load
    function []=initHist(~)
        axes(histaxes)
        currim = IMAGE(:,:,curvFig.frameslider.Value);
        [histoldc, edges] = histcounts(currim,histedges,...
            'Normalization','probability');
        edges = edges(1:end-1);
        curvmap = edges';
        histoldc = histoldc / max(histoldc);
        histold = area(histaxes,edges(histoldc~=0),...
            histoldc(histoldc~=0),...
            'FaceColor',[0.8 0.8 0.8],'Linestyle','none');
        hold on
        histcurrc = histoldc;
        histcurr = area(histaxes,edges(histcurrc~=0),...
            histcurrc(histcurrc~=0),...
            'FaceColor',[0.4 0.4 0.6],'Linestyle','none');
        hold off
        xlim(lims);
    end

% Update the curve plot and image preview
    function [] = plotCurve(~)
        % Obtain point locations
        for i = 1:size(pp,2)
            selectpoints(i,:) = getPosition(pp(i));
        end
        [~,I] = sort(selectpoints(:,1));
        selectpoints = selectpoints(I,:);
        
        % Remove duplicate points
        [~,b] = unique(selectpoints(:,1));
        selectpoints = selectpoints(b,:);
        
        % Fit curve through selectpoints
        cc1 = spline(selectpoints(:,1),selectpoints(:,2),xaxx); % Calculate new luminosity values (decimal)
        cc1(cc1>255)=255;
        cc1(cc1<0)=0;
        % Plot that curve in caxis
        axes(caxes);
        curvplot.XData = xaxx;
        curvplot.YData = cc1;
        
        % Apply to colormap
        newcmap = cc1'*[1 1 1]/255;
        
        % Update image
        colormap(curvFig.axes,newcmap)
        
        % Update histogram
        % - Convert counts
        Y = discretize(cc1,histedges);
        histcurrc = zeros(numel(edges),1);
        for i=1:numel(histedges)
            histcurrc(i)=sum(histoldc(Y==i));
        end
        histcurrc = histcurrc/max(histcurrc);
        % - Update bar plot
        histcurr.XData = edges(histcurrc~=0);
        histcurr.YData = histcurrc(histcurrc~=0);
        
    end

%%
    function ImageClickCallback ( objectHandle , ~ )
        axesHandle  = get(objectHandle,'Parent');
        coordinates = get(axesHandle,'CurrentPoint');
        coordinates = coordinates(1,1:2);
        newpointindex = size(pp,2)+1;
        pp(newpointindex) = impoint(caxes,coordinates);
        setPositionConstraintFcn(pp(newpointindex),ConstrFcn);
        addNewPositionCallback(pp(newpointindex),@plotCurve);
    end


end
