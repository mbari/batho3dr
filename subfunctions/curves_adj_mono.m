function [curvimg,curvmap,selectpoints]=curv_adj_mono(IMAGE,selectpoints)

if nargin<2
    selectpoints = [0 0; 127 127; 255 255];
elseif selectpoints==0
    selectpoints = [0 0; 127 127; 255 255];
end

%% Initialize window settings

sizeIm=size(IMAGE);
wa=get(0,'screensize');
screenwidth=wa(3);screenheight=wa(4);

% Set initial OUTER window size
toolsize = 400;
outerwidth = sizeIm(2)+ toolsize + 150;
outerheight = sizeIm(1) + 150;
if or(outerwidth>screenwidth,outerheight>screenheight)
    outerwidth=screenwidth-70;
    outerheight=screenheight-120;
    xpos = 50;
    ypos = 100;
else
    xpos = (screenwidth-outerwidth)/2;
    ypos = (screenheight-outerheight)/2;
end


%% Build window and controls

hfig=figure('Menubar','none','Toolbar','none','Name','Curves adjustments',...
    'NumberTitle','on','IntegerHandle','off','Units','Pixels',...
    'InnerPosition',[xpos ypos outerwidth outerheight]);

% Determine inner size and set up other dimensions
temp = get(hfig,'InnerPosition');
width = temp(3); height = temp(4);
toolleft = width - toolsize - 40;
figsizex = toolleft - 40;
figsizey = height - 80 - 60;
if or(sizeIm(2)<figsizex,sizeIm(1)<figsizey)
    figsizex = sizeIm(2);
    figsizey = sizeIm(1);
end

% Build axes
haxes = axes(hfig,'Units','Pixels','Position',[20 80 figsizex figsizey]);

htxt=uicontrol('Parent',hfig,'Style','text',...
    'Position',[toolleft height-60 toolsize 20],...
    'String','Curves (click on curve to add points / drag to move)','HorizontalAlignment','left','Fontsize',15);

caxes = axes(hfig,'Units','Pixels',...
    'Position',[toolleft height/2 toolsize height/2-70]);
lims = [0 255];
xlim(lims);
ylim(lims);
ConstrFcn = makeConstrainToRectFcn('impoint',lims,lims);

htxthist=uicontrol('Parent',hfig,'Style','text',...
    'Position',[toolleft height/2-80 toolsize 20],...
    'String','Histogram','HorizontalAlignment','left','Fontsize',15);

% Finish button
button=uicontrol('Parent',hfig,'Style','pushbutton',...
    'Callback',@button_Callback,'String','Finish','Units','pixels',...
    'Position',[toolleft+(toolsize-200)/2 20 200 40],'Fontsize',16);

% Reset button
button=uicontrol('Parent',hfig,'Style','pushbutton',...
    'Callback',@reset_Callback,'String','Reset','Units','pixels',...
    'Position',[toolleft+(toolsize+240)/2 30 80 20],'Fontsize',12);

% Initialize some global variables
cc1=1;
curvimg = IMAGE;
histoldc = 0; edges = 0; histold = 0; histcurr = 0; histcurrc = 0;

histaxes = axes(hfig,'Units','Pixels',...
    'Position',[toolleft 110 toolsize height/2-200]);
histedges = 0:1:256;
initHist;

axes(caxes)
xaxx = 0:1:255;

curvplot = plot(caxes,selectpoints(:,1),selectpoints(:,2),'ButtonDownFcn',@ImageClickCallback);
xlim([0 255]); ylim([0 255]);

for i=1:size(selectpoints,1)
    pp(i) = impoint(caxes,selectpoints(i,1),selectpoints(i,2));
    setPositionConstraintFcn(pp(i),ConstrFcn);
    addNewPositionCallback(pp(i),@plotCurve);
end


% If 3D image, add slice capability
if numel(sizeIm)==3
    slice = round(sizeIm(3)/2);
    origIMAGE = IMAGE;
    IMAGE = origIMAGE(:,:,slice);
    
    % Add slice slider below image
    sliderlength = 300;
    htxt1=uicontrol('Style','text','Units','pixels',...
        'Position',[20+(sizeIm(2)-sliderlength)/2 40 sliderlength 20],...
        'String',['Slice: ',num2str(slice)],'HorizontalAlignment','left');
    slider1=uicontrol('Parent',hfig,'Style','slider','Callback',@slider1_Callback,...
        'String','Slice Number','Units','pixels','Position',[20+(sizeIm(2)-sliderlength)/2 20 sliderlength 20]);
    set(slider1,'Min',1,'Max',sizeIm(3),'Value',slice)
end

%% Create image
axes(haxes);
previmg = imshow(IMAGE);
plotCurve;

waitfor(hfig);

%% Functions
    function []=plotCurve(~)
        % Obtain point locations
        for i = 1:size(pp,2)
            selectpoints(i,:) = getPosition(pp(i));
        end
        [~,I] = sort(selectpoints(:,1));
        selectpoints = selectpoints(I,:);
        
        % Remove duplicate points
        [~,b] = unique(selectpoints(:,1));
        selectpoints = selectpoints(b,:);
        
        % Fit curve through selectpoints
        cc1 = spline(selectpoints(:,1),selectpoints(:,2),xaxx); % Calculate new luminosity values (decimal)
        cc1(cc1>255)=255;
        cc1(cc1<0)=0;
        % Plot that curve in caxis
        axes(caxes);
        curvplot.XData = xaxx;
        curvplot.YData = cc1;
        
        % Apply to colormap
        newcmap = cc1'*[1 1 1]/255;
        
        % Update image
        colormap(haxes,newcmap)
        
        % Update histogram
        % - Convert counts
        Y = discretize(cc1,histedges);
        histcurrc = zeros(numel(edges),1);
        for i=1:numel(histedges)
            histcurrc(i)=sum(histoldc(Y==i));
        end
        histcurrc = histcurrc/max(histcurrc);
        % - Update bar plot
        histcurr.XData = edges(histcurrc~=0);
        histcurr.YData = histcurrc(histcurrc~=0);
        
    end
%%
    function []=initHist(~)
        axes(histaxes)
        [histoldc, edges] = histcounts(IMAGE,histedges,'Normalization','probability');
        edges = edges(1:end-1);
        curvmap = edges';
        histoldc = histoldc / max(histoldc);
        % histold = bar(edges,histoldc,'EdgeColor','none','FaceColor',[0.7 0.7 0.7]);
        histold = area(histaxes,edges(histoldc~=0),histoldc(histoldc~=0),...
            'FaceColor',[0.8 0.8 0.8],'Linestyle','none');
        hold on
        histcurrc = histoldc;
        % histcurr = bar(edges,histcurrc,'EdgeColor','none','FaceColor',[0.2 0.2 0.2]);
        histcurr = area(histaxes,edges(histcurrc~=0),histcurrc(histcurrc~=0),...
            'FaceColor',[0.4 0.4 0.6],'Linestyle','none');
        hold off
        xlim(lims);
    end
%%
    function ImageClickCallback ( objectHandle , ~ )
        axesHandle  = get(objectHandle,'Parent');
        coordinates = get(axesHandle,'CurrentPoint');
        coordinates = coordinates(1,1:2);
        newpointindex = size(pp,2)+1;
        pp(newpointindex) = impoint(caxes,coordinates);
        setPositionConstraintFcn(pp(newpointindex),ConstrFcn);
        addNewPositionCallback(pp(newpointindex),@plotCurve);
    end

%%
function []=slider1_Callback(slider1,~,~)
        slice=round(get(slider1,'Value'));
        set(htxt1,'String',['Slice: ',num2str(slice)]);
        IMAGE = origIMAGE(:,:,slice);
        previmg.CData = IMAGE;
        initHist;
        plotCurve;
end

%%
    function []=button_Callback(button,~,~)
        curvmap = edges';
        curvmap(:,2) = cc1;
        
        % Special treatment if 3D
        if numel(sizeIm)==3
            IMAGE = origIMAGE;
        end
        set(button,'String','Processing...');
        set(button,'Backgroundcolor',[0.7 0.7 1]);
        pause(0.005);
        
%         tic
%         curvimg = double(IMAGE);            % Allows double and not just integer values for better bit depth
%         uniqvals = unique(IMAGE);
%         for i=1:numel(uniqvals)
%             % Can take a while, so update status text every 10 loops
%             if mod(i,10)==0
%                 progr = round(100*i/numel(uniqvals));
%                 set(button,'String',['Processing... (' num2str(progr) '%)']);
%                 pause(0.001);
%             end
%             curvimg(IMAGE==uniqvals(i)) = curvmap(uniqvals(i)==curvmap(:,1),2);
%         end
%         size(curvimg)
%         toc
%         curvimg2=curvimg;

        curvimg = double(IMAGE);            % Allows double and not just integer values for better bit depth
        [uniqvals,~,ic] = unique(IMAGE);
        newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
        for i=1:numel(uniqvals)
            newuniqvals(i) = (curvmap(uniqvals(i)==curvmap(:,1),2));
        end
        curvimg = reshape(newuniqvals(ic),sizeIm);
        
        assignin('caller','selectpoints',selectpoints);
        assignin('caller','curvimg',curvimg);
        assignin('caller','curvmap',curvmap);
        close; return
    end

    function []=reset_Callback(~,~,~)
        selectpoints = [0 0; 127 127; 255 255];
        % Remove existing points from graph
        for ixx=1:numel(pp)
            delete(pp(ixx));
        end
        % Remove entries beyond initial points 
        if numel(pp)>size(selectpoints,1)
            pp(size(selectpoints,1)+1:end)=[];
        end
        % Add default points
        for ixx=1:size(selectpoints,1)
            pp(ixx) = impoint(caxes,selectpoints(ixx,1),selectpoints(ixx,2));
            setPositionConstraintFcn(pp(ixx),ConstrFcn);
            addNewPositionCallback(pp(ixx),@plotCurve);
        end
        plotCurve;
        initHist;
    end
end
