function [newmask] = editpix3D(mask1,basemask,action,slice,slrange,slicedir)
if nargin < 5
    slrange = 40;
end
if nargin < 6
    slicedir = 1;   % Forward extent is default [symmetric = 0, forward = 1, backward = -1]
end
brushsize = 5;
if str2num(action(end))>0
    % if number appended, it must mean brush size
    brushsize = str2num(action(10:end));
    action = 'addmanual';
end
% Whole function copied from editblobs3D, some some terms may not make
% sense
% action = 'remove';
% action should be 'remove'/'delete' or 'add'/'include'
% slice is only required when using 3D masks

if ~strcmp(action,'addmanual')
    M = imfreehand(gca,'Closed','true');
else
%     disp('nonclosed');
    M = imfreehand(gca,'Closed','false');
end
% title('Calculating new mask...');
if size(M,1)~=0
pts=(M.getPosition);    % Get points on line
if size(pts,1)==1   % If only a single point selected
    ptsx = pts(1);
    ptsy = pts(2);
else
    if ~strcmp(action,'addmanual')
        N = regionfill(uint8(M.createMask),pts(:,2),pts(:,1));  % Fill freehand object
        [ptsy, ptsx] = find(N>0);       % Find selected region
    else
        pts=round(pts);
        ptsx=pts(:,1);
        ptsy=pts(:,2);
    end
end
title('Calculating new mask...');

if slicedir==0  % If symmetric extent
    slstrt = max(ceil(slice-slrange/2),1);
    slend = min(floor(slice+slrange/2),size(mask1,3));
elseif slicedir==1    % If in forward direction
    slstrt = slice;
    slend = min((slice+slrange-1),size(mask1,3));
elseif slicedir==-1    % If in backward direction
    slstrt = max(ceil(slice-slrange+1),1);
    slend = slice;
end

if or(strcmp(action,'remove'),strcmp(action,'delete'))
    if numel(size(basemask)) == 3       % If 3D mask
        title('Calculating...');pause(0.005);
        newmask = uint8(mask1);
        for lp=slstrt:slend
            newmask(:,:,lp) = newmask(:,:,lp)-N;
        end
        
%         tempmask=bwselect(logical(mask1(:,:,slice)),ptsx,ptsy,4);  % Select blobs within selection in 2D
%         [ptsy, ptsx] = find(tempmask>0);       % Find selected region
%         tempmask2=bwselect3(logical(mask1(:,:,slstrt:slend)),ptsx,ptsy,ptsx*0+slice-slstrt+1,18); 
%         % Select connected areas
%         tempmask = logical(mask1)*0; tempmask(:,:,slstrt:slend) = tempmask2;
        title('');pause(0.003);
    else
%         tempmask=bwselect(logical(mask1),ptsx,ptsy,4);  % Select connected areas
        newmask=uint8(mask1)-uint8(N);   % Remove areas
    end
    
elseif or(strcmp(action,'add'),strcmp(action,'include'))
    if numel(size(basemask)) == 3       % If 3D mask
        title('Calculating...');pause(0.005);
        newmask = uint8(mask1);
        secmask = uint8(basemask);
        for lp=slstrt:slend
            newmask(:,:,lp) = newmask(:,:,lp)+secmask(:,:,lp).*N;
        end
        title('');pause(0.003);
    else
        newmask=uint8(mask1)+uint8(basemask).*uint8(N);   % Add areas
    end
elseif strcmp(action,'addmanual')   % Manual point addition
    
%     disp('to code');
    pointmask = uint8(mask1(:,:,1)*0);
    badind = or(or(ptsy>size(pointmask,1),ptsx>size(pointmask,2)),or(ptsy<1,ptsx<1));
    ptsx(badind) = [];
    ptsy(badind) = [];
    indices = sub2ind(size(pointmask), ptsy, ptsx);
    pointmask(indices) = 1;
    if brushsize>1
    SE = strel('disk',brushsize-1);
    pointmask = imdilate(pointmask,SE);
    end
    if numel(size(basemask)) == 3       % If 3D mask
        title('Calculating...');pause(0.005);
        newmask = uint8(mask1);
%         secmask = uint8(basemask);
        for lp=slstrt:slend
            newmask(:,:,lp) = newmask(:,:,lp)+pointmask;
        end
        title('');pause(0.003);
    else
        newmask=uint8(mask1)+pointmask;   % Add pixel areas
    end
    
else
    disp('You have to specify what you want to do with this shape. Check the code!');
end
else
    newmask = mask1>0;
end
title('');
