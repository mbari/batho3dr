% This function creates a curves mapping for 8 bit monochrome images
function curvmap = create_curvmap(curvpoints)
xaxx = (0:1:255)';
cc1 = spline(curvpoints(:,1),curvpoints(:,2),xaxx); % Calculate new luminosity values (decimal)
cc1(cc1>255)=255;
cc1(cc1<0)=0;
curvmap = xaxx;
curvmap(:,2) = cc1;
end