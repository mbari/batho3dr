%% Information
% This program (called by ThreeDR_FeatureSelect) loads a GUI to set basic
% settings for luminosity-based mask creation.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 13th, 2018

function [featFig,output] = selectFeatures(IMAGE,output)

%% Initialize window settings

% Load existing settings, or make new ones.
if isstruct(output) && isfield(output,'curves')
    % Load settings automatically
    curvpoints = output.curves;
    numfeatures = numel(output.names);
else
    output.filt=1;
    output.filt3D=1;
    curvpoints = [0 0; 127 127; 255 255];
    numfeatures = 4;
    threshinit = round(255/(numfeatures+1):255/(numfeatures+1):255);
    output.minthresh = threshinit(1:end-1)+1;
    output.maxthresh = threshinit(2:end);
    output.names = cellstr([repmat('feat',numfeatures,1),...
        num2str((1:numfeatures)')]);
end

% Create curves mapping
curvmap = create_curvmap(curvpoints);


%% Initialize window

featFig = generalEditorWindow(IMAGE);
featFig.fig.Name = "Generate masks for 3DR features";


%% Set additional control parameters

featFig.sidepanel.Units = 'pixels';
sidepos = featFig.sidepanel.Position;

% Reassign callbacks
featFig.finishbutton.Callback = @finish_Callback;
featFig.frameslider.Callback = @frame_Callback;
featFig.brightslider.Visible = 'off';
featFig.brighttxt.Visible = 'off';

threshlim1=0;threshlim2=255;
minFilt=1; maxFilt=20; startFilt=output.filt;
minFilt3D=1; maxFilt3D=21; startFilt3D=output.filt3D;

controlpos = zeros(13,4);     % Array of sidebar control locations

% Start out with pixel units
controlunits = 'pixels';

% Set button, slider and text locations in pixel units
controlpos(:,1) = 0.15 * sidepos(3); % General x position
controlpos(:,3) = sidepos(3) * 0.7; % General width
controlpos(:,4) = 17;               % General height

% Adjust for individual controls
controlpos(1,4) = 30;       % Curves button height
controlpos(1,2) = sidepos(4) - controlpos(1,4) - 5; % curves button Y

controlpos(2:end,2) = controlpos(1,2) - ...
    ((1:size(controlpos,1)-1)') * 20;   % Pre-set all other y positions

controlpos(3:end,2) = controlpos(3:end,2) + 8;
controlpos(3,4) = 14;       % 2D median filter slider text
controlpos(6:end,2) = controlpos(6:end,2) + 8;
controlpos(6,4) = 14;       % 3D median filter slider text
controlpos(9:end,2) = controlpos(9:end,2) + 8;
controlpos(9,4) = 14;       % 3D median filter slider text
controlpos(12:end,2) = controlpos(12:end,2) + 8;
controlpos(12,2) =  controlpos(12,2) - 5;    % number of features text
controlpos(13,2) =  controlpos(12,2);    % number of features box Y
controlpos(13,1) =  170;    % number of features box Y
controlpos(13,3) =  50;    % number of features box Y

% Set feature panel position in pixels
featpanelpos = [0.05 * sidepos(3), 10, 0.9 * sidepos(3), 250];

% If the number of vertical pixels is too high, change to normalized units
if featpanelpos(4)+featpanelpos(2) > min(controlpos(:,2))
%     controlunits = 'normalized';
    
    disp('This section for very small screens/windows is to be coded');
    
%     goherenow
end


%% Add additional controls

% Add a control to set the view mode
viewmodecontr = uicontrol('Parent',featFig.toppanel,'Style','popupmenu',...
    'Callback',@updateImage,...
    'String',{'Multi-feature view','Single feature view','Full image'},...
    'Units','normalized','Position',[0.6 0.05 0.4 0.5],'Value',1);

% Curves button
curvbutt = uicontrol('Parent',featFig.sidepanel,'Style','pushbutton',...
    'Callback',@curvbutt_Callback,...
    'String','Curves adjustments','Units',controlunits,...
    'Position',controlpos(1,:),...
    'Fontsize',15,...
    'Tag','curvesbutton');

% 2D median filter slider and text
sliderMed2Dtxt = uicontrol('Parent',featFig.sidepanel,'Style','text',...
    'Units',controlunits,'Position',controlpos(3,:),...
    'String',['2D median filter size: ',num2str(startFilt)],...
    'HorizontalAlignment','left',...
    'Tag','med2DTxt');
sliderMed2D = uicontrol('Parent',featFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,...
    'String','Threshold','Units',controlunits,...
    'Position',controlpos(4,:),...
    'SliderStep',[0.1 0.3],...
    'Min',minFilt,'Max',maxFilt,'Value',startFilt,...
    'Tag','med2D');

% 3D median filter and text
sliderMed3Dtxt = uicontrol('Parent',featFig.sidepanel,'Style','text',...
    'Units',controlunits,'Position',controlpos(6,:),...
    'String',['3D median filter size: ',num2str(startFilt)],...
    'HorizontalAlignment','left',...
    'Tag','med3DTxt');
sliderMed3D = uicontrol('Parent',featFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,'String','Threshold',...
    'Units',controlunits,'Position',controlpos(7,:),...
    'SliderStep',[0.1 0.3],...
    'Min',minFilt3D,'Max',maxFilt3D,'Value',startFilt3D,...
    'Tag','med3D');

% 3D maximum filter and text
sliderMaxZtxt = uicontrol('Parent',featFig.sidepanel,'Style','text',...
    'Units',controlunits,'Position',controlpos(9,:),...
    'String',['Z-maximum filter size (exp.): ',num2str(0)],...
    'HorizontalAlignment','left',...
    'Tag','maxZTxt');
sliderMaxZ = uicontrol('Parent',featFig.sidepanel,'Style','slider',...
    'Callback',@slider_Callback,'String','Threshold',...
    'Units',controlunits,'Position',controlpos(10,:),...
    'SliderStep',[1/31 5/31],...
    'Min',0,'Max',30,'Value',0,...
    'Tag','maxZ');


% Set number of features
numfeatcontrtxt = uicontrol('Parent',featFig.sidepanel,'Style','text',...
    'Units',controlunits,'Position',controlpos(12,:),...
    'String','Number of features: ','HorizontalAlignment','left',...
    'Tag','numFeatTxt');
numfeatcontr = uicontrol('Parent',featFig.sidepanel,'Style','edit',...
    'Callback',@changeNumFeatures,'String',num2str(numfeatures),...
    'Units',controlunits,'Position',controlpos(13,:),...
    'Tag','numFeat');


%% Feature tabs

tabgp = uitabgroup(featFig.sidepanel,'Units',controlunits,...
    'Position',featpanelpos,...
    'SelectionChangedFcn',@updateImage);

panelunits = 'normalized';
panelpos = zeros(6,4);
panelpos(:,1) = 0.05;   % General control x position
panelpos(:,3) = 0.9;   % General control x extent
panelpos(:,4) = 0.08;   % General control y extent
spacer = 0.07;
txtheight = 0.07;
controlheight = 0.09;

panelpos(1,2) = 1 - spacer - txtheight;
panelpos(2,2) = panelpos(1,2) - controlheight - 0.02;
panelpos(2,4) = panelpos(2,4) + 0.02;

panelpos(3,2) = panelpos(2,2) - spacer - txtheight;
panelpos(4,2) = panelpos(3,2) - controlheight;

panelpos(5,2) = panelpos(4,2) - spacer - txtheight;
panelpos(6,2) = panelpos(5,2) - controlheight;

threshstepsmall = 1/256;
threshsteplarge = 15/256;
threshlim1 = 0;
threshlim2 = 255;

% Loop through number of features to fill tab panels
for i=1:numfeatures
    feattab(i) = uitab(tabgp,'Title',output.names{i},...
        'Tag',['Feat' num2str(i)]);
    
    % Feature name text and control
    uifeatnametxt(i) = uicontrol(feattab(i),'Style','text',...
        'Units',panelunits,...
        'Position',panelpos(1,:),...
        'String','Feature name:','HorizontalAlignment','left',...
        'Tag',['featName' num2str(i) 'Txt']);
    uifeatname(i) = uicontrol(feattab(i),'Style','edit',...
        'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
        'String',output.names(i),...
        'Units',panelunits,'Position',panelpos(2,:),...
        'HorizontalAlignment','left',...
        'Tag',['featName' num2str(i)]);
    
    % Minimum threshold text and control
    uithreshmintxt(i) = uicontrol(feattab(i),'Style','text',...
        'Units',panelunits,'Position',panelpos(3,:),...
        'String',['Min Threshold: ',num2str(output.minthresh(i))],...
        'HorizontalAlignment','left',...
        'Tag',['threshMin' num2str(i) 'Txt']);
    uithreshmin(i) = uicontrol(feattab(i),'Style','slider',...
        'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
        'String',['Min Threshold Feature ' num2str(i)],...
        'Units',panelunits,'Position',panelpos(4,:),...
        'SliderStep',[threshstepsmall threshsteplarge],...
        'Min',threshlim1,'Max',threshlim2,...
        'Value',output.minthresh(i),...
        'Tag',['threshMin' num2str(i)]);
    
    % Maximum threshold text and control
    uithreshmaxtxt(i) = uicontrol(feattab(i),'Style','text',...
        'Units',panelunits,'Position',panelpos(5,:),...
        'String',['Max Threshold: ',num2str(output.maxthresh(i))],...
        'HorizontalAlignment','left',...
        'Tag',['threshMax' num2str(i) 'Txt']);
    uithreshmax(i) = uicontrol(feattab(i),'Style','slider',...
        'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
        'String',['Max Threshold Feature ' num2str(i)],...
        'Units',panelunits,'Position',panelpos(6,:),...
        'SliderStep',[threshstepsmall threshsteplarge],...
        'Min',threshlim1,'Max',threshlim2,...
        'Value',output.maxthresh(i),...
        'Tag',['threshMax' num2str(i)]);
end

updateImage;
waitfor(featFig.fig);


%% Callbacks

% Change frame
    function [] = frame_Callback(~,~,~)
        
        currframe = round(featFig.frameslider.Value);
        featFig.frameslider.Value = currframe;
        featFig.frametxt.String = ['Frame: ',num2str(currframe)];
        
        updateImage;
    end

% Callback to finish
    function [] = finish_Callback(~,~,~)
        
        readAllParams;
        
        close(featFig.fig); return
    end

% Slider Callback
    function []=slider_Callback(source,~,~)
        sliderval = round(source.Value);
        % Correct to odd value if median filter
        if or(strcmp(source.Tag,'med2D'),strcmp(source.Tag,'med3D'))
            if mod(sliderval,2)==0  % If medfilt2D is even
            sliderval = sliderval + 1;    % Make it odd!
            end
        end
        source.Value = sliderval;
        
        slidertxtobj = findobj(featFig.fig,'Tag',[source.Tag 'Txt']);
        sliderstr = get(slidertxtobj,'String');
        temp = strsplit(sliderstr,':');
        sliderstr = [temp{1} ': ' num2str(sliderval)];
        slidertxtobj.String = sliderstr;
        pause(0.002);
        
        updateImage;
    end

% Curves adjustments callback
    function [] = curvbutt_Callback(~,~,~)
        
        currfr = featFig.frameslider.Value;
        im = IMAGE(:,:,currfr);
        [~,curvmap,curvpoints] = curvesAdjust(im,curvpoints);
        output.curves = curvpoints;
        updateImage;
    end

%goherenow
%%

function []=featureChange_Callback(src,~)
    featnum = str2double(src.Parent.Tag(end));
    if strcmp(src.Style,'edit') == 1      % If edit box (name change)
        output.names(featnum) = get(src,'String');
        feattab(featnum).Title = output.names{featnum};
    else
        switch src.String(1:3)
            case 'Min'
                output.minthresh(featnum) = round(src.Value);
                src.Value = output.minthresh(featnum);
                set(uithreshmintxt(featnum),'String',...
                    ['Min Threshold: ',num2str(src.Value)]);
            case 'Max'
                output.maxthresh(featnum) = round(src.Value);
                src.Value = output.maxthresh(featnum);
                set(uithreshmaxtxt(featnum),'String',...
                    ['Max Threshold: ',num2str(src.Value)]);
        end
        
        updateImage;
    end
end


%% Other functions

    function readAllParams(~)
        % This function reads out all typical gui parameters
        
        % Remove duplicate curvpoints
        [~,b] = unique(curvpoints(:,1));
        
        output.filt = get(sliderMed2D,'Value');
        output.filt3D = get(sliderMed3D,'Value');
        output.curves = curvpoints(b,:);
    end

% Update the image
    function []=updateImage(~,~,~)
        set(featFig.updtxt,'Visible','on');
        pause(0.005);
        
        slice=featFig.frameslider.Value;
        medfilt=get(sliderMed2D,'Value');
        medfilt3D=get(sliderMed3D,'Value');
        maxZfilt = get(sliderMaxZ,'Value');
%         im=IMAGE(:,:,(slice-(medfilt3D-1)/2):(slice+(medfilt3D-1)/2));
        im=IMAGE(:,:,ceil(slice-((medfilt3D+maxZfilt)-1)/2):ceil(slice+((medfilt3D+maxZfilt)-1)/2));
        sizeImtemp = size(im);
        
        % Apply curves
        curvimg = double(im);            % Allows double and not just integer values for better bit depth
        [uniqvals,~,ic] = unique(curvimg);
        newuniqvals=double(uniqvals);       % Double here is critical to maintain bitdepth of curves adjustment
        for i2=1:numel(uniqvals)
            newuniqvals(i2) = (curvmap(uniqvals(i2)==curvmap(:,1),2));
        end
        im = reshape(newuniqvals(ic),sizeImtemp);
        
        % 3D median filter
        im = medfilt3(im,[medfilt medfilt medfilt3D],'symmetric');
        
        
        % Z maximum filter
        if maxZfilt>0
            tmp = ceil((medfilt3D+maxZfilt)/2)-floor((maxZfilt-1)/2):ceil((medfilt3D+maxZfilt)/2)+floor(maxZfilt/2);
            im = im(:,:,tmp);
            im = max(im,[],3);
        else
            im = im(:,:,ceil(medfilt3D/2));     % Show only middle part
        end
        
        axes(featFig.axes);
        switch viewmodecontr.Value
            case 1 %'Multi-feature view'
                % Mask out sections
                newim = zeros(size(im));
                colors = jet(numfeatures);
                colors = [0 0 0;colors];
                
%                 newim = zeros(size(im,1),size(im,2),3);
        
                for ii=1:numfeatures
%                     mask=bwareaopen(bwmorph(bwmorph(setthresh(im,...
%                         output.minthresh(ii),output.maxthresh(ii)),'spur'),'close'),output.imclosesize(ii));
%                     mask=bwareaopen(bwmorph(setthresh(im,...
%                         output.minthresh(ii),output.maxthresh(ii)),'spur'),output.imclosesize(ii));
%                     mask=bwmorph(bwmorph(setthresh(im,...
%                         output.minthresh(ii),output.maxthresh(ii)),'spur'),'open',output.imclosesize(ii));
                    mask=bwmorph(setthresh(im,...
                        output.minthresh(ii),output.maxthresh(ii)),'spur');
%                     mask3D = false(size(newim));
%                     mask3D(:,:,1) = mask;
%                     mask3D(:,:,2) = mask;
%                     mask3D(:,:,3) = mask;
%                     type mask3D
%                     tempim = im(logical(mask));
                    % Set the right colors here!!! Choose between overlay
                    % (luminosity levels within feature visible) or
                    % high-contrast
                    
                  
%                     newim(mask) = im(mask)*(ii/numfeatures)+(ii-1)*255/numfeatures;
                    newim(mask)=ii;
%                     newim(mask3D) = colors(ii,:);
%                     newim(:,:,1) = logical(mask)*colors(ii,1);
%                     newim(:,:,2) = logical(mask)*colors(ii,2);
%                     newim(:,:,3) = logical(mask)*colors(ii,3);
                    feattab(ii).ForegroundColor = colors(ii+1,:);
                end
%                 unique(newim.*im)
                imshow(newim,[]);
                colormap(featFig.axes,colors);
                
            case 2 %'Single feature view'
                % Mask out sections
                ii = str2double(tabgp.SelectedTab.Tag(end));
%                 mask=bwareaopen(bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
% output.maxthresh(ii)),'spur'),'close'),output.imclosesize(ii));
% mask=bwareaopen(bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
%     output.maxthresh(ii)),'spur'),'close',output.imclosesize(ii)),1);
mask=bwmorph(setthresh(im,...
                        output.minthresh(ii),output.maxthresh(ii)),'spur');
%                 mask=bwmorph(bwmorph(setthresh(im,output.minthresh(ii),...
% output.maxthresh(ii)),'spur'),'close');
%                 se = strel('disk',output.imclosesize(ii));
%                 mask = ~(imerode((~mask),se));
%                 imt=setthresh(im,output.minthresh(ii),output.maxthresh(ii));
% save('imtemp.mat','imt');


% Perhaps a combination of the following:
% - 
                imshow(im.*mask,[]);
                colormap(featFig.axes,gray);
                
                % Set colors of tabs
                for ii=1:numfeatures
                    feattab(ii).ForegroundColor = [0 0 0];
                end
                
            case 3 %'Full image'
                imshow(im,[]);
                colormap(featFig.axes,gray);
                % Set colors of tabs
                for ii=1:numfeatures
                    feattab(ii).ForegroundColor = [0 0 0];
                end
        end
        set(featFig.updtxt,'Visible','off');
    end

%%
    function []=changeNumFeatures(~,~,~)
        numfeatures = str2double(numfeatcontr.String);
        newlines = numfeatures - numel(feattab);
        
        if newlines < 0
            % Remove one or more tabs
            delete(feattab(numfeatures+1:end));
            output.names(numfeatures+1:end) = [];
            output.minthresh(numfeatures+1:end) = [];
            output.maxthresh(numfeatures+1:end) = [];
            
            feattab(numfeatures+1:end)=[];
        elseif newlines > 0
            threshinit = round(255/(numfeatures+1):255/(numfeatures+1):255);
            output.minthresh(numel(feattab)+1:numfeatures) = ...
                threshinit(numel(feattab)+1:end-1)+1;
            output.maxthresh(numel(feattab)+1:numfeatures) = ...
                threshinit(numel(feattab)+2:end);
            
            % Add one or more tabs
            for ix=numel(feattab)+1:numfeatures
                output.names(ix) = {['Feat',num2str(ix)]};
                feattab(ix) = uitab(tabgp,'Title',output.names{ix},...
                    'Tag',['Feat' num2str(ix)]);
                
                % Feature name text and control
                uifeatnametxt(ix) = uicontrol(feattab(ix),'Style','text',...
                    'Units',panelunits,...
                    'Position',panelpos(1,:),...
                    'String','Feature name:','HorizontalAlignment','left',...
                    'Tag',['featName' num2str(ix) 'Txt']);
                uifeatname(ix) = uicontrol(feattab(ix),'Style','edit',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',output.names(ix),...
                    'Units',panelunits,'Position',panelpos(2,:),...
                    'HorizontalAlignment','left',...
                    'Tag',['featName' num2str(ix)]);
                
                % Minimum threshold text and control
                uithreshmintxt(ix) = uicontrol(feattab(ix),'Style','text',...
                    'Units',panelunits,'Position',panelpos(3,:),...
                    'String',['Min Threshold: ',num2str(output.minthresh(ix))],...
                    'HorizontalAlignment','left',...
                    'Tag',['threshMin' num2str(ix) 'Txt']);
                uithreshmin(ix) = uicontrol(feattab(ix),'Style','slider',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',['Min Threshold Feature ' num2str(ix)],...
                    'Units',panelunits,'Position',panelpos(4,:),...
                    'SliderStep',[threshstepsmall threshsteplarge],...
                    'Min',threshlim1,'Max',threshlim2,...
                    'Value',output.minthresh(ix),...
                    'Tag',['threshMin' num2str(ix)]);
                
                uithreshmaxtxt(ix) = uicontrol(feattab(ix),'Style','text',...
                    'Units',panelunits,'Position',panelpos(5,:),...
                    'String',['Max Threshold: ',num2str(output.maxthresh(ix))],...
                    'HorizontalAlignment','left',...
                    'Tag',['threshMax' num2str(ix) 'Txt']);
                uithreshmax(ix) = uicontrol(feattab(ix),'Style','slider',...
                    'Callback',@(src,eventdata)featureChange_Callback(src,eventdata),...
                    'String',['Max Threshold Feature ' num2str(ix)],...
                    'Units',panelunits,'Position',panelpos(6,:),...
                    'SliderStep',[threshstepsmall threshsteplarge],...
                    'Min',threshlim1,'Max',threshlim2,...
                    'Value',output.maxthresh(ix),...
                    'Tag',['threshMax' num2str(ix)]);
                
            end
        else
            % Same number; don't change a thing
        end
        
        % Set new default values
        for ix = 1:numfeatures
            set(uithreshmin(ix),'Min',threshlim1,'Max',threshlim2,'Value',output.minthresh(ix));
            set(uithreshmax(ix),'Min',threshlim1,'Max',threshlim2,'Value',output.maxthresh(ix));
        end
        
        updateImage;        
        
    end

end