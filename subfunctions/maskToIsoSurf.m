function [stlPath] = maskToIsoSurf(maskPath,imfile,vals,calibration)
% vals may be a single value or an array
global altmode

%% Verify inputs
if nargin < 1
    maskPath = '0';
end

% If no valid mask path selected, select one
if ~(exist(maskPath,'file')==2)
    % Select videos copied to the temporary folder, or browse to a new one
    matPath = loadVideoFile;
    
    % If matPath is not a valid file, abort
    if exist(matPath,'file')==0
        return
    end
    
    % Get crop image file name
    [fpath, fname, ~] = fileparts(matPath);
    
    % Select feature
    maskPath = loadFeature(fpath,'MASK','.mat');
end

% Guess image file if necessary
if nargin < 2
    temp = strsplit(maskPath,'feature');
    imfile = [temp{1} 'IMAGEcrop_nonbg.mat'];
end

% If image file does not exist, select one
while ~(exist(imfile,'file')==2)
    [fpath, fname, ~] = fileparts(matPath);
    disp('Please select an associated image file (.mat)');
    [imfile,impath,~] = uigetfile({'*.mat'},...
        'Select an associated image file',fpath);
    imfile = [impath filesep imfile]; %#ok<AGROW>
end

% Set isovalues if necessary
if nargin < 3
    temp = inputdlg('Enter desired isovalue(s), separated by commas:',...
        'Isovalues', [1 50]);
    vals = str2double(strsplit(temp{:},','));
end

% Retrieve / request calibration if not provided
if nargin < 4
    par_file = [fpath, filesep, strrep(fname,'_IMAGE',''), '_params.mat'];
    
    if exist(par_file,'file')==2
        load(par_file,'output');
    else
        output.a = 0;
    end
    
    % Set rotation to zero if it doesn't exist
    if ~isfield(output,'calibration_UVWrotation')
            output.calibration_UVWrotation = [0,0,0];
    end
    
    if or(~isfield(output,'calibration_XYpixcal'),altmode==1)
        
        % Prefill if exists
        if isfield(output,'calibration_XYpixcal')
            calvals = {num2str(output.calibration_XYpixcal),...
                num2str(output.calibration_Zvelocity),...
                sprintf('%d,%d,%d',output.calibration_XYZoffset),...
                sprintf('%d,%d,%d',output.calibration_UVWrotation)};
        else
            calvals = {'','','',''};
        end
        
        cals = inputdlg({'X & Y pixel calibration (mm/pixel)',...
            'Z velocity (mm/frame)',['Origin offset (format: x,y,z) ',...
            '[optional]'],'Rotation (format: u,v,w) [optional]'},...
            'Calibration values', [1 50; 1 50; 1 50; 1 50],calvals);
        output.calibration_XYpixcal = str2double(cals{1});
        output.calibration_Zvelocity = str2double(cals{2});
        if ~isempty(cals{3})
            output.calibration_XYZoffset = str2num(cals{3}); %#ok<ST2NM>
        else
            output.calibration_XYZoffset = [0,0,0];
        end
        if ~isempty(cals{4})
            output.calibration_UVWrotation = str2num(cals{4}); %#ok<ST2NM>
        else
            output.calibration_UVWrotation = [0,0,0];
        end
        
        if exist(par_file,'file')==2
            save(par_file,'output');
            disp('New calibration saved.');
        end
    end
    
    
    calib = [output.calibration_XYpixcal, output.calibration_XYpixcal,...
        output.calibration_Zvelocity, output.calibration_XYZoffset];
else
    calib = calibration;
end
rot = output.calibration_UVWrotation;

disp('Calibrations:');
disp(['  X&Y: ',num2str(calib(1)),' mm/pix']);
disp(['  Z: ',num2str(calib(3)),' mm/frame']);
disp(['  offsets: [',num2str(calib(4)),',',num2str(calib(5)),',',...
    num2str(calib(6)),'] pix']);
disp(['  rotation: [',num2str(rot(1)),',',num2str(rot(2)),',',...
    num2str(rot(3)),'] degrees']);


%% Load files

if exist(imfile,'file')==2
    fprintf('  Loading image stacks...');
    load(maskPath,'MASKfilter');
    fl = load(imfile);
    names = fieldnames(fl);
    eval(['IMAGEcurvmedfilt = double(fl.' names{1} ');']);
        
    % Get mask limits to reduce array sizes
    margins = 2;    % Maximum pixel margin around ROI box
    xmasklim=sum(sum(MASKfilter,1),3)>0;xmasklim=xmasklim(:);
    ymasklim=sum(sum(MASKfilter,2),3)>0;ymasklim=ymasklim(:);
    zmasklim=sum(sum(MASKfilter,2),1)>0;zmasklim=zmasklim(:);
    xstart = max(find(xmasklim>0,1,'first')-margins,1);
    xend = min(find(xmasklim>0,1,'last')+margins,size(MASKfilter,2));
    ystart = max(find(ymasklim>0,1,'first')-margins,1);
    yend = min(find(ymasklim>0,1,'last')+margins,size(MASKfilter,1));
    zstart = max(find(zmasklim>0,1,'first')-margins,1);
    zend = min(find(zmasklim>0,1,'last')+margins,size(MASKfilter,3));
    
    fprintf([' Trimmed matrices from ' num2str(size(MASKfilter,1)) 'x' ...
        num2str(size(MASKfilter,2)) 'x' num2str(size(MASKfilter,3))]);
    
    IMAGEcurvmedfilt = IMAGEcurvmedfilt(ystart:yend,xstart:xend,zstart:zend);
    MASKfilter = MASKfilter(ystart:yend,xstart:xend,zstart:zend);
    
    fprintf([' to ' num2str(size(MASKfilter,1)) 'x' ...
        num2str(size(MASKfilter,2)) 'x' num2str(size(MASKfilter,3)) '.\n']);
    
    IMAGE = IMAGEcurvmedfilt.*double(MASKfilter);
%     IMAGE=smooth3(IMAGEcurvmedfilt).*double(MASKfilter);
%     IMAGE=IMAGEcurvmedfilt.*smooth3(double(MASKfilter),'box',5);
    
%     clear IMAGEcurvmedfilt
%     clear MASKfilter
%     IMAGE = IMAGEcurvmedfilt*0;
%     for i=1:1:size(IMAGEcurvmedfilt,3)
%         IMAGE(:,:,i)=IMAGEcurvmedfilt(:,:,i).*maskPath(:,:,i);
%     end
else
    error('  Cropped image stack does not exist. Run ThreeDR_CropPreProc.m.');
end


%% Smoothing adjacent images in stack

imsm_file = [maskPath(1:end-4) 'sm.mat'];
smoothDate = 0;

% Get mask creation date of previously smoothed file
if exist(imsm_file,'file')==2
    load(imsm_file,'maskDate');
    if exist('maskDate','var')==1
        smoothDate = maskDate; %#ok<NODEF>
    else
        smoothDate = 0;
    end
end
maskInfo = dir(maskPath);
maskDate = maskInfo.datenum;

% If mask is newer (or shift was pressed), ignore old smoothed file, otherwise load.
if or(maskDate > smoothDate, altmode == 1)
    disp('  Smoothing images in image stack...')
%     IMAGEsm = IMAGE;
    IMAGEsm=smooth3(IMAGE);          %smooth images
    save(imsm_file,'IMAGEsm','maskDate','-v7.3');        %save post-processing parameters
else
    disp('  Loading post-processed image stack...');
    load(imsm_file,'IMAGEsm');
end


%% Creating isosurfaces

stlPath = cell(length(vals),1);
superres = [1 1 1];

for i=1:length(vals)
    val = vals(i);
    
    outdata=[imsm_file(1:end-4),'_',num2str(val),'val_fv.mat'];
    outstl=[outdata(1:end-4),'.stl'];
    % outstlred=[outdata(1:end-4),'red.stl'];   % Only needed if using patch
    
    % If file exists and mask date agrees with smooth date, ignore
    if and(and(exist(outstl,'file')==2, maskDate == smoothDate),altmode==0)
        disp(['  Isosurface data file ' outstl ' already exists.']);
        %     disp(['  Loading isosurface data with val = ',num2str(val),'...']);
        %     load(outdata,'fv');
    else
        disp('  Generating meshgrid...');
        if ~(exist('xx','var')==1)
            [X,Y,Z]=meshgrid(xstart:xend,ystart:yend,zstart:zend);
            if numel(calib)>=3
                if numel(calib)>3
                    X = X-calib(4);
                    Y = Y-calib(5);
                    Z = Z-calib(6);
                end
                xx=single(X)*calib(1);
                yy=single(Y)*calib(2);
                zz=single(Z)*calib(3);
            end
            
            % If rotation needed
            if sum(abs(rot))>0
                disp('  Rotating meshgrid...');
                rot = rot*pi/180;
                c1=cos(rot(3));
                c2=cos(rot(2));
                c3=cos(rot(1));
                s1=sin(rot(3));
                s2=sin(rot(2));
                s3=sin(rot(1));
                R_zyx=[c1*c2 c1*s2*s3-c3*s1 s1*s3+c1*c3*s2;
                    c2*s1 c1*c3+s1*s2*s3 c3*s1*s2-c1*s3;
                    -s2 c2*s3 c2*c3];
                temp=[xx(:),yy(:),zz(:)]*R_zyx.' ;
                sz = size(xx);
                xx = reshape(temp(:,1),sz);
                yy = reshape(temp(:,2),sz);
                zz = reshape(temp(:,3),sz);
            end
            
            clear X Y Z
        end
        
        disp(['  Calculating isosurface data with val = ',num2str(val),'...']);
        % Do interpolation only if non-unity superresolution parameter is
        % found
        if sum(abs(superres - [1 1 1]))>0
            fprintf('  Interpolating...')
            
            
            
            if ~(exist('xx2','var')==1)
                [X2,Y2,Z2]=meshgrid(xstart:(1/superres(1)):xend,...
                    ystart:(1/superres(2)):yend,...
                    zstart:(1/superres(3)):zend);
                if numel(calib)>=3
                    if numel(calib)>3
                        X2 = X2-calib(4);
                        Y2 = Y2-calib(5);
                        Z2 = Z2-calib(6);
                    end
                    xx2=single(X2)*calib(1);
                    yy2=single(Y2)*calib(2);
                    zz2=single(Z2)*calib(3);
                end
                clear X2 Y2 Z2
            end
            tic
            % 1D interp method
            IMAGEsm_int = zeros(size(xx2,1),size(xx2,2),size(xx2,3));
            IMAGEcurvmedfilt2 = zeros(size(xx2,1),size(xx2,2),size(xx2,3));
            MASKfilter2 = zeros(size(xx2,1),size(xx2,2),size(xx2,3));
            zarrold = zz(1,1,:); zarrold = zarrold(:);
            zarrnew = zz2(1,1,:); zarrnew = zarrnew(:);
            save(outdata);
            for xxx=1:size(xx2,2)
                for yyy = 1:size(xx2,1)
                    size(zarrold)
                    size(shiftdim(IMAGEcurvmedfilt(yyy,xxx,:),2))
%                     IMAGEsm_int(yyy,xxx,:) = interp1(zarrold,IMAGEsm(yyy,xxx,:),zarrnew);
                    IMAGEcurvmedfilt2(yyy,xxx,:) = interp1(zarrold,shiftdim(IMAGEcurvmedfilt(yyy,xxx,:),2),zarrnew);
                    MASKfilter2(yyy,xxx,:) = interp1(zarrold,double(shiftdim(MASKfilter(yyy,xxx,:),2)),zarrnew);
                end
            end
            IMAGE = IMAGEcurvmedfilt2.*double(MASKfilter2);
            IMAGEsm_int2=smooth3(IMAGE);
            save(outdata);
            
%             % 3D interp method
%             IMAGEsm_int = interp3(xx,yy,zz,IMAGEsm,xx2,yy2,zz2,'makima');

            fprintf([' done (' num2str(toc) ' seconds).\n']);
            tic
%             save(outdata,'xx2','yy2','zz2','IMAGEsm_int');
            fv = isosurface(xx2,yy2,zz2,IMAGEsm_int2,val,'verbose');
        else
            tic
            fv = isosurface(xx,yy,zz,IMAGEsm,val,'verbose');
%             save(outdata,'xx','yy','zz','IMAGEsm','fv');
            
        end
        disp(['  Isosurface took ',num2str(toc),' seconds.']);
        
        disp(['  Saving isosurface data with val = ',num2str(val),'...']);
        stlwrite2018(outstl,fv);
%         save(outdata,'fv');
        %     disp(['  Reducing number of faces by factor 20...']);
        %     tic
        %     nfv = reducepatch(fv,0.05);
        %     stlwrite(outstlred,nfv);
        %     toc
    end
    % % disp('Plotting 3D reconstruction');
    % h=figure(3);
    % set(h,'Position',[100,100,1000,1000]);
    % disp(['  Plotting isosurface data with val = ',num2str(val),'...']);
    % hiso=patch(fv,'FaceColor',[0.5,0.5,0.65],'EdgeColor','none');
    % isonormals(IMAGEsm,hiso)
    % alpha(0.2)  %sets level of transparency
    % view(3)
    % camlight
    % lighting gouraud
    % grid on
    % daspect([1 1 1]);
    % pause(5)
    % savefig([outdata(1:end-4),'.fig']);
    stlPath(i) = {outstl};
end
altmode = 0;
clear xx yy zz
