%% Information
% This program allows automated stabilization of frames and manual tracking
% of a feature to aid in stabilization. 
% At this time, only XY translation is corrected with manual tracking,
% and rotation is not. You will need to track a single feature through the
% entire clip, but the algorithm is adaptive in that it takes the past few
% frames into account to determine the feature to look for.

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2021
% Date: August 2021

function [motioncorrout,trackpoints,autostab] = MotionTracker(inputstack,...
    trackpoints,autostab,opt)
% inputstack: 3D matrix of input image data, or ref to video file
% trackpoints: any existing manual tracking points
% autostab: existing manual stabilization parameters

% Note 1: The program will run without any inputs and simply request the
% required data.

% Note 2: inputstack may be a path to a video file, it does not have to be an
% actual stack. In fact, this may be more efficient in the case of large
% files.

if nargin<2     % If no track points predefined
    trackpoints = [0,0,0];
    alltrackpoints = {trackpoints};
elseif iscell(trackpoints)
    alltrackpoints = trackpoints;
    trackpoints = alltrackpoints{1};
else
    alltrackpoints = {trackpoints};
end

laseraux = 0;       % Setting to 1 initiates laser background subtraction code
mask = [];
exportnow = false;
if nargin>3
    % Read options
    if isfield(opt,'laseraux')
        if opt.laseraux == 1
            laseraux = 1;
        end
    end
    if isfield(opt,'exportnow')
        if opt.exportnow == true
            exportnow = true;
            if ~isfield(opt,'exportnow_channelmix')
                opt.exportnow_channelmix = 4;
            end
%             exportnow_outpath = '';
        end
    end
end

newVidPath = 0;
extension = 0;
channelmix = 0;
extFile = 0;    % External file or not?
vid = 0;        % External video handle
sidecarFileName = 'null';   % Side car file path for external file
vidsz = [1 1 1];      % Size of video frames

if nargin<3     % If no track points predefined
    autostab = struct();
end

% Initialize automatic stabilization parameters
roirect = 0;
trackpointsInt = [];
stabilizing = 0;
% Possible values for stabilizing: 0 = nothing done, 0.5 = manual only, 1 =
% automatic in progress, 2 = automatic completed, 2.5 = automatic done and
% manual also

currpoint = 0;
currpointMoved = 0;
stabplotX = 0;          % Used to refer to plot indicator
motioncorr = [0 0 0];
motioncorrref = 0;      % Reference for motioncorr
motioncorrout = 0;
autoCropCorr = [0 0 0 0];
illCorr = 0;
corrMatrix = [];
corrBG = [];

% If no input stack given, load a file
if nargin < 1
    loadImgStack;
elseif isa(inputstack,'char')
    loadImgStack(inputstack);
else
    vidsz = size(inputstack);
    allthetforms = zeros(3,3,vidsz(3));
    allthetforms(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
end

if ~isfield(autostab,'ytr')
    autostab.xtr = zeros(1,vidsz(3));
    xtr = zeros(1,vidsz(3));
    ytr = zeros(1,vidsz(3));
    rot = zeros(1,vidsz(3));
    % Type of automatic motion correction: no motion (0) or smooth motion (1)
    motionType = 0;
    referenceType = 'moving';
    interpMethod = 'linear';    % Default interpolation method
    autoCrop = 0;               % Default auto crop setting
else
    xtr = autostab.xtr;
    ytr = autostab.ytr;
    rot = autostab.rot;
    motionType = autostab.motionType;
    if isfield(autostab,'referenceType')
        referenceType = autostab.referenceType;
    else
        referenceType = 'moving';
    end
    if isfield(autostab,'interpMethod')
        interpMethod = autostab.interpMethod;
    else
        interpMethod = 'nearest';
    end
    if isfield(autostab,'autoCrop')
        autoCrop = autostab.autoCrop;
        if size(trackpoints,1)>2
            CalcCorrection;
        end
        calcCropCorr;
    else
        autoCrop = 0;
    end
    
    if iscell(trackpoints)
        alltrackpoints = trackpoints;
        trackpoints = alltrackpoints{1};
    elseif iscell(alltrackpoints)
    else
        alltrackpoints = {trackpoints};
    end
end
      
startROI = [vidsz(2)/2-vidsz(2)/10, vidsz(1)/2-vidsz(1)/10,...
    vidsz(2)/5, vidsz(1)/5];
global mmT


%% Initialize window

currfr = 1;
mmT = generalEditorWindow(getExtFrame(currfr));

% Set dimensions to normalized to allow resizing (experimental)
mmT.axes.Units = 'normalized';
mmT.toppanel.Units = 'normalized';
mmT.botpanel.Units = 'normalized';
mmT.sidepanel.Units = 'normalized';
mmT.updtxt.Units = 'normalized';
mmT.finishbutton.Units = 'normalized';

if newVidPath == 0
    mmT.fig.Name = 'Motion Tracker';
else
    [~,nm,~] = fileparts(newVidPath);
    mmT.fig.Name = ['Motion Tracker - ' nm];
end

mmT.frameslider.Max = vidsz(3);
mmT.frameslider.SliderStep = [1/vidsz(3) 10/vidsz(3)];
mmT.frameslider.Value = currfr;
mmT.frametxt.String = ['Frame: ',num2str(currfr)];
imobj = mmT.axes.Children;

%% Add controls

mmT.sidepanel.Units = 'pixels';
sidepos = mmT.sidepanel.Position;
mmT.sidepanel.Units = 'normalized';

% Reassign callbacks
mmT.finishbutton.Callback = @finish_Callback;
mmT.frameslider.Callback = @frame_Callback;
mmT.brightslider.Callback = @brightness_Callback;

% Add 'load video' button
buttonLoadVid = uicontrol('Parent',mmT.sidepanel,'Style','pushbutton',...
    'Callback',@loadImgStack,...
    'String','Load video file...',...
    'Units','pixels',...
    'Position',[0.2*sidepos(3), sidepos(4) - 50, 0.6*sidepos(3), 30]);

if sidepos(4) > 610+160
    smallWindow = 0;
    panelunits = 'pixels';
    panelautopos = [0.1*sidepos(3), sidepos(4) - 20 - 380, ...
        0.8*sidepos(3), 300];
    panelmanualpos = [0.1*sidepos(3), sidepos(4) - 610, ...
        0.8*sidepos(3), 200];
    paneloutputpos = [0.1*sidepos(3), sidepos(4) - 610-120-10, ...
        0.8*sidepos(3), 120];
    prevaxespos = [0.1*sidepos(3), 60, ...
        0.8*sidepos(3), paneloutputpos(2) - 70];
    prevaxespos(prevaxespos<0) = 0;
else
    smallWindow = 1;
    panelunits = 'pixels';
    panelautoh = 150;%180;  % 0.6 * (sidepos(4) - 70)
    panelmanualh = 130;  % 0.3 * (sidepos(4) - 110)
    paneloutputh = 110; % 0.3 * (sidepos(4) - 110)
    panelautopos = [0.1*sidepos(3), sidepos(4) - 60 - ...
    panelautoh, 0.8*sidepos(3), panelautoh];
    panelmanualpos = [0.1*sidepos(3), ...
        panelautopos(2)-panelmanualh-10, 0.8*sidepos(3), ...
        panelmanualh];
    paneloutputpos = [0.1*sidepos(3), ...
        panelmanualpos(2)-paneloutputh-10,...
        0.8*sidepos(3), paneloutputh];
    
    prevaxesposy = 60;
    prevaxesposh = paneloutputpos(2)-prevaxesposy-10;
    if prevaxesposh > 80
        prevaxespos = [0.1*sidepos(3), ...
            prevaxesposy, 0.8*sidepos(3), prevaxesposh];
        smallWindow = 0;
    else
        prevaxespos = [0.1*sidepos(3), prevaxesposy, ...
            0.8*sidepos(3), ...
            paneloutputpos(2)+paneloutputpos(4)-prevaxesposy];
    end
    %     prevaxespos = panelmanualpos;
    if ispc
        warning('Still need to test small window on Windows');
    end
end

% Add axes inside sidepanel
prevaxes = axes('Parent',mmT.sidepanel,...
    'Units','Pixels',...
    'Position',prevaxespos,...
    'Visible','off');
if (smallWindow == 0) && (stabilizing > 1)
    prevaxes.Visible = 'On';
    updatePreviewPlot;
end

% Add panel "point tracker" inside sidepanel
panelauto = uipanel('Parent',mmT.sidepanel,...
    'Title','Point tracker',...
    'FontSize',12,'Units',panelunits,...
    'Position',panelautopos,...
    'BorderType','etchedin');

% "Set ROI" button
buttonROI = uicontrol('Parent',panelauto,'Style','pushbutton',...
    'Callback',@setROI_Callback,...
    'String','Select ROI',...
    'Units','normalized',...
    'Position',[0.15, 0.85, 0.7, 0.10],...
    'ToolTipString',['Select region to analyze. A smaller area will ',...
    'process faster. Tracking is intensity-based, so pick a region ',...
    'of decent contrast.']);

% Radio buttons for trans&rot / trans only
bg1 = uibuttongroup('Parent',panelauto,...
    'Units','normalized',...
    'Position',[0.1 0.66 0.8 0.18],...
    'BorderType','none');
r11 = uicontrol(bg1,'Style',...
    'radiobutton',...
    'String','Translation & rotation [exp.]',...
    'Units','normalized',...
    'Position',[0.1 0.01 0.9 0.44],...
    'Enable','off');
r12 = uicontrol(bg1,'Style',...
    'radiobutton',...
    'String','Translation only',...
    'Units','normalized',...
    'Position',[0.1 0.51 0.9 0.44]);

% Radio buttons for reference type
bg3 = uibuttongroup('Parent',panelauto,...
    'Units','normalized',...
    'Position',[0.1 0.43 0.8 0.18],...
    'BorderType','none');
r31 = uicontrol(bg3,'Style',...
    'radiobutton',...
    'String','Fixed reference (frame 1)',...
    'Units','normalized',...
    'Position',[0.1 0.01 0.9 0.44],...
    'Enable','on',...
    'Value',strcmp(referenceType,'fixed'));
r32 = uicontrol(bg3,'Style',...
    'radiobutton',...
    'String','Moving reference',...
    'Units','normalized',...
    'Position',[0.1 0.51 0.9 0.44],...
    'Value',strcmp(referenceType,'moving'));

% Radio buttons for smooth motion / no motion
bg2 = uibuttongroup('Parent',panelauto,...
    'Units','normalized',...
    'Position',[0.1 0.20 0.8 0.18],...
    'BorderType','none');
r21 = uicontrol(bg2,'Style',...
    'radiobutton',...
    'String','Smooth motion [exp.]',...
    'Units','normalized',...
    'Position',[0.1 0.01 0.9 0.44],...
    'Callback',@motionType_Callback,...
    'Value',motionType);
r22 = uicontrol(bg2,'Style',...
    'radiobutton',...
    'String','No motion',...
    'Units','normalized',...
    'Position',[0.1 0.51 0.9 0.44],...
    'Callback',@motionType_Callback,...
    'Value',~motionType);

bg1.SelectedObject = r12;
bg2.SelectedObject = r22;

% "Stabilize" button
buttonStab = uicontrol('Parent',panelauto,'Style','pushbutton',...
    'Callback',@autoStab_Callback,...
    'String','Stabilize',...
    'Units','normalized',...
    'Position',[0.15, 0.05, 0.7, 0.10],...
    'ToolTipString','Start analysis and apply stabilization.');

% Add panel for streak straightener
panelautostreak = uipanel('Parent',mmT.sidepanel,...
    'Title','Streak straightener',...
    'FontSize',12,'Units',panelunits,...
    'Position',panelautopos,...
    'BorderType','etchedin','Visible','Off');

% "Open Streak Straightener" button
buttonStreakStraightener = uicontrol('Parent',panelautostreak,'Style','pushbutton',...
    'Callback',@streakStart_Callback,...
    'String','Start Streak Straightener',...
    'Units','normalized',...
    'Position',[0.15, 0.65, 0.7, 0.15],...
    'ToolTipString','Load streak straightener interface');

% "Load Streak Straightener" button
buttonLoadStreakStraightener = uicontrol('Parent',panelautostreak,'Style','pushbutton',...
    'Callback',@streakLoad_Callback,...
    'String','Load streak straightener results',...
    'Units','normalized',...
    'Position',[0.10, 0.25, 0.8, 0.15],...
    'ToolTipString','Load results from streak straightener');

autobuttonpos = panelautopos;
autobuttonpos = [panelautopos(1) + 3*panelautopos(3)/5, ...
    panelautopos(2) + 0.85 * panelautopos(4),...
    2*panelautopos(3)/5, 0.2 * panelautopos(4)];

% Auto stab mode button
buttonAutoStabMode = uicontrol('Parent',mmT.sidepanel,'Style','pushbutton',...
    'Callback',@autoStabMode_Callback,...
    'String','Streak straightener',...
    'Units',panelunits,...
    'Position',autobuttonpos,...
    'ToolTipString','Load results from streak straightener');


% Add panel "manual feature tracking" inside sidepanel
panelmanual = uipanel('Parent',mmT.sidepanel,...
    'Title','Manual feature tracking',...
    'FontSize',12,'Units',panelunits,...
    'Position',panelmanualpos,...
    'BorderType','etchedin');

% Add popup for track selection
% "Add primary point" button
popupChangeTrack = uicontrol('Parent',panelmanual,'Style','popupmenu',...
    'Callback',@changeTrack_Callback,...
    'String',{'Track 1','Add track...'},...
    'Units','normalized',...
    'Position',[0.20, 0.7 ,0.6, 0.2]);
if numel(alltrackpoints)>1
    temp = {};
    for i = 1:numel(alltrackpoints)
        temp{i} = ['Track ' num2str(i)];
    end
    temp{end+1} = 'Add track...';
    popupChangeTrack.String = temp;
end

% "Add primary point" button
buttonAddPoint = uicontrol('Parent',panelmanual,'Style','pushbutton',...
    'Callback',@addPoint_Callback,...
    'String','Add control point',...
    'Units','normalized',...
    'Position',[0.20, 0.50 ,0.6, 0.18]);

% "Jump to previous point" button
buttonPrevPoint = uicontrol('Parent',panelmanual,'Style','pushbutton',...
    'Callback',@jumpToPoint_Callback,...
    'String','<',...
    'Units','normalized',...
    'Position',[0.05, 0.52 ,0.1, 0.14],...
    'Enable','off');

% "Jump to next point" button
buttonNextPoint = uicontrol('Parent',panelmanual,'Style','pushbutton',...
    'Callback',@jumpToPoint_Callback,...
    'String','>',...
    'Units','normalized',...
    'Position',[0.85, 0.52 ,0.1, 0.14],...
    'Enable','off');

% "Clear control points" button
buttonManualReset = uicontrol('Parent',panelmanual,'Style','pushbutton',...
    'Callback',@clearManual_Callback,...
    'String','Clear control points',...
    'Units','normalized',...
    'Position',[0.20, 0.28 ,0.6, 0.18],...
    'Enable','off');

% "Apply manual tracking" button
buttonManualApply = uicontrol('Parent',panelmanual,'Style','pushbutton',...
    'Callback',@applyManual_Callback,...
    'String','Apply correction',...
    'Units','normalized',...
    'Position',[0.15, 0.04 ,0.7, 0.20],...
    'Enable','off');


% Add panel "output options" inside sidepanel
paneloutput = uipanel('Parent',mmT.sidepanel,...
    'Title','Output options',...
    'FontSize',12,'Units',panelunits,...
    'Position',paneloutputpos,...
    'BorderType','etchedin');


% Radio buttons interpolation method
bgint = uibuttongroup('Parent',paneloutput,...
    'Units','normalized',...
    'Position',[0.1 0.43 0.8 0.54],...
    'BorderType','none');
rInt1 = uicontrol(bgint,'Style',...
    'radiobutton',...
    'String','Nearest neighbor',...
    'Units','normalized',...
    'Position',[0.1 0.67 0.9 0.3],...
    'Value',strcmp(interpMethod,'nearest'),...
    'Callback',@interpMethod_Callback);
rInt2 = uicontrol(bgint,'Style',...
    'radiobutton',...
    'String','Linear interpolation',...
    'Units','normalized',...
    'Position',[0.1 0.34 0.9 0.3],...
    'Value',strcmp(interpMethod,'linear'),...
    'Callback',@interpMethod_Callback);
rInt3 = uicontrol(bgint,'Style',...
    'radiobutton',...
    'String','Cubic interpolation',...
    'Units','normalized',...
    'Position',[0.1 0.01 0.9 0.3],...
    'Value',strcmp(interpMethod,'cubic'),...
    'Callback',@interpMethod_Callback);
% Auto crop check box
buttonAutoCrop = uicontrol('Parent',paneloutput,'Style','checkbox',...
    'Callback',@autocrop_Callback,...
    'String','Auto crop',...
    'Units','normalized',...
    'Position',[0.18 0.25 0.72 0.15],...
    'Value',autoCrop,...
    'Enable','on');
% Illumination correction check box
buttonIllCorr = uicontrol('Parent',paneloutput,'Style','checkbox',...
    'Callback',@autocrop_Callback,...
    'String','Illumination correction',...
    'Units','normalized',...
    'Position',[0.18 0.06 0.72 0.15],...
    'Value',illCorr,...
    'Enable','off');
if extFile == 1
    buttonIllCorr.Enable = 'on';
end


% Add 'export video' button
buttonExportVideo = uicontrol('Parent',mmT.sidepanel,'Style','pushbutton',...
    'Callback',@exportVideo,...
    'String','Export video...',...
    'Units','pixels',...
    'Position',[0.3*sidepos(3), 5, 0.4*sidepos(3), 30]);


% Set finish button function to export video
if nargin < 1
    mmT.finishbutton.String = 'Export & close...';
end

if size(trackpoints,1)>2
    set(findall(panelauto, '-property', 'enable'), 'enable', 'off');
    buttonManualApply.Enable = 'on';
    buttonManualReset.Enable = 'on';
    stabilizing = 0;
    if sum(abs(autostab.xtr))>0
        stabilizing = 2;
    end
%     buttonManualApply.String = 'Disable correction';
end

% Make sure any existing settings from sidecar are loaded
if stabilizing > 0
    buttonStab.String = 'Clear stabilization';
    buttonROI.Enable = 'off';
    r12.Enable = 'off';
    if mod(stabilizing,1) > 0
        % Set manual track settings
        if numel(alltrackpoints)>1
            tempxx = {};
            for ixx = 1:numel(alltrackpoints)
                tempxx{ixx} = ['Track ' num2str(ixx)];
            end
            tempxx{end+1} = 'Add track...';
            popupChangeTrack.String = tempxx;
        end
    end
end

% Enable Loupe
set(mmT.axes,'ButtonDownFcn',@showLoupe)

updateImage;
updateContrPtButtons;
autocrop_Callback;      % Make sure auto crop settings are generated

% Export as desired (if applicable) and close window
if exportnow
    channelmix = opt.exportnow_channelmix;
    exportVideo;
    close(mmT.fig)
end

waitfor(mmT.fig);

%% Control callbacks

% Apply frame selector change
    function [] = frame_Callback(~,~,~)
        TrackCurrPoint;
        ffr = round(mmT.frameslider.Value);
        mmT.frameslider.Value = ffr;
        mmT.frametxt.String = ['Frame: ',num2str(ffr)];
        pause(0.02);
        
        currpoint = 0;
        roirect = 0;
        
        updateImage;
        currfr = mmT.frameslider.Value;
        updateContrPtButtons;
        
        if size(trackpoints,1)>1
            if trackpoints(2,2) == 0
                warning('zero point found; check trackpoints');
                trackpoints
            end
        end
    end

% Apply brightness change
    function [] = brightness_Callback(source,~,~)
        TrackCurrPoint;
        
        brightness = round(get(source,'Value'));
        set(source,'Value',brightness);
        mmT.brighttxt.String = ['Brightness: ',num2str(brightness) '%'];
        
        currpoint = 0;
        
        updateImage;
    end


% Set ROI for automated stabilization
    function setROI_Callback(~,~,~)
        % Go to frame 1, then build existing ROI
%         inputstack = originput;
        mmT.frameslider.Value = 1;
        mmT.frametxt.String = ['Frame: ',num2str(1)];
        updateImage;
        pause(0.002);
        
        % draw existing roi
        roirect = imrect(mmT.axes, startROI);
        roifcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),...
            get(gca,'YLim'));
        setPositionConstraintFcn(roirect,roifcn);
        
    end

% Change motion correction type (smooth / no motion)
    function motionType_Callback(~,~,~)
        % Get value (0 is no motion)
        motionType = r21.Value;
        
        % If stabilized, refresh, otherwise, do nothing
        if stabilizing > 1
            if motionType > 0
                % Smooth the motion
                int = min([60, round(vidsz(3)/4)]); % Sample interval
                % X
                temp = movmedian(autostab.xtr,60);     % Moving median
                xtr = autostab.xtr - spline(1:int:vidsz(3),...
                    temp(1:int:vidsz(3)),1:vidsz(3));
                xtr = xtr-min(xtr);
                % Y
                temp = movmedian(autostab.ytr,60);     % Moving median
                ytr = autostab.ytr - spline(1:int:vidsz(3),...
                    temp(1:int:vidsz(3)),1:vidsz(3));
                ytr = ytr-min(ytr);
                % Rotation
                temp = movmedian(autostab.rot,60);     % Moving median
                rot = autostab.rot - spline(1:int:vidsz(3),...
                    temp(1:int:vidsz(3)),1:vidsz(3));
            else
                xtr = autostab.xtr;
                ytr = autostab.ytr;
                rot = autostab.rot;
            end
            
            updatePreviewPlot;
            
            updateImage;
        end
        
    end

% Update preview plot
    function updatePreviewPlot(~)
            axes(prevaxes);
            plot(1:vidsz(3),xtr(1:vidsz(3)),'-b',...
                1:vidsz(3),ytr(1:vidsz(3)));
            legend('x','y');
            hold on
            stabplotX = plot([currfr, currfr],prevaxes.YLim,'--',...
                'Color',[0.3, 0.3, 0.3]);
            hold off
            prevaxes.XLim = [0, vidsz(3)];
    end

% Change motion correction type (smooth / no motion)
    function interpMethod_Callback(~,~,~)
        % Get value
        if rInt1.Value
            interpMethod = 'nearest';
        else
            if rInt2.Value
                interpMethod = 'linear';
            elseif rInt3.Value
                interpMethod = 'cubic';
            else
                error('Error in interpMethod_Callback: no valid method');
            end
        end
        
        % If stabilized, refresh, otherwise, do nothing
        if stabilizing > 1
            updateImage;
        end
        
    end

% Change autocrop or illumination correction
    function autocrop_Callback(~,~,~)
        % Get value
        if buttonAutoCrop.Value
            autoCrop = 1;
            
            % Update autocrop correction window
            if size(trackpoints,1)>2
                CalcCorrection;
            end
            calcCropCorr;
        else
            autoCrop = 0;
            autoCropCorr = [0 0 0 0];
        end
        
        illCorr = buttonIllCorr.Value;
        if illCorr == 1
            if isempty(corrMatrix)
                % Get correction matrix
                [corrMatrix, corrBG] = DeepPIVbeamEqualizer(newVidPath,100,500);
            end
        end
        
%         % If stabilized, refresh, otherwise, do nothing
%         if stabilizing > 1
%             updateImage;
%         end
        updateImage;
    end

% Calculate crop correction
    function calcCropCorr(~,~,~)
        wl = 1+ceil(max(motioncorr(:,2)));
        wr = vidsz(2);
        wb = 1+ceil(max(motioncorr(:,3)));
        wt = vidsz(1);
        autoCropCorr = [wl wr wb wt];
    end

% Switch auto stabilization mode
    function autoStabMode_Callback(~,~,~)
        if panelauto.Visible == 'on';
            panelauto.Visible = 'off';
            panelautostreak.Visible = 'on';
            buttonAutoStabMode.String = 'Point tracker';
        else
            panelauto.Visible = 'on';
            panelautostreak.Visible = 'off';
            buttonAutoStabMode.String = 'Streak straightener';
        end
    end

% Start streak straightener
    function streakStart_Callback(~,~,~)
        StreakStraightener(newVidPath);
    end

% Load  streak straightener results
    function streakLoad_Callback(~,~,~)
        % Load from .mat
        [filepath,name,extension] = fileparts(newVidPath);
        streakFileName = [filepath,filesep,name,'_streakInfo.mat'];
        load(streakFileName,'autostab','motioncorr','trackpoints');
        loadSideCar;
        buttonStab.String = 'Clear stabilization';
        buttonROI.Enable = 'off';
        r12.Enable = 'off';
        motionType_Callback;
    end

% Perform stabilization
    function autoStab_Callback(~,~,~)
        if stabilizing == 1
            % Just stop stabilization
            buttonStab.String = 'Stopping...';
            stabilizing = 0;
            
            pause(0.1);
            
            % Clear auto stab
            xtr = zeros(1,vidsz(3));
            ytr = zeros(1,vidsz(3));
            rot = zeros(1,vidsz(3));
            motioncorr = [0 0 0];
            motioncorrout = 0;
            trackpoints = [0 0 0];
            alltrackpoints = {trackpoints};
            calcCropCorr;
            frame_Callback;
            
        elseif stabilizing > 1
            stabilizing = 0;
            buttonStab.String = 'Stabilize';
            buttonROI.Enable = 'on';
            r12.Enable = 'on';
            
            % Clear auto stab
            xtr = zeros(1,vidsz(3));
            ytr = zeros(1,vidsz(3));
            rot = zeros(1,vidsz(3));
            motioncorr = [0 0 0];
            motioncorrout = 0;
            trackpoints = [0 0 0];
            alltrackpoints = {trackpoints};
            calcCropCorr;
            frame_Callback;
            
        else
            % Alert to set ROI if this has not happened yet
            if ~isa(roirect,'imrect')
                h = warndlg('Select a region of interest (ROI) first','Warning');
                waitfor(h);
                setROI_Callback;
            else
                % Load ROI
                startROI = getPosition(roirect); % [x y w h]
                totfr = vidsz(3);
                
                % Get other settings
                rotatset = r11.Value;
                smmotion = r21.Value;
                switch r31.Value
                    case 1
                        referenceType = 'fixed';
                    case 0
                        referenceType = 'moving';
                end
                
                % Initialize stabilization
                stabilizing = 1;
                stabplotX = 0;
                [optimizer, metric] = imregconfig('monomodal');
                
                % If window was small, turn on graph axes and hide manual
                % tracking panel
                if smallWindow == 1
                    paneloutput.Visible = 'Off';
                end
                prevaxes.Visible = 'On';
                
                % Meshgrid of entire frame
                [X,Y] = meshgrid(1:1:vidsz(2),...
                    1:1:vidsz(1));
                % Meshgrid of initial ROI
                [Xst,Yst] = meshgrid(startROI(1):1:startROI(1) + ...
                    startROI(3) - 1,startROI(2):1:startROI(2) + ...
                    startROI(4) - 1);
                Xroi = Xst; Yroi = Yst;     % Beginning ROI
                stfr = 1;
                im1 = interp2(X,Y,double(getExtFrame(stfr)),...
                        Xroi,Yroi);
                sz = size(Xst);
                im1corrstack = zeros(sz(1),sz(2),5,'uint8');
                im1corrstack(:,:,1) = im1;
                xtr = zeros(1,totfr);
                ytr = zeros(1,totfr);
                rot = zeros(1,totfr);
                
                % Do stabilization (within threshold)
                % Using while loop to prevent problems with inaccurate
                % frame count
%                 while hasFrame(vid)
%                     stfr = stfr+1;
                while stfr<totfr
                    stfr = stfr+1;
                    
                    if ~strcmp(buttonStab.String(1:5),'Stopp')
                        % Update progress on button
                        buttonStab.String = ['Tracking... ',...
                            num2str(round(100*stfr/totfr)) '%'];
                        pause(0.002);
                        
                        % Load current frame; getExtFrame is sometimes
                        % inaccurate and therefore just read the next frame
                        if extFile == 1
                            kk = readFrame(vid);
                            if channelmix>3
                                im2base = rgb2gray(kk);
                            else
                                im2base = kk(:,:,channelmix);
                            end
                            %                             im2base = kk(:,:,1);
                            im2basedub = double(im2base);
                        else
                            im2base = uint8(inputstack(:,:,stfr));
                            im2basedub = double(inputstack(:,:,stfr));
                        end
                        
                        % Make sure to restrict roi to range
                        outrange = Xroi>vidsz(2) | Xroi < 1 | ...
                            Yroi < 1 | Yroi > vidsz(1);
                        if sum(outrange(:))>0
                            % Remove bad columns
                            temp1 = sum(Xroi>vidsz(2) | Xroi < 1)>0;
                            Xroi(:,temp1) = [];
                            Yroi(:,temp1) = [];
                            % Remove bad rows
                            temp1 = sum(Yroi>vidsz(1) | Yroi < 1,2)>0;
                            Xroi(temp1,:) = [];
                            Yroi(temp1,:) = [];
                        end
                        
                        im2 = (interp2(X,Y,im2basedub,...
                            Xroi,Yroi));
                        % If no rotation adjustment requested, just do translation
                        if rotatset == 0
                            tform = imregtform(im2,im1,'translation',...
                                optimizer,metric);
                        else
                            tform = imregtform(im2,im1,'rigid',...
                                optimizer,metric);
                        end
                        
                        % Make sure it is within threshold (20 pixels total in
                        % translation or 10 degree rotation), otherwise ignore
                        % result
                        if or(sum(tform.T(3,1:2)) > 20,...
                                abs(acos(tform.T(1,1))) > pi/180)
                            tform.T = [1 0 0; 0 1 0; 0 0 1];
                        end
                        
                        % Save the tform for later
                        allthetforms(:,:,stfr) = tform.T;
                        
                        % Get warp values and build new ROI matrices
                        if rotatset == 0    % If no rotation
                            % Get warp values
                            rot(stfr) = 0;
                            xtr(stfr) = xtr(stfr-1) + tform.T(3,1);
                            ytr(stfr) = ytr(stfr-1) + tform.T(3,2);
                            
                            % Calculate new ROI
                            Xroi = Xst - xtr(stfr);
                            Yroi = Yst - ytr(stfr);
                        else
                            % Get warp values
                            rot(stfr) = sum(acos(allthetforms(1,1,1:stfr)));
                            xtr(stfr) = (xtr(stfr-1)) - ...
                                1*(-tform.T(3,1)*cos(rot(stfr)) + tform.T(3,2)*sin(rot(stfr)));
                            ytr(stfr) = (ytr(stfr-1)) - ...
                                1*(-tform.T(3,1)*sin(rot(stfr)) + tform.T(3,2)*cos(rot(stfr)));
                            
                            % Calculate new ROI
                            newrot = acos(tform.T(1,1));
                            roirot = [cos(newrot), -sin(newrot); ...
                                sin(newrot), cos(newrot)];
                            minX = min(Xroi(:)); %minX = mean(Xroi(:))
                            minY = min(Yroi(:)); %minY = mean(Yroi(:));
                            temp=[Xroi(:)-minX-tform.T(3,1),...
                                Yroi(:)-minY-tform.T(3,2)]*roirot';
                            Xroi=reshape(temp(:,1),sz) + minX;% * cos(rot(stfr))/cos(rot(stfr-1));
                            Yroi=reshape(temp(:,2),sz) + minY;
                        end
                        
                        % Go ahead and calculate adjusted image for next round
                        im2new = (interp2(X,Y,im2basedub,...
                            Xroi,Yroi));
                        
                        % New box position
                        currbox = [Xroi(1,1), Yroi(1,1);...
                            Xroi(1,end), Yroi(1,end);...
                            Xroi(end,end), Yroi(end,end);...
                            Xroi(end,1), Yroi(end,1)];
                        
                        % Draw new frame and box
                        mmT.frameslider.Value = stfr;
                        mmT.frametxt.String = ['Frame: ',num2str(stfr)];
                        currfr = stfr;
                        
                        if stfr>2
                            baseimshow.CData = im2base;
                        else
                            baseimshow = imshow(im2base);
                        end
                        
                        % Updating polygon takes ~ 0.002 s
                        if stfr>2
                            polyg.setPosition(currbox)
                        else
                            polyg = impoly(mmT.axes,currbox,'Closed',true);
                        end
                        
                        pause(0.01);
                        % Drawing preview takes ~0.003 seconds
                        if stfr>2
                            if stfr>3
                                imcomp.CData = imfuse(im2new,im1);
                            else
                                axes(prevaxes);
                                imcomp = imshowpair(im2new,im1);
                                axes(mmT.axes)
                            end
                        end
                        
                        % This awkward line helps to error out of the loop
                        % if window has been closed
                        if mod(stfr,50) == 0
                            drawnow;
                        end

                        % Prepare image for next round
                        switch referenceType
                            case 'moving'
                                % Update median stack by shifting all old entries one
                                % level deeper, and adding the new one as first entry
                                im1corrstack(:,:,2:5) = im1corrstack(:,:,1:4);
                                im1corrstack(:,:,1) = im2new;
                                
                                % Set the new original image
                                im1 = median(im1corrstack(:,:,1:min([stfr,5])),3);
                            case 'fixed'
                                % im1 is preserved from frame 1
                        end
                        
                        if extFile==1
                            if ~hasFrame(vid)
                                totfr = stfr;
                                mmT.frameslider.Max = totfr;
                                mmT.frameslider.SliderStep = [1/totfr 10/totfr];
                                vidsz(3) = totfr;
                            elseif totfr==stfr
                                totfr = totfr+1;
                                mmT.frameslider.Max = totfr;
                                mmT.frameslider.SliderStep = [1/totfr 10/totfr];
                                vidsz(3) = totfr;
                            end
                        end
                        
                    else
                        stfr = totfr;
                    end
                end
                imobj = findobj(mmT.axes.Children,'Type','Image');
                
                xtr = xtr-min(xtr);
                ytr = ytr-min(ytr);
                saveStabInfo;
                
                axes(prevaxes);
                plot(1:stfr,xtr(1:stfr),'-b',1:stfr,ytr(1:stfr));
                legend('x','y');
                hold on
                stabplotX = plot([stfr, stfr],prevaxes.YLim,'--',...
                    'Color',[0.3, 0.3, 0.3]);
                hold off
                prevaxes.XLim = [0, stfr];
                pause(0.01);
                
                motionType_Callback;
                
                % Quick image update for axis adjust
                thisfr = mmT.frameslider.Value;
                im = getExtFrame(thisfr);
                axes(mmT.axes);
                imshow(im);
                
                updateImage;
                
                % If window was too small for extra preview window, hide it
                % after completion
                if smallWindow == 1
                    prevaxes.Visible = 'Off';
                else
                    prevaxes.Visible = 'On';
                end
                paneloutput.Visible = 'On';
                
                stabilizing = 2;
                buttonStab.String = 'Clear stabilization';
                buttonROI.Enable = 'off';
                r12.Enable = 'off';
            end
            mmT.frameslider.Value = 1;
            frame_Callback;
        end
    end

% Change to different track
    function [] = changeTrack_Callback(~,~,~)
        currTrack = popupChangeTrack.Value;
        % If the last entry selected, add track
        if currTrack > numel(alltrackpoints)
            trackpoints = [0,0,0];
            alltrackpoints{currTrack} = trackpoints;
            popupChangeTrack.String{currTrack} = ['Track ' num2str(currTrack)];
            popupChangeTrack.String{currTrack+1} = 'Add track...';
        else
            % Select existing track
            trackpoints = alltrackpoints{currTrack};
            % Remove next track if it's empty
            if numel(alltrackpoints)>currTrack
                if size(alltrackpoints{currTrack+1},1)<2
                    disp('attempt')
                    alltrackpoints(currTrack+1) = [];
                    popupChangeTrack.String(currTrack+1) = [];
                end
            end
        end
        pause(0.002);
        updateImage;
    end


% Add manual point
    function [] = addPoint_Callback(~,~,~)
        pause(0.002);
        if (buttonAddPoint.String(1:3) == 'Add')
            set(findall(panelauto, '-property', 'enable'), 'enable', 'off');
            currfr = mmT.frameslider.Value;
            currpoint = impoint(mmT.axes);
            currpointMoved = 1;
            addNewPositionCallback(currpoint,@impointMove);
            buttonManualApply.Enable = 'on';
            buttonManualReset.Enable = 'on';
        else
            ind = abs(trackpoints(:,1) - currfr) < 0.1;
            if sum(ind) == 1
                trackpoints(ind,:) = [];
                delete(currpoint);
                currpoint = 0;
            end
            updateImage;
        end
        pause(0.002);
    end

    function [] = impointMove(~)
        currpointMoved = 1;
    end

% Jump to next / previous control point
    function [] = jumpToPoint_Callback(source,~,~)
        thisfr  = mmT.frameslider.Value;
        switch get(source,'String')
            case '<'
                newfr = max(trackpoints(trackpoints(:,1)<thisfr,1));
            case '>'
                newfr = min(trackpoints(trackpoints(:,1)>thisfr,1));
            otherwise
                updateContrPtButtons;
                newfr = thisfr;
        end
        pause(0.002);
        
        mmT.frameslider.Value = newfr;
        frame_Callback;
    end

% Update control point buttons
    function updateContrPtButtons
        thisfr  = mmT.frameslider.Value;
        if sum(trackpoints(:,1)<thisfr)<2
            buttonPrevPoint.Enable = 'off';
        else
            buttonPrevPoint.Enable = 'on';
        end
        if sum(trackpoints(:,1)>thisfr)==0
            buttonNextPoint.Enable = 'off';
        else
            buttonNextPoint.Enable = 'on';
        end
    end
        

% Clear control points
    function [] = clearManual_Callback(~,~,~)
        trackpoints = [0,0,0];
        if currpoint~=0
            delete(currpoint);
        end
        currpoint = 0;
        buttonManualReset.Enable = 'off';
        buttonAddPoint.String = 'Add control point';
        buttonManualApply.Enable = 'off';
        
        currTrack = popupChangeTrack.Value;
        alltrackpoints{currTrack} = trackpoints;
        
        if numel(alltrackpoints) < 2
            stabilizing = floor(stabilizing);
            set(findall(panelauto, '-property', 'enable'), 'enable', 'on');
        end
        
        pause(0.002);
    end


% Apply manual control points
    function [] = applyManual_Callback(~,~,~)
        if buttonManualApply.String(1:3) == 'App'
            % Apply manual corrections
            stabilizing = min([stabilizing + 0.5,2.5]);
            CalcCorrection;
            saveStabInfo;
            buttonManualApply.String = 'Disable correction';
        else
            % Don't
            stabilizing = max([stabilizing - 0.5,0]);
            buttonManualApply.String = 'Apply correction';
        end
        
        % Quick image update for axis adjust
        thisfr = mmT.frameslider.Value;
        im = getExtFrame(thisfr);
        axes(mmT.axes);
        imshow(im);
        
        updateImage;
        pause(0.002);
    end

%  Show detail view
    function [] = showLoupe(~,~,~)
        prevaxes.Visible = 'on';
    end

%  Close detail view
    function [] = hideLoupe(~,~,~)
        prevaxes.Visible = 'ff';
    end

%  Finish button
    function []=finish_Callback(~,~,~)
        if size(trackpoints,1)>2
            try
            TrackCurrPoint;
            catch
            end
        end
        
        if strcmp(mmT.finishbutton.String(1:4),'Expo')
            exportVideo;    % (contains saveStabInfo)
        else
            saveStabInfo;   
            % Generates output motion correction, and saves if necessary
        end
        
        close(mmT.fig);
        
    end

%% Update image

    function [] = updateImage(~,~,~)
        mmT.updtxt.Visible = 'on';
        pause(0.002);
%         buttonAddPoint.Enable = 'on';
        
        % Initialize parameters
        thisfr = mmT.frameslider.Value;
        im = getExtFrame(thisfr);
        
        % Adjust image brightness
        bright = 200 - mmT.brightslider.Value;
        temp = (0:1/255:1).^(bright/100);
        
        % Display image
        axes(mmT.axes);
%         imshow(im);
%         imobj = mmT.axes.Children(2);
        imobj = findobj(mmT.axes.Children,'Type','Image');

        if autoCrop == 1
            imobj.CData = ...
                im(autoCropCorr(3):autoCropCorr(4),...
                autoCropCorr(1):autoCropCorr(2));
            imobj.XData = [autoCropCorr(1) autoCropCorr(2)];
            imobj.YData = [autoCropCorr(3) autoCropCorr(4)];
        else
            imobj.CData = im;
            imobj.XData = [1 size(im,2)];
            imobj.YData = [1 size(im,1)];
        end
        
        %         imshow(im);
        colormap(gca,[temp' temp' temp']);
        
        % Remove other object in axes
        for i3 = size(mmT.axes.Children):-1:1 % In reverse
            if ~strcmp(mmT.axes.Children(i3).Type,'image')
                delete(mmT.axes.Children(i3));
            end
        end
        
        % Plot track
        if size(trackpoints,1)>2
            CalcCorrection;
            hold on
            if mod(stabilizing,1)>0.1
%                 plot(max(motioncorr(:,2))+motioncorrref(1),...
%                     max(motioncorr(:,3))+motioncorrref(2),'ro',...
%                     'MarkerSize',10);
                plot(trackpointsInt(thisfr,2)-motioncorr(thisfr,2)+max(motioncorr(:,2)),...
                    trackpointsInt(thisfr,3)-motioncorr(thisfr,3)+max(motioncorr(:,3)),'ro',...
                    'MarkerSize',10);
            else
%                 plot(motioncorr(thisfr,2)+motioncorrref(1),...
%                     motioncorr(thisfr,3)+motioncorrref(2),'ro',...
%                     'MarkerSize',10);
                plot(trackpointsInt(thisfr,2),...
                    trackpointsInt(thisfr,3),'ro',...
                    'MarkerSize',10);
            end
            hold off
        end
        
        ind = abs(trackpoints(:,1) - thisfr) < 0.1;
        if sum(ind) == 1
            if mod(stabilizing,1)>0.1
%                 currpoint = impoint(gca,max(motioncorr(:,2))+...
%                     motioncorrref(1),...
%                     max(motioncorr(:,3))+motioncorrref(2));
                currpoint = impoint(gca,trackpointsInt(thisfr,2)-...
                    motioncorr(thisfr,2)+max(motioncorr(:,2)),...
                    trackpointsInt(thisfr,3)-motioncorr(thisfr,3)+...
                    max(motioncorr(:,3)));
            else
                currpoint = impoint(gca,trackpoints(ind,2),...
                    trackpoints(ind,3));
            end
            currpointMoved = 0;
            addNewPositionCallback(currpoint,@impointMove);
%             buttonAddPoint.Enable = 'on';
            buttonAddPoint.String = 'Delete control point';
        else
            buttonAddPoint.String = 'Add control point';
        end
        
        % Update dashed line in miniplot if it exists
        if isa(stabplotX,'matlab.graphics.chart.primitive.Line')
            stabplotX.XData = [thisfr, thisfr];
        end
        
        mmT.updtxt.Visible = 'off';
        roirect = 0;
        
        drawnow;
        pause(0.001);
    end


%% Other functions

%  Add current point to tracker list
    function []=TrackCurrPoint(~)
        % Correct track points
        if sum(mod(trackpoints(:,1),1)>0)>0
            warning('Non-integer frame numbers detected in track!');
            trackpoints(mod(trackpoints(:,1),1)>0,:) = [];
        end
        
        if currpointMoved == 1
            if currpoint~=0
                try
                    currpos = getPosition(currpoint);
                    if mod(stabilizing,1)==0
                        currloc = [currfr currpos(1) currpos(2)];
                    else
                        if currfr>0
                            currloc = [currfr, currpos(1)-(max(motioncorr(:,2)))+...
                                motioncorr(currfr,2), ...
                                currpos(2)-(max(motioncorr(:,3)))+...
                                motioncorr(currfr,3)];
                        else
                            warning('Problem reading previous frame number');
                        end
                    end
                    
                    if sum(trackpoints(:,1)==currfr)==1
                        ind = (trackpoints(:,1)==currfr);
                        trackpoints(ind,1:3) = currloc;
                    else
                        trackpoints(end+1,1:3) = currloc;
                    end
                catch
                    warning('here');
                end
            end
        end
        [~,ind] = sort(trackpoints(:,1));
        trackpoints = trackpoints(ind,:);
        currTrack = popupChangeTrack.Value;
        alltrackpoints{currTrack} = trackpoints;
        
        currpointMoved = 0;
    end

% Calculate linear correction from manual point selection
    function corrr = CalcCorrection(~)
        % First, calculate correction point in all locations
        corrr = (1:vidsz(3))';
        temptrackpoints = trackpoints;
        temptrackpoints(trackpoints(1,1)==0,:) = [];  % Remove first entry
        % Extrapolate only if only one track exists
        if numel(alltrackpoints)==1
            corrr(:,2) = (interp1(temptrackpoints(:,1),...
                temptrackpoints(:,2),corrr(:,1),'linear','extrap'));
            corrr(:,3) = (interp1(temptrackpoints(:,1),...
                temptrackpoints(:,3),corrr(:,1),'linear','extrap'));
            motioncorr = corrr;
        else
            corrr(:,2) = (interp1(temptrackpoints(:,1),...
                temptrackpoints(:,2),corrr(:,1),'linear'));
            corrr(:,3) = (interp1(temptrackpoints(:,1),...
                temptrackpoints(:,3),corrr(:,1),'linear'));
        end
        trackpointsInt = corrr; % Current track interpolated
        
        % If multiple tracks exist, calculate new correction
        if numel(alltrackpoints)>1
            numTracks = numel(alltrackpoints);
            %         corrr = nan(vidsz(3),1+2*numTracks);
            corrr = nan(vidsz(3),3,numTracks);
            corrr(:,1,1) = (1:vidsz(3))';
            for j = 1:numTracks
                temptrackpoints = alltrackpoints{j};
                temptrackpoints(temptrackpoints(1,1)==0,:) = [];  % Remove first entry
                corrr(2:end,2,j) = diff(interp1(temptrackpoints(:,1),...
                    temptrackpoints(:,2),corrr(:,1),'linear'));
                corrr(2:end,3,j) = diff(interp1(temptrackpoints(:,1),...
                    temptrackpoints(:,3),corrr(:,1),'linear'));
                %                 corrr(1:end,2,j) = diff(interp1(temptrackpoints(:,1),...
                %                     temptrackpoints(:,2),corrr(:,1),'linear'));
                %                 corrr(1:end,3,j) = diff(interp1(temptrackpoints(:,1),...
                %                     temptrackpoints(:,3),corrr(:,1),'linear'));
                corrr(1,2:3,j) = 0;
            end
            %             for j = 2:3
            %                 temp = isnan(corrr(:,j));
            %                 corrr(temp,j) = interp1(corrr(~temp,1),corrr(~temp,j),...
            %                     corrr(temp,1),'linear','extrap');
            %             end
            % motioncorr = cumsum(median(corrr,3,'omitnan'),1,'omitnan');
            temp1 = median(corrr,3,'omitnan');
            
            for j = 2:3
                temp2 = isnan(temp1(:,j));
                temp1(temp2,j) = interp1(temp1(~temp2,1),temp1(~temp2,j),...
                    temp1(temp2,1),'linear','extrap');
            end
            
            motioncorr = cumsum(temp1,1,'omitnan');
            motioncorr(:,1) = corrr(:,1,1);
        end
        
        motioncorrref = [min(motioncorr(:,2)), min(motioncorr(:,3))];
        motioncorr(:,2) = motioncorr(:,2)-motioncorrref(1);
        motioncorr(:,3) = motioncorr(:,3)-motioncorrref(2);
    end

% Load video file as image stack
    function loadImgStack(vidpath,~,~)
        if nargin<1
            % Get file
            [FileName,PathName] = uigetfile({'*.*';'*.avi';'*.mov';...
                '*.MOV';'*.mp4';'*.mpg';'*.mat'},'Select video file');
            newVidPath = [PathName FileName];
        else
            newVidPath = vidpath;
        end
        [filepath,name,extension] = fileparts(newVidPath);
        
        if strcmp(extension,'.mat')
            extFile = 0;
            temp = load(newVidPath,'IMAGE');
            inputstack = temp.IMAGE;
        else
            extFile = 1;    % External file; not loaded completely, but called
            % frame-by-frame
            
            % Open video
            switch lower(extension)
                case '.cine'
                    vid = CineReaderRaw(newVidPath);
                    % Get frame size and number
                    vidsz = [vid.height, vid.width, vid.NumberOfFrames];
                otherwise
                    vid = VideoReader(newVidPath);
                    % Estimate number of frames
%                     finish = (vid.FrameRate*vid.Duration);  % round removed
                    finish = vid.NumFrames;
                    vidsz = round([vid.Height, vid.Width, finish]);
            end
            % Change finish button text to "export"
            if exist('mmT','var')
                mmT.finishbutton.String = 'Export video...';
            end
            
        end
        
        % Look for sidecar file
        sidecarFileName = [filepath,filesep,name,'_stabInfo.mat'];
        sidecarFileName2 = [filepath,filesep,name,'_params.mat'];
        
        if exist(sidecarFileName,'file')==2
            % Load sidecar data
            load(sidecarFileName);
            
            loadSideCar;
        elseif exist(sidecarFileName2,'file')==2
            % Load sidecar data
            load(sidecarFileName2,'output');
            warndlg(['Loading stabilization data from ''params'' file '...
                'with other adjustments. Any changes will be saved to '...
                'a separate file!']);
            autostab = output.autostab;
            trackpoints = output.motiontrackpoints;
            
            
            loadSideCar;
        else    % If no sidecar file found
            % Reinitialize a few settings
            trackpoints = [0,0,0];
            allthetforms = zeros(3,3,vidsz(3));
            allthetforms(:,:,1) = [1 0 0; 0 1 0; 0 0 1];
            autostab.xtr = zeros(1,vidsz(3));
            xtr = zeros(1,vidsz(3));
            ytr = zeros(1,vidsz(3));
            rot = zeros(1,vidsz(3));
            % Type of automatic motion correction: no motion (0) or smooth motion (1)
            motionType = 0;
            stabilizing = 0;
        end
        
        if stabilizing > 0
            buttonStab.String = 'Clear stabilization';
            buttonROI.Enable = 'off';
            r12.Enable = 'off';
        end
    end

% Load from sidecar file
    function loadSideCar(~)
        xtr = autostab.xtr;
        ytr = autostab.ytr;
        rot = autostab.rot;
        motionType = autostab.motionType;
        
        if iscell(trackpoints)
            alltrackpoints = trackpoints;
            trackpoints = alltrackpoints{1};
        else
            alltrackpoints = {trackpoints};
        end
        
        if size(trackpoints,1)>2
            stabilizing = 0.5;
            CalcCorrection;
        else
            stabilizing = 0;
        end
        if ~(sum(autostab.xtr)==0)
            stabilizing = stabilizing + 2;
        end
        
        if isfield(autostab,'interpMethod')
            interpMethod = autostab.interpMethod;
        else
            interpMethod = 'nearest';
        end
    end

% Retrieve frame from external video
    function imfr = getExtFrame(frno)
        % Takes about 0.25 s!
        
        % frno is the frame number requested
        if extFile == 1
            switch lower(extension)
                case '.cine'
                    im = vid.read(frno);
                otherwise
                    % If not simply next frame, search for it
                    if frno ~= currfr + 1
                        % Set appropriate time
                        vid.CurrentTime = (frno - 1)/vid.FrameRate;
                    end
                    if hasFrame(vid)
                        im = readFrame(vid);                % Read frame
                    end
            end
            
            if channelmix == 0
                if exportnow
                    channelmix = opt.exportnow_channelmix;
                else
                    [channelmix] = selectChannelMix(im);
                end
            end
            
            if (laseraux == 1 && isempty(mask))
%                 disp('Generating mask')
%                 warning('Step skipped')
%                 tic
%                 mask = maskLaserAux(newVidPath);
%                 toc
            end
            
            if channelmix>3
%                 imfr = rgb2gray(im);
                imfr = 255*rgb2gray(double(im)/255);
            else
                imfr = im(:,:,channelmix);
            end
            % Apply illumination correction
            if illCorr == 1
                imfr = uint8((double(imfr)-corrBG)./corrMatrix);
            else
                imfr = uint8(imfr);
            end
            
%             imfr = im(:,:,1);             % Make gray from red channel
            %             imfr = rgb2gray(im);
        else
            imfr = inputstack(:,:,frno);
        end
        
        if laseraux == 1
            imfr(mask) = NaN;
        end
        
        % If stabilization has been performed, retrieve stabilized version
        if and(stabilizing > 0,stabilizing~=1)
            imfr = getStabFrame(imfr,frno);
        end
        
        % Update currfr
        if exist('mmT.fig','var')
            currfr = mmT.frameslider.Value;
        end
    end

% Stabilize frame based on stabilization data
    function stabfr = getStabFrame(im,fr)
        
        % Set transformation for current frame
        angle = -rot(fr);
        tform = affine2d;
        
        if mod(stabilizing,1) > 0.1 % Supposed to be 0.5 or 2.5 if manually stabilized
            % Define size of entire output array
            sz = [vidsz(1)+round(max(ytr)-min(ytr))+...
                round(max(motioncorr(:,3))),...
                vidsz(2)+round(max(xtr)-min(xtr))+...
                round(max(motioncorr(:,2)))];
            xcorr = max(motioncorr(:,2))-motioncorr(fr,2);
            ycorr = max(motioncorr(:,3))-motioncorr(fr,3);
            try
            tform.T = [cos(angle), sin(angle), 0;...
                -sin(angle), cos(angle), 0;...
                xtr(fr)+xcorr, ytr(fr)+ycorr, 1];
            catch
                xcorr
                ycorr
                motioncorr
                disp('what''s happening');
            end
        else
            % Define size of entire output array
            sz = [vidsz(1)+round(max(ytr)-min(ytr)),...
                vidsz(2)+round(max(xtr)-min(xtr))];
            tform.T = [cos(angle), sin(angle), 0;...
                -sin(angle), cos(angle), 0;...
                xtr(fr), ytr(fr), 1];
        end
        
        im2 = imwarp(im,tform,interpMethod,'OutputView',imref2d(sz),...
            'FillValues',0);
        
        stabfr = uint8(im2);
    end


% Save sidecar file with stabilization parameters
    function saveStabInfo
        % Save all info to 'autostab' variable
        autostab.xtr = xtr;
        autostab.ytr = ytr;
        autostab.rot = rot;
        autostab.motionType = motionType;
        autostab.motionType = motionType;
        autostab.interpMethod = interpMethod;
        autostab.autoCrop = autoCrop;
        
        % Update manual tracking data
        currTrack = popupChangeTrack.Value;
        alltrackpoints{currTrack} = trackpoints;
        
        if numel(alltrackpoints) == 1 && size(trackpoints,1)<2
            motioncorr = 0;
        else
            CalcCorrection;
        end
        
        temp = motioncorr;
        motioncorrout = max(motioncorr,[],1) - motioncorr + ...
            [zeros(length(xtr),1), xtr', ytr'];
        motioncorrout(:,1) = motioncorr(:,1);
        motioncorr = motioncorrout;
        
        % Only if the original data is an external file
        if extFile == 1
            % Save file
            trackpoints = alltrackpoints;
            save(sidecarFileName,'autostab','trackpoints','motioncorr','channelmix');
            trackpoints = alltrackpoints{currTrack};
            % Save additional CSV with motion result
            [temp1,temp2,~] = fileparts(sidecarFileName);
            sidecarTextFile = [temp1 filesep temp2 '.csv'];
            motionT = table(motioncorr(:,1),motioncorr(:,2),motioncorr(:,3));
            motionT.Properties.VariableNames = {'FrameNumber','X','Y'};
            writetable(motionT,sidecarTextFile);
        end
        motioncorr = temp;
    end


% Export stabilized video
    function exportVideo(~,~,~)
        % Save settings
        saveStabInfo;
        
        % Verify which channel to export
        if vidsz(3) > 1
            if exportnow
                expChannel = opt.exportnow_channelmix;
            else
                expChannel = 0;
            end
            while expChannel == 0
                expChannel = inputdlg('Channel to export:','Confirm export channel',...
                    [1 20],{num2str(channelmix)});
                expChannel = str2double(expChannel{1});
                if expChannel < 5
                    % do something
                else
                    expChannel = 0;
                    warndlg(['A numeric channel selection is required (1 = R, '...
                        '2 = G, 3 = B, 4 = L)'],'Warning');
                end
            end
            channelmix = expChannel;
        end
        
        % If external file, auto-generate a file name and location
        if extFile == 1
            % Request file name
            [filepath,name,~] = fileparts(newVidPath);
            newFileName = [filepath,filesep,name,'_stab.mp4'];
            frrate = vid.FrameRate;
        else
            newFileName = 'StabilizedVideo.mp4';
            frrate = 30;
        end
        
        % File format options
        fformats = {'*.mp4','MPEG-4 (*.mp4)';...
            '*.avi','High-quality AVI (*.avi)';...
            '*.avi','Uncompressed AVI (*.avi)';...
            '*.mj2','Archival JPEG 2000 lossless (*.mj2)';...
            '*.tif','Image stack (*.tif)';...
            '*.png','Image stack (*.png)'};
        if ~exportnow
        [FileName,PathName,FilterIndex] = uiputfile(fformats,...
            'Export video as...',newFileName);
        else
            [PathName, FileName, ext] = fileparts(opt.exportnow_outpath);
            if sum(strcmpi(ext,{'.png','.mp4','.tif','.tiff','.jpg'}))>0
                FilterIndex = 6;
            end
            FileName = [FileName ext];
            PathName = [PathName filesep];
        end
        if FilterIndex > 4         % If exporting image stack
            [~,FileName,ext] = fileparts(FileName);
            mkdir([PathName FileName]);
            baseFileName = [PathName, FileName, filesep, FileName, ...
                '_'];
        else                        % If exporting video
            newFileName = [PathName, FileName];
            
            % Build video file
            switch FilterIndex
                case 1
                    v = VideoWriter(newFileName,'MPEG-4' );
                    v.Quality = 90;
                case 2
                    v = VideoWriter(newFileName,'Motion JPEG AVI' );
                    v.Quality = 100;
                case 3
                    v = VideoWriter(newFileName,'Uncompressed AVI' );
                case 4
                    v = VideoWriter(newFileName,'Archival' );
            end
            v.FrameRate = frrate;
            open(v);
        end
        
        axes(mmT.axes);
        
        for k = 1:vidsz(3)
            mmT.finishbutton.String = ['Exporting... ',...
                num2str(100*k/vidsz(3),'%.1f'),'%'];
            newfr = getExtFrame(k);
            if autoCrop == 1
                newfr = newfr(autoCropCorr(3):autoCropCorr(4),...
                autoCropCorr(1):autoCropCorr(2));
            end
            imshow(newfr);
            mmT.frameslider.Value = k;
            mmT.frametxt.String = ['Frame: ',num2str(k)];
            % Update dashed line in miniplot if it exists
            if isa(stabplotX,'matlab.graphics.chart.primitive.Line')
                stabplotX.XData = [k, k];
            end
            currfr = k;
            pause(0.005);
            
            % Save frame
            if FilterIndex > 4
                % Make new file name
                newFileName = [baseFileName, num2str(k-1,...
                    ['%0' num2str(ceil(log10(vidsz(3)))) 'u']), ext];
                % Save as image
                imwrite(newfr,newFileName);
            else
                writeVideo(v,newfr);
            end
        end
        
        if FilterIndex < 5
            close(v);
        end
        
        mmT.finishbutton.String = {'<html>Export complete<br>(click to continue)</html>'};
    end

% Create mask for laser aux camera
    function mask = maskLaserAux(vidpath)
        numav = 50;     % Max number of frames to load
        % Get total number of frames
        tempvid = VideoReader(vidpath);
        numfr = floor(vid.FrameRate*vid.Duration);
%         vidszt = round([tempvid.Height, tempvid.Width, numfr]);
        % Determine interval to read
        if numfr<numav
            frameRead = 1:numfr;
        else
            intv = numfr/numav;
            frameRead = unique(round(1:intv:numfr));
        end
        j = 1;
        im = nan(vidsz(1),vidsz(2),vidsz(3));
        k = 1;
        while hasFrame(tempvid)
            if sum(frameRead==k)>0
                temp = readFrame(tempvid);
                if channelmix>3
                    imfr = 255*rgb2gray(double(temp)/255);
                else
                    imfr = temp(:,:,channelmix);
                end
                im(:,:,j) = uint8(imfr);
                j = j+1;
            else
                readFrame(tempvid);
%                 % This code has an issue with incrementing j and k. Figure
%                 % out later?
%                 if intv < 4
%                     readFrame(tempvid);
%                 else
%                     if j<=numav
%                         vid.CurrentTime = (frameRead(j) - 1)/vid.FrameRate;
%                     end
%                 end
            end
            k = k+1;
        end
        mask = median(im,3,'omitnan');
        mask = mask>0.75*max(mask(:));
    end
        
        
% % Read external frame for direct use or stabilization by getExtFrame
% % This just handles reading of different file formats, type casting, and
% % channel mixing.
%     function outfr = readExtFrame(frameno)
%         % Get frame number if not given
%         if nargin < 1
%             %
%         end
%         
%         % Read frame
%         switch lower(extension)
%             case 'cine'
%             otherwise
%                 kk = readFrame(vid);
%         end
%         
%         % Mix channels
%         im2base = kk(:,:,1);
%         outfr = double(im2base);
%         % Cast to type?
%         
%     end


end