%% Information
% This program reads an image and requests the user to select a mix of
% channels

% Author: Joost Daniels, Monterey Bay Aquarium Research Institute 2018
% Date: August 2nd, 2018

%%
function [channelmix] = selectChannelMix(im)

% Check if channels are different
diffarr = zeros(size(im,3),1);
for i = 1:size(im,3)
    temp = abs(double(im(:,:,1))-double(im(:,:,2)));
    diffarr(i) = sum(temp(:));
end

if sum(diffarr)>0
    f = compareChannelMixing(im);
    drawnow;
    disp('Close channel selection screen before continuing');
    waitfor(f)
    
    prompt = ['Enter color channel to use (enter 4 for '...
        'balanced luminance values):'];
    dlgtitle = 'Color channel mixing';
    dims = [1 35];
    definput = {'1'};
    answer = inputdlg(prompt,dlgtitle,dims,definput);
    channelmix = str2double(answer);
else
    channelmix = 1;
end

end


